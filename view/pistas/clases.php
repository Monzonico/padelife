<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Clases");
  $errors = $view->getVariable("errors");
  $clases = $view->getVariable("clases");
  $meinclas = $view->getVariable("meinclas");
  $clasfull = $view->getVariable("clasfull");
  $inscri_txt = $view->getVariable("inscri_txt");

  if($_SESSION){
  $userrole = $_SESSION["currentuserrole"];
  }
?>

<?php if ($_SESSION) : ?>
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
  <div class="card-body">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
      </li>
      <li class="breadcrumb-item active"><?= $view->getVariable("title"); ?></li>
    </ol>

      <?php if ($clases != NULL) : ?>
      <!-- Page Content -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
                  <tr>
                    <th class="text-center">
                      Entrenador
                    </th>
                    <th class="text-center">
                      Pista
                    </th>
                    <th class="text-center">
                      Hora
                    </th>
                    <th class="text-center">
                      Fecha
                    </th>
                    <th class="text-center">
                      Clase
                    </th>
                    <th class="text-center">
                      Descripcion
                    </th>
                    <th class="text-center">
                      Plazas
                    </th>
                    <?php if ($userrole != "entrenador") : ?>
                    <th class="text-center">
                      Inscribirse
                    </th>
                    <?php endif; ?>
                    <?php if ($userrole == "entrenador") : ?>
                      <th class="text-center">
                      </th>
                    <?php endif; ?>
                    <?php if ($userrole == "entrenador") : ?>
                      <th class="text-center">
                      </th>
                    <?php endif; ?>
                  </tr>
                </thead>
                <tbody>

                <?php foreach ($clases as $index=>$clase) : ?>
                    <tr>
                      <td class="text-center">
                      <?= $clase->getEntrenador() ?>
                      </td>
                      <td class="text-center">
                      <?= $clase->getPista() ?>
                      </td>
                      <td class="text-center">
                      <?= $clase->getHora() ?>
                      </td>
                      <td class="text-center">
                      <?= $clase->getFecha() ?>
                      </td>
                      <td class="text-center">
                      <?= $clase->getNombre() ?>
                      </td>
                      <td class="text-center">
                      <?= $clase->getDescripcion() ?>
                      </td>
                      <td class="text-center">
                      <?= $clase->getCapacidad() ?>
                      </td>
                      <?php if($userrole != "entrenador") : ?>
                         <?php if(!$meinclas[$index] && !$clasfull[$index] && $userrole != "entrenador") : ?>
                          <td class="text-center">
                            <a href="index.php?controller=pistas&amp;action=asist_add&amp;id_clase=<?= $clase->getId() ?>"><i class="fas fa-user-plus"></i></a>
                          </td>
                         <?php else : ?>
                          <td class="text-center">
                              <i class="fas fa-user-slash"></i>
                          </td>
                          <?php endif; ?>
                        <?php endif; ?>
                      <?php if ($userrole == "entrenador"  && $clase->getEntrenador() == $_SESSION["currentusername"]) : ?>
                        <td class="text-center">
                          <a href="index.php?controller=pistas&amp;action=clases_update&amp;id_clase=<?= $clase->getId() ?>"><i class="fas fa-edit"></i></a>
                        </td>
                      <?php endif; ?>
                      <?php if ($userrole == "entrenador" && $clase->getEntrenador() == $_SESSION["currentusername"]) : ?>
                        <td class="text-center">
                          <a href="index.php?controller=pistas&amp;action=clases_delete&amp;id_reserva=<?= $clase->getReserva() ?>"><i class="far fa-trash-alt"></i></a>
                        </td>
                      <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
         <?php if ($userrole == "entrenador") : ?>
          <p class="text-center"><a href="index.php?controller=pistas&amp;action=clases_add"><i class="fas fa-plus-circle fa-2x"></i></a></p>                    
        <?php endif; ?>
    </div>
    <?php else : ?>
      <?php if ($userrole == "entrenador") : ?>
          <p class="text-center"><a href="index.php?controller=pistas&amp;action=clases_add"><i class="fas fa-plus-circle fa-2x"></i></a></p>                    
        <?php endif; ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-dog"></i> Actualmente no existen Clases</li>
    </ol>
    <?php endif; ?>



    <?php if ($userrole != "administrador" && $userrole != "entrenador") : ?>
  <div class="card-header">
    <h4 class="card-title">Inscripciones activas en Clases</h4>
  </div>
  <div class="card-body">

    <?php if ($inscri_txt != NULL) : ?>
      <!-- Page Content -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
                  <tr>
                  <th class="text-center">
                      Entrenador
                    </th>
                    <th class="text-center">
                      Clase
                    </th>
                    <th class="text-center">
                      Descripcion
                    </th>
                    <th class="text-center">
                      Pista
                    </th>
                    <th class="text-center">
                      Hora
                    </th>
                    <th class="text-center">
                      Fecha
                    </th>
                    <th class="text-center">
                      Cancelar
                    </th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach ($inscri_txt as $inscrito) : ?>
                    <tr>
                      <td class="text-center">
                      <?= $inscrito->getEntrenador()?>
                      </td>
                      <td class="text-center">
                      <?= $inscrito->getNombre()?>
                      </td>
                      <td class="text-center">
                      <?= $inscrito->getDescripcion()?>
                      </td>
                      <td class="text-center">
                      <?= $inscrito->getPista()?>
                      </td>
                      <td class="text-center">
                      <?= $inscrito->getHora()?>
                      </td>
                      <td class="text-center">
                      <?= $inscrito->getFecha()?>
                      </td>
                      <td class="text-center">
                      <a href="index.php?controller=pistas&amp;action=asist_delete&amp;id_clase=<?= $inscrito->getId() ?>"><i class="fa fa-user-minus"></i></a>
                      </td> 
                    </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php else : ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-kiwi-bird"></i> &nbsp&nbsp&nbsp&nbsp Actualmente no tienes inscripciones activas en ninguna clase</li>
    </ol>
    </div>
    <?php endif; ?>    
    <?php endif; ?>



<?php else : ?>
  Se requiere Login
<?php endif; ?>
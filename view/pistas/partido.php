<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Promocionar Partido");
  $horas = $view->getVariable("horas");
  $partidos = $view->getVariable("partidos");
  $mypartidos = $view->getVariable("mypartidos");
  $fechascamp = $view->getVariable("fechascamp");
  $check = false;
  $errors = $view->getVariable("errors");
  $promocionados = $view->getVariable("promocionados");
  if($_SESSION){
  $userrole = $_SESSION["currentuserrole"];
  $userid = $_SESSION["currentuserid"];
  }
?>

<?php if ($_SESSION): ?>

<div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
<div class="card-body">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
            </li>
            <li class="breadcrumb-item active"><?= $view->getVariable("title"); ?></li>
          </ol>
          
    <form action="index.php?controller=pistas&amp;action=partido" method="POST">
      <div class="row align-items-center">
        <div class="col center">
            <input type="text" id="datepicker" class="form-control" name="fecha" required="required" readonly>
						  <?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
            </div>
            <div class="col-sm">
            <label for="horas"></label>
              <select class="form-control" id="horas" name="hora" required="required">
                <?php foreach ($horas as $hora): ?>
                  <option><?= $hora->getHora_ini() ?></option>
                <?php endforeach; ?>
              </select>
              </div>
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Reservar</button>
      </form>
      </div>
    </div>

    <?php if ($partidos!=NULL): ?>
    <div class="card-header">
    <h4 class="card-title">Partidos en Promocion</h4>
    <ol class="breadcrumb"></ol>
    </div>
    <div class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                      <tr>
                        <th class="text-center">
                          Nombre Pista
                        </th>
                        <th class="text-center">
                          Fecha
                        </th>
                        <th class="text-center">
                          Hora Inicio
                        </th>
                        <th class="text-center">
                          Hora Fin
                        </th>
                        <th class="text-center">
                          Apuntarse
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($partidos as $partido): ?>
                        <?php if ($mypartidos!=NULL): ?>
                          <?php foreach ($mypartidos as $mypartido): ?>
                            <?php if ($partido->getPista() == $mypartido->getPista() && $partido->getFecha() == $mypartido->getFecha() && $partido->getHora_ini() == $mypartido->getHora_ini()): ?>
                           <p hidden><?= $check=true ?></p>
                           <?php endif; ?>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      <tr>
                      <td class="text-center">
                            <?= $partido->getPista() ?>
                        </td>
                        <td class="text-center">
                            <?= $partido->getFecha() ?>
                        </td>
                        <td class="text-center">
                            <?= $partido->getHora_ini() ?>
                        </td>
                        <td class="text-center">
                            <?= $partido->getHora_fin() ?>
                        </td>
                        <td class="text-center">
                          <?php if ($check): ?><i class="fas fa-check-circle"></i><?php else: ?><a href="index.php?controller=pistas&amp;action=partido&amp;id=<?= $partido->getId() ?>"><i class="fas fa-user-plus"></i></a><?php endif; ?>
                        </td>
                      </tr>
                      <p hidden><?= $check=false ?></p>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <ol class="breadcrumb">
    <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp Actualmente no existen Partidos en Promocion</li>
          </ol>
    <?php endif; ?>

    <?php if ($promocionados!=NULL): ?>
    <div class="card-header">
    <h4 class="card-title">Partidos Promocionados</h4>
    <ol class="breadcrumb"></ol>
    </div>
    <div class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                      <tr>
                        <th class="text-center">
                          Nombre Pista
                        </th>
                        <th class="text-center">
                          Fecha
                        </th>
                        <th class="text-center">
                          Hora Inicio
                        </th>
                        <th class="text-center">
                          Hora Fin
                        </th>
                        <th class="text-center">
                          Jugador 1
                        </th>
                        <th class="text-center">
                          Jugador 2
                        </th>
                        <th class="text-center">
                          Jugador 3
                        </th>
                        <th class="text-center">
                          Jugador 4
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($promocionados as $promocionado): ?>
                      <tr>
                        <td class="text-center">
                            <?= $promocionado->getPista() ?>
                          </td>
                          <td class="text-center">
                            <?= $promocionado->getFecha() ?>
                        </td>
                        <td class="text-center">
                            <?= $promocionado->getHora_ini() ?>
                        </td>
                        <td class="text-center">
                            <?= $promocionado->getHora_fin() ?>
                        </td>
                        <td class="text-center">
                            <?= $promocionado->getUsuario1() ?>
                        </td>
                        <td class="text-center">
                            <?= $promocionado->getUsuario2() ?>
                        </td>
                        <td class="text-center">
                            <?= $promocionado->getUsuario3() ?>
                        </td>
                        <td class="text-center">
                            <?= $promocionado->getUsuario4() ?>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp Actualmente no existen partidos promocionados</li>
          </ol>
<?php endif; ?>

<?php else: ?>
Se requiere Login
<?php endif; ?>

<script>

$(document).ready(function(){

  var array = [<?php echo '"'.implode('","', $fechascamp).'"' ?>];

$("#datepicker").datepicker({
    beforeShowDay: function(date){
        var string = jQuery.datepicker.formatDate('yy-mm-dd',date);
        return [ array.indexOf(string) == -1 ]
    },
    dateFormat: "yy-mm-dd",
    minDate: '0',
    maxDate: '6',
    numberOfMonths: 2,
    hideIfNoPrevNext: true
});
})

</script>
<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Crear Pista");
  $errors = $view->getVariable("errors");
?>

  <?= isset($errors["general"])?$errors["general"]:"" ?>

  <body class="text-center">
  <div class="cover-container d-flex h-100 p-5 mx-auto flex-column justify-content-center">
    <main role="main" class="inner cover">
      <h1 class="title">
        <span class="text-wrapper">
          <?= $view->getVariable("title") ?>
        </span> 
      </h1>

 <form action="index.php?controller=pistas&amp;action=add" method="POST">

    <div class="form-group">
        <label for="exampleInputEmail1">Nombre</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="nombre" placeholder="" value="" require>
        <?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
    </div>
      
      <div class="form-row">
          <div class="col">
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Crear</button>
          </div>
          <div class="col-sx-6">
            <a href="index.php?controller=pistas&amp;action=index"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
          </div>
        </div>
      </div>
    </form>

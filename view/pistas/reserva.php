<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Reservas");
  $horas = $view->getVariable("horas");
  $errors = $view->getVariable("errors");
  $reservas = $view->getVariable("reservas");
  $fechascamp = $view->getVariable("fechascamp");
  if($_SESSION){
  $userrole = $_SESSION["currentuserrole"];
  $userid = $_SESSION["currentuserid"];
  }
?>

<?php if ($_SESSION): ?>

<div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
<div class="card-body">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
            </li>
            <li class="breadcrumb-item active"><?= $view->getVariable("title"); ?></li>
          </ol>
          
    <form action="index.php?controller=pistas&amp;action=reserva" method="POST">
      <div class="row align-items-center">
        <div class="col center">
            <input type="text" id="datepicker" class="form-control" name="fecha" required="required" readonly>
						  <?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
            </div>
            <div class="col-sm">
            <label for="horas"></label>
              <select class="form-control" id="horas" name="hora" required="required">
                <?php foreach ($horas as $hora): ?>
                  <option><?= $hora->getHora_ini() ?></option>
                <?php endforeach; ?>
              </select>
              </div>
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Reservar</button>
      </form>
      </div>
    </div>

    <?php if ($reservas!=NULL): ?>
    <div class="card-header">
    <h4 class="card-title">Reservas Activas</h4>
    <ol class="breadcrumb"></ol>
    </div>
    <div class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                      <tr>
                        <th class="text-center">
                          Nombre Pista
                        </th>
                        <th class="text-center">
                          Fecha
                        </th>
                        <th class="text-center">
                          Hora Inicio
                        </th>
                        <th class="text-center">
                          Hora Fin
                        </th>
                        <th class="text-center">
                          Cancelar
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($reservas as $reserva): ?>
                      <tr>
                      <td class="text-center">
                            <?= $reserva->getNombre() ?>
                        </td>
                        <td class="text-center">
                            <?= $reserva->getFecha() ?>
                        </td>
                        <td class="text-center">
                            <?= $reserva->getHora_ini() ?>
                        </td>
                        <td class="text-center">
                            <?= $reserva->getHora_fin() ?>
                        </td>
                        <td class="text-center">
                            <a href="index.php?controller=pistas&amp;action=delete&amp;idreserva=<?= $reserva->getId_reserva() ?>"><i class="far fa-trash-alt"></i></a>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>

<?php else: ?>
Se requiere Login
<?php endif; ?>


<script>

$(document).ready(function(){

  var array = [<?php echo '"'.implode('","', $fechascamp).'"' ?>];

$("#datepicker").datepicker({
    beforeShowDay: function(date){
        var string = jQuery.datepicker.formatDate('yy-mm-dd',date);
        return [ array.indexOf(string) == -1 ]
    },
    dateFormat: "yy-mm-dd",
    minDate: '0',
    maxDate: '6',
    numberOfMonths: 2,
    hideIfNoPrevNext: true
});
})

</script>
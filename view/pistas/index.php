<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Pistas");
  $errors = $view->getVariable("errors");
  $pistas = $view->getVariable("pistas");
  if($_SESSION){
  $userrole = $_SESSION["currentuserrole"];
  }
?>
<?php if ($_SESSION): ?>
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
<div class="card-body">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
            </li>
            <li class="breadcrumb-item active"><?= $view->getVariable("title"); ?></li>
          </ol>
          <?php if ($pistas!=NULL): ?>
          <!-- Page Content -->
          <div class="content">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                      <tr>
                      <?php if ($userrole==="administrador"): ?>
                        <th class="text-center">
                          Id
                        </th>
                        <?php endif; ?>
                        <th class="text-center">
                          Nombre
                        </th>
                        <?php if ($userrole==="administrador"): ?>
                        <th class="text-center">
                        </th>
                        <?php endif; ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($pistas as $pista): ?>
                      <tr>
                      <?php if ($userrole==="administrador"): ?>
                      <td class="text-center">
                            <?= $pista->getId() ?>
                        </td>
                        <?php endif; ?>
                        <td class="text-center">
                        <?php if ($userrole==="administrador"): ?>
                          <a href="index.php?controller=pistas&amp;action=update&amp;id=<?= $pista->getId() ?>"><?= $pista->getNombre() ?></a>
                        <?php else: ?>
                            <?= $pista->getNombre() ?>
                        <?php endif; ?>
                        </td>
                        <?php if ($userrole==="administrador"): ?>
                        <td class="text-center">
                            <a href="index.php?controller=pistas&amp;action=delete&amp;id=<?= $pista->getId() ?>"><i class="far fa-trash-alt"></i></a>
                          </td>
                        <?php endif; ?>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
    <?php if ($userrole==="administrador"): ?>
        <p class="text-center"><a href="index.php?controller=pistas&amp;action=add"><i class="fas fa-plus-circle fa-2x"></i></a></p>
    <?php endif; ?>
</div>
    <?php else: ?>
    <ol class="breadcrumb">
          <li class="breadcrumb-item"><i class="fas fa-hippo"></i> Actualmente no existen Pistas Disponibles</li>
          </ol>
          <p class="text-center"><a href="index.php?controller=pistas&amp;action=add"><i class="fas fa-plus-circle fa-2x"></i></a></p>
    <?php endif; ?>
<?php else: ?>
Se requiere Login
<?php endif; ?>

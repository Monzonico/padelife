<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Crear Clase");
  $horas = $view->getVariable("horas");
  $capacidades = $view->getVariable("capacidades");
  $fechascamp = $view->getVariable("fechascamp");
  $errors = $view->getVariable("errors");
?>

<div class="container">
	
        <div class="card-header"><h1 class="text-center"><?= $view->getVariable("title") ?></h1></div>
        <?= isset($errors["general"])?$errors["general"]:"" ?>
          <div class="card-body">
            <form action="index.php?controller=pistas&amp;action=clases_add" method="POST">

		    <div class="form-group mb-5">
              <label for="nombre">Nombre</label>
              <input type="text" id="inputNombre" class="form-control" name="nombre" aria-describedby="nombre" required="required">
              <small id="nombre" class="form-text text-muted">Nombre de la Clase.</small>
            </div>

            <div class="form-group my-5">
              <label for="descripcion">Descripcion</label>
              <textarea class="form-control" id="descripcion" rows="2" name="descripcion" aria-describedby="descripcion" required="required"></textarea>
              <small id="descripcion" class="form-text text-muted">Descripcion de la Clase</small>
            </div>

            <div class="form-group my-5">
              <label for="fechaIniCamp">Fecha</label>
              <input type="text" class="form-control" id="datepicker" name="fecha" required="required" aria-describedby="fechaIni" readonly>
              <small id="fechaIni" class="form-text text-muted">Fecha de la Clase</small>
            </div>

            <div class="form-group my-5">
            <label for="horas">Hora de la Clase</label>
              <select class="form-control" id="horas" name="hora" required="required">
                <?php foreach ($horas as $hora): ?>
                  <option><?= $hora->getHora_ini() ?></option>
                <?php endforeach; ?>
              </select>
              <small id="horas" class="form-text text-muted">Hora de la Clase</small>
            </div>

            <div class="form-group my-5">
            <label for="capacidad">Capacidad de la Clase</label>
              <select class="form-control" id="capacidad" name="capacidad" required="required">
                <?php foreach ($capacidades as $capacidad): ?>
                  <option><?= $capacidad ?></option>
                <?php endforeach; ?>
              </select>
              <small id="capacidad" class="form-text text-muted">Capacidad de la Clase</small>
            </div>

            <div class="form-row">
              <div class="col">
                <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Crear</button>
              </div>
              <div class="col-sx-2">
                <a href="index.php?controller=pistas&amp;action=clases"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
              </div>
            </div>
          </div>

          </form>
        </div>
      </div>

<script>
$(document).ready(function(){

  var array = [<?php echo '"'.implode('","', $fechascamp).'"' ?>];

$("#datepicker").datepicker({
    beforeShowDay: function(date){
        var string = jQuery.datepicker.formatDate('yy-mm-dd',date);
        return [ array.indexOf(string) == -1 ]
    },
    dateFormat: "yy-mm-dd",
    minDate: '0',
    maxDate: '30',
    numberOfMonths: 2,
    hideIfNoPrevNext: true
});
})

</script>

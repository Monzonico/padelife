<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Modificar Clase");
  $capacidades = $view->getVariable("capacidades");
  $clase = $view->getVariable("clase");
  $errors = $view->getVariable("errors");
?>

<div class="container">
	
        <div class="card-header"><h1 class="text-center"><?= $view->getVariable("title") ?></h1></div>
        <?= isset($errors["general"])?$errors["general"]:"" ?>
          <div class="card-body">
            <form action="index.php?controller=pistas&amp;action=clases_update&amp;id_clase=<?= $clase->getId() ?>" method="POST">

		    <div class="form-group mb-5">
              <label for="nombre">Nombre</label>
              <input type="text" id="inputNombre" class="form-control" name="nombre" aria-describedby="nombre"  value=<?= $clase->getNombre() ?> required="required">
              <small id="nombre" class="form-text text-muted">Nombre de la Clase.</small>
            </div>

            <div class="form-group my-5">
              <label for="descripcion">Descripcion</label>
              <textarea class="form-control" id="descripcion" rows="2" name="descripcion" aria-describedby="descripcion" required="required"><?= $clase->getDescripcion() ?></textarea>
              <small id="descripcion" class="form-text text-muted">Descripcion de la Clase</small>
            </div>

            <div class="form-group my-5">
            <label for="capacidad">Capacidad de la Clase</label>
              <select class="form-control" id="capacidad" name="capacidad" required="required">
                <?php foreach ($capacidades as $capacidad): ?>
                  <option><?= $capacidad ?></option>
                <?php endforeach; ?>
              </select>
              <small id="capacidad" class="form-text text-muted">Capacidad de la Clase</small>
            </div>

            <div class="form-row">
              <div class="col">
                <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Modificar</button>
              </div>
              <div class="col-sx-2">
                <a href="index.php?controller=pistas&amp;action=clases"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
              </div>
            </div>
          </div>

          </form>
        </div>
      </div>

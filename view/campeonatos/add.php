<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Crear Campeonato");
  $errors = $view->getVariable("errors");
?>

<div class="container">
	
        <div class="card-header"><h1 class="text-center"><?= $view->getVariable("title") ?></h1></div>
        <?= isset($errors["general"])?$errors["general"]:"" ?>
          <div class="card-body">
            <form action="index.php?controller=campeonatos&amp;action=add" method="POST">

					  <div class="form-group mb-5">
              <label for="nombre">Nombre</label>
              <input type="text" id="inputNombre" class="form-control" name="nombre" placeholder="Nombre" aria-describedby="nombre" required="required">
              <small id="nombre" class="form-text text-muted">Nombre del Campeonato.</small>
							<?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
            </div>

            <div class="form-group my-5">
              <label for="normativa">Normativa</label>
              <textarea class="form-control" id="inputNormas" rows="2" name="normativa" aria-describedby="normativa" required="required"></textarea>
              <small id="normativa" class="form-text text-muted">Normativa del Campeonato.</small>
							<?= isset($errors["email"])?$errors["email"]:"" ?>
            </div>

            <div class="form-group my-5">
              <label for="fechaIniCamp">Fecha Inicio</label>
              <input type="text" class="form-control" id="datepicker" name="fechaIniCamp" required="required" aria-describedby="fechaIni" placeholder="Fecha Inicio" readonly>
              <small id="fechaIni" class="form-text text-muted">Fecha de Inicio del Campeonato.</small>
            </div>

            <div class="form-group my-5">
              <label for="fechaFinCamp">Fecha Fin</label>
              <input type="text" class="form-control" id="datepicker1" name="fechaFinCamp" required="required" aria-describedby="fechaFin" placeholder="Fecha Fin" readonly>
              <small id="fechaFin" class="form-text text-muted">Fecha de Fin del Campeonato.</small>
            </div>

            <div class="form-group my-5">
              <label for="fechIniInscri">Fecha Inicio Incripcion</label>
              <input type="text" class="form-control" id="datepicker2" name="fechIniInscri" required="required" aria-describedby="fechIniInscri" placeholder="Fecha Inicio Inscripcion" readonly>
              <small id="fechaIniInscri" class="form-text text-muted">Fecha Inicio Inscripcion del Campeonato.</small>
            </div>

            <div class="form-group my-5">
              <label for="fechaFinInscri">Fecha Fin Incripcion</label>
              <input type="text" class="form-control" id="datepicker3" name="fechaFinInscri" required="required" aria-describedby="fechaFinInscri" placeholder="Fecha Fin Inscripcion" readonly>
              <small id="fechaFinInscri" class="form-text text-muted">Fecha Fin Inscripcion del Campeonato.</small>
            </div>

            <div class="form-row">
              <div class="col">
                <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Crear</button>
              </div>
              <div class="col-sx-2">
                <a href="index.php?controller=campeonatos&amp;action=index"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
              </div>
            </div>
          </div>

          </form>
        </div>
      </div>

<script>
$(document).ready(function(){
  $( "#datepicker" ).datepicker({
    dateFormat: "yy-mm-dd",
    minDate: '0',
    numberOfMonths: 2,
    hideIfNoPrevNext: false
  });

  $( "#datepicker1" ).datepicker({
    dateFormat: "yy-mm-dd",
    minDate: '0',
    numberOfMonths: 2,
    hideIfNoPrevNext: false
  });

  $( "#datepicker2" ).datepicker({
    dateFormat: "yy-mm-dd",
    minDate: '0',
    numberOfMonths: 2,
    hideIfNoPrevNext: false
  });

  $( "#datepicker3" ).datepicker({
    dateFormat: "yy-mm-dd",
    minDate: '0',
    numberOfMonths: 2,
    hideIfNoPrevNext: false
  });
})
</script>

<?php
require_once(__DIR__ . "/../../core/ViewManager.php");
$view = ViewManager::getInstance();
$view->setVariable("title", "Campeonatos");
$errors = $view->getVariable("errors");
$campeonatos = $view->getVariable("campeonatos");
if ($_SESSION) {
  $userrole = $_SESSION["currentuserrole"];
}
?>
<?php if ($_SESSION) : ?>
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
  <div class="card-body">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
      </li>
      <li class="breadcrumb-item active"><?= $view->getVariable("title"); ?></li>
    </ol>

    <?php if ($campeonatos != NULL) : ?>
      <!-- Page Content -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
                  <tr>
                    <th class="text-center">
                      Nombre
                    </th>
                    <th class="text-center">
                      Normativa
                    </th>
                    <th class="text-center">
                      Fecha Inicio
                    </th>
                    <th class="text-center">
                      Fecha Fin
                    </th>
                    <th class="text-center">
                      Inscripcion Desde
                    </th>
                    <th class="text-center">
                      Inscripcion Hasta
                    </th>
                      <?php if ($userrole == "administrador") : ?>
                      <th class="text-center">
                      </th>
                    <?php endif; ?>
                    <?php if ($userrole == "administrador") : ?>
                      <th class="text-center">
                      </th>
                    <?php endif; ?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($campeonatos as $campeonato) : ?>
                    <tr>
                      <td class="text-center">
                          <a href="index.php?controller=categorias&amp;action=index&amp;idcamp=<?= $campeonato->getId() ?>"><?= $campeonato->getNombre() ?></a>
                      </td>
                      <td class="text-center">
                        <?= $campeonato->getNormativa() ?>
                      </td>
                      <td class="text-center">
                        <?= $campeonato->getFecha_ini() ?>
                      </td>
                      <td class="text-center">
                        <?= $campeonato->getFecha_fin() ?>
                      </td>
                      <td class="text-center">
                        <?= $campeonato->getFecha_ini_inscri() ?>
                      </td>
                      <td class="text-center">
                        <?= $campeonato->getFecha_fin_inscri() ?>
                      </td>
                      <?php if ($userrole == "administrador") : ?>
                        <td class="text-center">
                          <a href="index.php?controller=campeonatos&amp;action=update&amp;id=<?= $campeonato->getId() ?>"><i class="fas fa-edit"></i></a>
                        </td>
                      <?php endif; ?>
                      <?php if ($userrole == "administrador") : ?>
                        <td class="text-center">
                          <a href="index.php?controller=campeonatos&amp;action=delete&amp;id=<?= $campeonato->getId() ?>"><i class="far fa-trash-alt"></i></a>
                        </td>
                      <?php endif; ?>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php if ($userrole == "administrador") : ?>
        <p class="text-center"><a href="index.php?controller=campeonatos&amp;action=add"><i class="fas fa-plus-circle fa-2x"></i></a></p>
      <?php endif; ?>
    </div>
  <?php else : ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-dog"></i> Actualmente no existen Campeonatos</li>
    </ol>
    <?php if ($userrole == "administrador") : ?>
      <p class="text-center"><a href="index.php?controller=campeonatos&amp;action=add"><i class="fas fa-plus-circle fa-2x"></i></a></p>
    <?php endif; ?>
  <?php endif; ?>
<?php else : ?>
  Se requiere Login
<?php endif; ?>
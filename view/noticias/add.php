<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Crear Noticia");
  $errors = $view->getVariable("errors");
?>

<div class="container">

  <?= isset($errors["general"])?$errors["general"]:"" ?>

  <div class="card-header"><h1 class="text-center"><?= $view->getVariable("title") ?></h1></div>

 <form action="index.php?controller=noticias&amp;action=add" method="POST">

    <div class="form-group">
        <label for="exampleInputEmail1">Titulo</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="titulo" placeholder="" value="" require>
        <?= isset($errors["titulo"])?$errors["titulo"]:"" ?>
    </div>
    
        <div class="form-group">
            <label for="exampleTextarea">Texto</label>
            <textarea class="form-control" id="exampleTextarea" name="texto"  rows="6" require></textarea>
            <?= isset($errors["texto"])?$errors["texto"]:"" ?>
        </div>
      
      <div class="form-row">
          <div class="col">
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Crear</button>
          </div>
          <div class="col-sx-6">
            <a href="index.php?controller=noticias&amp;action=index"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
          </div>
        </div>
      </div>
    </form>
</div>
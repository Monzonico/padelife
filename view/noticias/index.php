<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Noticias");
  $errors = $view->getVariable("errors");
  $noticias = $view->getVariable("noticias");
  if($_SESSION){
    $userrole = $_SESSION["currentuserrole"];
    }
?>
<?php if ($_SESSION): ?>
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
  <div class="card-body">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
            </li>
          </ol>

          <!-- Page Content -->

          <div class="content">
            <div class="row">
              <div class="col">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                      <tr>
                      <?php if ($userrole==="administrador"): ?>
                        <th class="text-center">
                          Id
                        </th>
                        <?php endif ?>
                        <th class="text-center">
                          Fecha Publicacion
                        </th>
                        <th class="text-center">
                          Titulo
                        </th>
                        <th class="text-center">
                          Contenido
                        </th>
                        <?php if ($userrole==="administrador"): ?>
                        <th class="text-center">
                        </th>
                        <?php endif ?>
                        <?php if ($userrole == "administrador") : ?>
                      <th class="text-center">
                      </th>
                    <?php endif; ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($noticias as $noticia): ?>
                      <tr>
                      <?php if ($userrole==="administrador"): ?>
                        <td class="text-center">
                            <?= $noticia->getId() ?>
                        </td>
                        <?php endif ?>
                        <td class="text-center">
                            <?= $noticia->getHora() ?>
                        </td>
                        <td class="text-center">
                        <?php if ($userrole==="administrador"): ?>
                            <?= $noticia->getTitulo() ?>
                        <?php else: ?>
                            <?= $noticia->getTitulo() ?>
                        <?php endif ?>
                        </td>
                        <td class="text-center">
                        <?php if ($userrole==="administrador"): ?>
                            <?= $noticia->getTexto() ?>
                        <?php else: ?>
                            <?= $noticia->getTexto() ?>
                        <?php endif ?>
                        </td>
                        <?php if ($userrole==="administrador"): ?>
                        <td class="text-center">
                            <a href="index.php?controller=noticias&amp;action=update&amp;id=<?= $noticia->getId() ?>"><i class="far fa-edit"></i></a>
                        </td>
                        <?php endif ?>
                        <?php if ($userrole==="administrador"): ?>
                        <td class="text-center">
                            <a href="index.php?controller=noticias&amp;action=delete&amp;id=<?= $noticia->getId() ?>"><i class="far fa-trash-alt"></i></a>
                        </td>
                        <?php endif ?>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
    <?php if ($userrole==="administrador"): ?>
        <p class="text-center"><a href="index.php?controller=noticias&amp;action=add"><i class="fas fa-plus-circle fa-2x"></i></a></p>
    <?php endif ?>
</div>
<?php else: ?>
Se requiere Login
<?php endif; ?>
<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Modificar Noticia");
  $errors = $view->getVariable("errors");
  $noticia = $view->getVariable("noticia");
?>

  <?= isset($errors["general"])?$errors["general"]:"" ?>

  <body class="text-center">
  <div class="cover-container d-flex h-100 p-5 mx-auto flex-column justify-content-center">
    <main role="main" class="inner cover">
      <h1 class="title">
        <span class="text-wrapper">
          Modificar Noticia
        </span>
      </h1>

 <form action="index.php?controller=noticias&amp;action=update&amp;id=<?= $noticia->getId() ?>" method="POST">

    <div class="form-group">
        <label for="exampleInputEmail1">Titulo</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="titulo" placeholder="Titulo" value="<?= $noticia->getTitulo() ?>" require>
        <?= isset($errors["titulo"])?$errors["titulo"]:"" ?>
    </div>
    
        <div class="form-group">
            <label for="exampleTextarea">Texto</label>
            <textarea class="form-control" id="exampleTextarea" name="texto"  rows="6" require><?= $noticia->getTexto() ?></textarea>
            <?= isset($errors["texto"])?$errors["texto"]:"" ?>
        </div>
      
      <div class="form-row">
          <div class="col">
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Modificar</button>
          </div>
          <div class="col-sm-5">
            <a href="index.php?controller=noticias&amp;action=index"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
          </div>
        </div>
      </div>
    </form>

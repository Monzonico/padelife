<?php
$view = ViewManager::getInstance();
if($_SESSION){
$username = $_SESSION["currentusername"];
$userrole = $_SESSION["currentuserrole"];
$userid = $_SESSION["currentuserid"];
}
$timezone = date_default_timezone_get();
$date = date('d/m/Y h:i:s a', time());
?>

<!DOCTYPE html>
<html lang="en">

  <head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="img/favicon.png">
  <title><?= $view->getVariable("title", "title") ?></title>
  <!-- Fonts and icons -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="css/black-dashboard.css?v=1.0.0" rel="stylesheet" />
  <!-- JQuery -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/jquery/perfect-scrollbar.jquery.min.js"></script>
  <!-- Popper -->
  <script src="vendor/popper/popper.min.js"></script>
  <!-- Moment.js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
  <!-- Bootstrap -->
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap-notify.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

  <!-- Chart JS -->
  <script src="vendor/chartjs/Chart.bundle.min.js"></script>
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  </head>

<?php if($_SESSION) :?>

<body class="">
  <div class="wrapper">
    <div class="sidebar">

      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
      <div class="sidebar-wrapper">
        <div class="logo">
          <a class="simple-text logo-mini">
          <?= $userrole ?>
          </a>
          <a class="simple-text logo-normal">
          <?= $username ?>
          </a>
        </div>
        <ul class="nav">
          <li>
            <a href="index.php?controller=noticias&amp;action=index">
              <i class="fas fa-newspaper"></i>
              <p>Noticias</p>
            </a>
          </li>
          <li>
            <a href="index.php?controller=campeonatos&amp;action=index">
              <i class="fas fa-trophy"></i>
              <p>Campeonatos</p>
            </a>
          </li>
          <li>
            <a href="index.php?controller=pistas&amp;action=partido">
              <i class="fas fa-handshake"></i>
              <p>Partidos</p>
            </a>
          </li>
          <li>
            <a href="index.php?controller=pistas&amp;action=clases">
              <i class="fas fa-chalkboard-teacher"></i>
              <p>Clases</p>
            </a>
          </li>
           <?php if($_SESSION['currentuserrole'] === "administrador"):?>
           <li>
            <a href="index.php?controller=usuarios&amp;action=index">
              <i class="fas fa-users"></i>
              <p>Usuarios</p>
            </a>
          </li>
          <?php endif ;?>
          <?php if($_SESSION['currentuserrole'] === "administrador"):?>
          <li>
            <a href="index.php?controller=pistas&amp;action=index">
              <i class="fas fa-layer-group"></i>
              <p>Pistas</p>
            </a>
          </li>
          <?php endif ;?>
          <li>
            <a href="index.php?controller=pistas&amp;action=reserva">
              <i class="far fa-calendar-alt"></i>
              <p>Reservas</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle d-inline">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:void(0)">Padelife</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav ml-auto">
              <li class="dropdown nav-item">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                  <div class="photo">
                    <img src="img/anime3.png" alt="Profile Photo">
                  </div>
                  <b class="caret d-none d-lg-block d-xl-block"></b>
                  <p class="d-lg-none">
                    Logout
                  </p>
                </a>
                <ul class="dropdown-menu dropdown-navbar">
                  <li class="nav-link">
                    <a href="index.php?controller=usuarios&amp;action=logout" class="nav-item dropdown-item">Cerrar Sesion</a>
                  </li>
                </ul>
              </li>
              <li class="separator d-lg-none"></li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="card">
          <?= $view->getFragment(ViewManager::DEFAULT_FRAGMENT) ?>
      </div>
    </div>
    </div>
  <?php else :?>
  <div class="content">
        <div class="card">
          <?= $view->getFragment(ViewManager::DEFAULT_FRAGMENT) ?>
      </div>
    </div>
  <?php endif ;?>
      <footer class="footer">
        <div class="container-fluid">
          <div class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script> made with <i class="far fa-heart"></i> by Padelife.
        </div>
      </footer>
    <!-- Datatables -->
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="js/black-dashboard.min.js?v=1.0.0"></script>
  </body>
</html>
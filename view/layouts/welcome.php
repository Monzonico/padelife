<?php
$view = ViewManager::getInstance();
?>

<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

  <head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="">
  <title><?= $view->getVariable("title", "title") ?></title>
  <!-- Fonts and icons -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="css/black-dashboard.css?v=1.0.0" rel="stylesheet" />

  </head>


  <div class="content">
        <div class="card">
          <?= $view->getFragment(ViewManager::DEFAULT_FRAGMENT) ?>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <div class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script> made with <i class="far fa-heart"></i> by Padelife.
        </div>
      </footer>
    </div>

    <!-- JQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/jquery/perfect-scrollbar.jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap-notify.js"></script>
    <!-- Popper -->
    <script src="vendor/popper/popper.min.js"></script>
    <!-- Chart JS -->
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <!-- Datatables -->
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="js/black-dashboard.min.js?v=1.0.0"></script>

</html>


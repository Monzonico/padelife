<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Modificar Usuario");
  $roles= ["administrador","entrenador","deportista"];
  $errors = $view->getVariable("errors");
  $usuario = $view->getVariable("usuario");
?>

<div class="container">
	
<?= isset($errors["general"])?$errors["general"]:"" ?>

        <div class="card-header"><?= $view->getVariable("title") ?></div>
        <div class="card-body">
          <form action="index.php?controller=usuarios&amp;action=update&amp;id=<?= $usuario->getId() ?>" method="POST">
					<div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputName" class="form-control" name="nombre" placeholder="Nombre" value="<?= $usuario->getNombre() ?>" required="required">
								<?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
              </div>
              <div class="form-group">
              <label for="exampleFormControlSelect1"></label>
              <select class="form-control" id="exampleFormControlSelect1" name="rol">
              <?php foreach ($roles as $rol): ?>
                <option><?= $rol ?></option>
              <?php endforeach; ?>
              </select>
              </div>
            </div>
            <div class="form-group">
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" class="form-control" name="contrasena" placeholder="Contraseña" value="<?= $usuario->getContrasena() ?>" required="required">
										<?= isset($errors["contrasena"])?$errors["contrasena"]:"" ?>
                  </div>
              </div>
              <div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email" value="<?= $usuario->getEmail() ?>" required="required">
								<?= isset($errors["email"])?$errors["email"]:"" ?>
              </div>
            </div>
            </div>
            <div class="form-row">
          <div class="col">
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Modificar</button>
          </div>
          <div class="col-sx-2">
            <a href="index.php?controller=usuarios&amp;action=index"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
          </div>
        </div>
          </form>
        </div>
      </div>

<?php 
  //file: view/users/login.php
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Login");
  $errors = $view->getVariable("errors");
?>

	    <div class="container">

      <?= isset($errors["general"])?$errors["general"]:"" ?>
      <h2 class="display-1 text-center mt-4">PADELIFE <i class="fas fa-table-tennis"></i></h2>
      <div class="card card-login mx-auto mt-5 mb-5">
        <div class="card-header">Login</div>
        <div class="card-body">
		  <form action="index.php?controller=usuarios&amp;action=login" method="POST" id="loginForm">
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputName" class="form-control" name="nombre" placeholder="Username" required="required" autofocus="autofocus">
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" name="contrasena" placeholder="Password" required="required">
                <label for="inputPassword"></label>         
                </div>
            </div>
			<a class="btn btn-primary btn-block" href="#" onclick="document.getElementById('loginForm').submit()">Acceder</a>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="index.php?controller=usuarios&amp;action=registro">Registrarse</a>
          </div>
        </div>
      </div>
	</div>
  <div class="mb-4"></div>
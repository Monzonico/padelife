<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Usuarios");
  $errors = $view->getVariable("errors");
  $usuarios = $view->getVariable("usuarios");
  if($_SESSION){
    $username = $_SESSION["currentusername"];
    $userrole = $_SESSION["currentuserrole"];
    }
?>
<?php if ($_SESSION): ?>
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?></h4>
  </div>
  <div class="card-body">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="index.php?controller=usuarios&amp;action=index">Noticias</a>
            </li>
            <li class="breadcrumb-item active"><?= $view->getVariable("title"); ?></li>
          </ol>

          <!-- Page Content -->
          <div class="content">
            <div class="row">
              <div class="col">
                <div class="table-responsive">
                  <table class="table tablesorter " id="">
                    <thead class=" text-primary">
                      <tr>
                        <th class="text-center">
                          Id
                        </th>
                        <th class="text-center">
                          Nombre
                        </th>
                        <th class="text-center">
                          Rol
                        </th>
                        <th class="text-center">
                          Contrasena
                        </th>
                        <th class="text-center">
                          Email
                        </th>
                        <th class="text-center">
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($usuarios as $usuario): ?>
                      <tr>
                        <td class="text-center">
                            <?= $usuario->getId() ?>
                        </td>
                        <td class="text-center">
                            <a href="index.php?controller=usuarios&amp;action=update&amp;id=<?= $usuario->getId() ?>"><?= $usuario->getNombre() ?></a>
                        </td>
                        <td class="text-center">
                            <a href="index.php?controller=usuarios&amp;action=update&amp;id=<?= $usuario->getId() ?>"><?= $usuario->getRol() ?></a>
                        </td>
                        <td class="text-center">
                            <a href="index.php?controller=usuarios&amp;action=update&amp;id=<?= $usuario->getId() ?>"><?= $usuario->getContrasena() ?></a>
                        </td>
                        <td class="text-center">
                            <a href="index.php?controller=usuarios&amp;action=update&amp;id=<?= $usuario->getId() ?>"><?= $usuario->getEmail() ?></a>
                        </td>
                        <td class="text-center">
                            <a href="index.php?controller=usuarios&amp;action=delete&amp;id=<?= $usuario->getId() ?>"><i class="far fa-trash-alt"></i></a>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div> 
        <p class="text-center"><a href="index.php?controller=usuarios&amp;action=add"><i class="fas fa-plus-circle fa-2x"></i></a></p>
</div>
<?php else: ?>
Se requiere Login
<?php endif; ?>
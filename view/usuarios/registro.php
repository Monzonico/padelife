<?php
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Registro");
  $errors = $view->getVariable("errors");
?>
  
<div class="container">
	
<?= isset($errors["general"])?$errors["general"]:"" ?>

      <div class="card card-register mx-auto mt-5 mb-5">
        <div class="card-header">Registrar Cuenta</div>
        <div class="card-body">
          <form action="index.php?controller=usuarios&amp;action=registro" method="POST" id="registerForm">
					<div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputName" class="form-control" name="nombre" placeholder="Username" required="required">
								<?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="required">
								<?= isset($errors["email"])?$errors["email"]:"" ?>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" class="form-control" name="contrasena" placeholder="Contraseña" required="required">
										<?= isset($errors["contrasena"])?$errors["contrasena"]:"" ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="confirmPassword" class="form-control" name="ccontrasena" placeholder="Confirmar contraseña" required="required">
										<label for="confirmPassword"></label>
										<?= isset($errors["ccontrasena"])?$errors["ccontrasena"]:"" ?>
                  </div>
                </div>
              </div>
            </div>
						<a class="btn btn-primary btn-block" href="#" onclick="document.getElementById('registerForm').submit()">Registrar</a>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="index.php?controller=usuarios&amp;action=login">Acceder</a>
          </div>
        </div>
      </div>
    </div>
<?php 
  require_once(__DIR__."/../../core/ViewManager.php");
  $view = ViewManager::getInstance();
  $view->setVariable("title", "Crear Usuario");
  $roles= ["administrador","entrenador","deportista"];
  $errors = $view->getVariable("errors");
?>

<div class="container">
	
<?= isset($errors["general"])?$errors["general"]:"" ?>

        <div class="card-header"><?= $view->getVariable("title") ?></div>
        <div class="card-body">
          <form action="index.php?controller=usuarios&amp;action=add" method="POST">
					<div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputName" class="form-control" name="nombre" placeholder="Nombre" required="required">
								<?= isset($errors["nombre"])?$errors["nombre"]:"" ?>
              </div>
              <div class="form-group">
              <label for="exampleFormControlSelect1"></label>
              <select class="form-control" id="exampleFormControlSelect1" name="rol">
              <?php foreach ($roles as $rol): ?>
                <option><?= $rol ?></option>
              <?php endforeach; ?>
              </select>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" class="form-control" name="contrasena" placeholder="Contraseña" required="required">
										<?= isset($errors["contrasena"])?$errors["contrasena"]:"" ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="confirmPassword" class="form-control" name="ccontrasena" placeholder="Confirmar contraseña" required="required">
										<label for="confirmPassword"></label>
										<?= isset($errors["ccontrasena"])?$errors["ccontrasena"]:"" ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="required">
								<?= isset($errors["email"])?$errors["email"]:"" ?>
              </div>
            </div>
            </div>
            <div class="form-row">
          <div class="col">
            <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Crear</button>
          </div>
          <div class="col-sx-2">
            <a href="index.php?controller=usuarios&amp;action=index"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
          </div>
        </div>
          </form>
        </div>
      </div>

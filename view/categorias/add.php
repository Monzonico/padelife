<?php
require_once(__DIR__ . "/../../core/ViewManager.php");
$view = ViewManager::getInstance();
$view->setVariable("title", "Crear Categoria");
$errors = $view->getVariable("errors");
echo "<script>console.log( 'Debug idcamp: " . $_REQUEST["idcamp"] . "' );</script>";
?>

<div class="container">

  <?= isset($errors["general"]) ? $errors["general"] : "" ?>

  <div class="card-header">
    <h1 class="text-center"><?= $view->getVariable("title") ?></h1>
  </div>
  <div class="card-body">
    <form action="index.php?controller=categorias&amp;action=add" method="POST">

    <input id="campId" name="idcamp" type="hidden" value="<?= $_REQUEST["idcamp"] ?>">

      <div class="form-group mb-5">
        <label for="nombre">Nombre</label>
        <select  id="inputNombre" class="form-control" name="nombre" placeholder="Nombre"required="required" aria-describedby="nombre" >
        <option value="Masculino">Masculino</option>
        <option value="Femenino">Femenino</option>
        <option value="Mixto">Mixto</option>
        </select>
        <small id="nombre" class="form-text text-muted">Nombre de la Categoria.</small>
        <?= isset($errors["nombre"]) ? $errors["nombre"] : "" ?>
        <br>
        <label for="nombre">Nivel</label>
        <select  id="inputNivel" class="form-control" name="nivel" placeholder="Nivel"required="required" aria-describedby="nivel" >
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        </select>
        <small id="nivel" class="form-text text-muted">Nive de la Categoria.</small>
        <?= isset($errors["nivel"]) ? $errors["nivel"] : "" ?>
      </div>
      <div class="form-row">
        <div class="col">
          <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Crear</button>
        </div>
        <div class="col-sx-2">
          <a href="index.php?controller=categorias&amp;action=index&amp;idcamp=<?= $_REQUEST["idcamp"] ?>"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
        </div>
      </div>
  </div>

  </form>
</div>
</div>
<?php
require_once(__DIR__ . "/../../core/ViewManager.php");
$view = ViewManager::getInstance();
$view->setVariable("title", "Calendario Campeonato");
$errors = $view->getVariable("errors");
$campeonato = $view->getVariable("campeonato");
$categoria = $view->getVariable("categoria");
$enfrentamientos = $view->getVariable("enfrentamientos");
if ($_SESSION) {
  $userrole = $_SESSION["currentuserrole"];
}
?>

<?php if ($_SESSION) : ?>
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title") ?> : <?= $categoria->getNombre() ?> [ <?= $categoria->getNivel() ?> ] , <?= $campeonato->getNombre() ?></h4>
  </div>
  <div class="card-body">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php?controller=noticias&amp;action=index">Noticias</a>
      </li>
      <li class="breadcrumb-item">
        <a href="index.php?controller=campeonatos&amp;action=index">Campeonatos</a>
      </li>
      <li class="breadcrumb-item">
        <a href="index.php?controller=categorias&amp;action=index&amp;idcamp=<?= $campeonato->getId() ?>"><?= $campeonato->getNombre() ?></a>
      </li>
      <li class="breadcrumb-item active"><?= $categoria->getNombre() ?> [ <?= $categoria->getNivel() ?> ]</li>
    </ol>

    <?php if ($enfrentamientos != NULL) : ?>
      <!-- Page Content -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
                  <tr>
                  <th class="text-center">
                        Pista
                    </th>
                    <th class="text-center">
                        Fecha
                    </th>
                    <th class="text-center">
                        Hora
                    </th>
                    <th class="text-center">
                      Pareja_1
                    </th>
                    <th class="text-center">
                      Pareja_2
                    </th>
                      <th class="text-center">
                        Set_1
                      </th>
                      <th class="text-center">
                        Set_2
                      </th>
                      <th class="text-center">
                        Set_3
                      </th>
                      <th class="text-center">
                        Resultado
                      </th>
                      <th class="text-center">
                        Modalidad
                      </th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($enfrentamientos as $enfrentamiento) : ?>
                    <tr>
                      <td class="text-center">
                      <?= $enfrentamiento->getId_Pista() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getFecha() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getId_Hora() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getId_Pareja1() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getId_Pareja2() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getSet_1() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getSet_2() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getSet_3() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getResultado() ?>
                      </td>
                      <td class="text-center">
                      <?= $enfrentamiento->getTipo() ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php else : ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-dog"></i> Actualmente no existen Enfrentamientos para esta Categoria (Modificar este else limitando acceso por enlace)</li>
    </ol>
  <?php endif; ?>

      <div class="card-body">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><i class="fas fa-info-circle"></i> &nbsp&nbsp&nbsp&nbsp La tabla muestra el calendario junto a los resultados obtenidos en la fase Campeonato del Torneo correspondientes a la categoria (<?= $categoria->getNombre() ?> Nivel: <?= $categoria->getNivel() ?>)</li>
        </ol>
      </div>

<?php else : ?>
  Se requiere Login
<?php endif; ?>
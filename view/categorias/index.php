<?php
require_once(__DIR__ . "/../../core/ViewManager.php");
$view = ViewManager::getInstance();
$view->setVariable("title", "Categorias");
$errors = $view->getVariable("errors");
$categorias = $view->getVariable("categorias");
$campeonato = $view->getVariable("campeonato");
$inscritos = $view->getVariable("inscritos");
$regularexist = $view->getVariable("regularexist");
$campexist = $view->getVariable("campexist");
$camp_semi_exist = $view->getVariable("camp_semi_exist");
$camp_final_exist = $view->getVariable("camp_final_exist");
$camp_final_full = $view->getVariable("camp_final_full");
$valido = $view->getVariable("valido");
$meincat = $view->getVariable("meincat");
$catfull = $view->getVariable("catfull");
if ($_SESSION) {
  $userrole = $_SESSION["currentuserrole"];
}
?>
<?php if ($_SESSION) : ?>

  <!--Valido: <?= $valido ?>/Regular_Exist: <?= $regularexist ?>/Camp_Exist: <?= $campexist ?>/Camp_Semi_Exist: <?= $camp_semi_exist ?>/Camp_Final_Exist: <?= $camp_final_exist ?>/Camp_Final_Full: <?= $camp_final_full ?>-->
  <div class="card-header">
    <h4 class="card-title"><?= $view->getVariable("title"); ?> del Campeonato: <?= $campeonato->getNombre() ?></h4>
  </div>
  <div class="card-body">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php?controller=noticias&amp;action=index">Home</a>
      </li>
      <li class="breadcrumb-item">
        <a href="index.php?controller=campeonatos&amp;action=index">Campeonatos</a>
      </li>
      <li class="breadcrumb-item active"><?= $campeonato->getNombre() ?></li>
    </ol>

    <?php if ($categorias != NULL) : ?>
      <!-- Page Content -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
                  <tr>
                    <th class="text-center">
                      Nombre
                    </th>
                    <th class="text-center">
                      Nivel
                    </th>
                    <?php if ($regularexist) : ?>
                       <?php if (!$campexist) : ?>
                      <th class="text-center">
                        Calendario - Fase Regular
                      </th>
                      <?php else : ?>
                        <th class="text-center">
                        Resultados - Fase Regular
                      </th>
                      <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($campexist) : ?>
                      <?php if (!$camp_semi_exist) : ?>
                      <th class="text-center">
                        Calendario - Fase Campeonato
                      </th>
                      <?php else : ?>
                        <th class="text-center">
                        Resultados - Fase Campeonato
                      </th>
                      <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($camp_semi_exist) : ?>
                      <?php if (!$camp_final_exist) : ?>
                      <th class="text-center">
                        Calendario - Semifinales Campeonato
                      </th>
                      <?php else : ?>
                        <th class="text-center">
                        Resultados - Semifinales Campeonato
                      </th>
                      <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($camp_final_exist) : ?>
                      <?php if (!$camp_final_full) : ?>
                      <th class="text-center">
                        Calendario - Final Campeonato
                      </th>
                      <?php else : ?>
                        <th class="text-center">
                        Resultados - Final Campeonato
                      </th>
                      <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($userrole == "administrador") : ?>
                      <th class="text-center">
                        Editar
                      </th>
                    <?php else : ?>
                      <?php if (!$regularexist) : ?>
                        <th class="text-center">
                          Incribirse
                        </th>
                      <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($userrole == "administrador") : ?>
                      <th class="text-center">
                        Eliminar
                      </th>
                    <?php endif; ?>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($categorias as $index=>$categoria) : ?>
                    <tr>
                      <td class="text-center">
                        <?= $categoria->getNombre() ?>
                      </td>
                      <td class="text-center">
                        <?= $categoria->getNivel() ?>
                      </td>
                      <?php if ($regularexist) : ?>
                      <th class="text-center">
                        <a href="index.php?controller=categorias&amp;action=index_r&amp;id_categoria=<?= $categoria->getId() ?>"><i class="fas fa-list-ul fa-2x"></i></a>
                      </th>
                    <?php endif; ?>
                    <?php if ($campexist) : ?>
                      <th class="text-center">
                        <a href="index.php?controller=categorias&amp;action=index_c&amp;id_categoria=<?= $categoria->getId() ?>"><i class="fas fa-list-ol fa-2x"></i></a>
                      </th>
                    <?php endif; ?>
                    <?php if ($camp_semi_exist) : ?>
                      <th class="text-center">
                        <a href="index.php?controller=categorias&amp;action=index_s&amp;id_categoria=<?= $categoria->getId() ?>"><i class="fas fa-medal fa-2x"></i></a>
                      </th>
                    <?php endif; ?>
                    <?php if ($camp_final_exist) : ?>
                      <th class="text-center">
                        <a href="index.php?controller=categorias&amp;action=index_f&amp;id_categoria=<?= $categoria->getId() ?>"><i class="fas fa-trophy fa-2x"></i></a>
                      </th>
                    <?php endif; ?>
                      <?php if ($userrole == "administrador") : ?>
                        <td class="text-center">
                          <a href="index.php?controller=categorias&amp;action=update&amp;idcategoria=<?= $categoria->getId() ?>"><i class="fas fa-edit"></i></a>
                        </td>
                        <?php else : ?>
                        <?php if (!$regularexist) : ?>
                         <?php if(!$meincat[$index] && !$catfull[$index]) : ?>
                          <td class="text-center">
                            <a href="index.php?controller=parejas&amp;action=add&amp;idcamp=<?= $campeonato->getId() ?>&amp;idcategoria=<?= $categoria->getId() ?>"><i class="fas fa-user-plus"></i></a>
                          </td>
                         <?php else : ?>
                          <td class="text-center">
                              <i class="fas fa-user-slash"></i>
                          </td>
                        <?php endif; ?>
                      <?php endif; ?>
                      <?php endif; ?>
                      <?php if ($userrole == "administrador") : ?>
                        <td class="text-center">
                          <a href="index.php?controller=categorias&amp;action=delete&amp;idcategoria=<?= $categoria->getId() ?>"><i class="far fa-trash-alt"></i></a>
                        </td>
                      <?php endif; ?>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php if ($userrole == "administrador" && !$regularexist) : ?>
        <p class="text-center"><a href="index.php?controller=categorias&amp;action=add&amp;idcamp=<?= $campeonato->getId() ?>"><i class="fas fa-plus-circle fa-2x"></i></a></p>
      <?php endif; ?>
    </div>
  <?php else : ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-dog"></i> Actualmente no existen Categorias para este Campeonato</li>
    </ol>
    <?php if ($userrole == "administrador" && !$regularexist) : ?>
      <p class="text-center"><a href="index.php?controller=categorias&amp;action=add&amp;idcamp=<?= $campeonato->getId() ?>"><i class="fas fa-plus-circle fa-2x"></i></a></p>
    <?php endif; ?>
  <?php endif; ?>

  <?php if ($userrole != "administrador" && $userrole != "entrenador") : ?>
  <div class="card-header">
    <h4 class="card-title">Inscripciones Activas en este Campeonato</h4>
  </div>
  <div class="card-body">

    <?php if ($inscritos != NULL) : ?>
      <!-- Page Content -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table tablesorter " id="">
                <thead class=" text-primary">
                  <tr>
                    <th class="text-center">
                      Nombre
                    </th>
                    <th class="text-center">
                      Nivel
                    </th>
                    <th class="text-center">
                      Pareja
                    </th>
                    <?php if (!$regularexist) : ?>
                    <th class="text-center">
                      Cancelar
                    </th>
                    <?php endif; ?>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach ($inscritos as $inscrito) : ?>
                    <tr>
                      <td class="text-center">
                      <?= $inscrito->getNombre()?>
                      </td>
                      <td class="text-center">
                      <?= $inscrito->getNivel()?>
                      </td>
                      <?php if ($inscrito->getCapitan() == $_SESSION["currentusername"]) : ?>
                      <td class="text-center">
                      <?= $inscrito->getPareja()?>
                      </td>
                      <?php else : ?>
                      <td class="text-center">
                      <?= $inscrito->getCapitan()?>
                      </td>
                      <?php endif; ?>
                      <?php if (!$regularexist) : ?>
                      <td class="text-center">
                      <a href="index.php?controller=categorias&amp;action=deleteCat&amp;id_categoria=<?= $inscrito->getIdCategoria() ?>&amp;nombre_capitan=<?= $inscrito->getCapitan() ?>&amp;nombre_pareja=<?= $inscrito->getPareja() ?>&amp;id_campeonato=<?= $_REQUEST["idcamp"] ?>"><i class="fas fa-user-minus"></i></a>
                      </td>
                      <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php else : ?>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-kiwi-bird"></i> &nbsp&nbsp&nbsp&nbsp Actualmente no tienes inscripciones activas para este campeonato</li>
    </ol>
    </div>
    <?php endif; ?>    
    <?php endif; ?>


    <?php if ($userrole == "administrador") : ?>
        <?php if ($valido && !$regularexist && $categorias!=NULL) : ?>




                  <div class="card-header">
                        <h4 class="card-title">Lanzar Calendario Regular Campeonato</h4>
                  </div>
                  <div class="card-body">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Al pulsar en el icono se simulara el calendario de la fase Regular del Campeonato (Efectuando prereservas en para la fase Regular)</li>
                        </ol>
                        <p class="text-center"><a href="index.php?controller=categorias&amp;action=creaRegular&amp;id_campeonato=<?= $_REQUEST["idcamp"] ?>"><i class="fas fa-calendar-alt fa-2x"></i></a></p>
        <?php elseif($valido && $regularexist) : ?>                            
            <?php if ($regularexist && !$campexist) : ?>
                              <div class="card-header">
                                  <h4 class="card-title">Simular Resultados fase Regular y lanzar Calendario de Campeonato</h4>
                              </div>
                              <div class="card-body">
                                  <ol class="breadcrumb">
                                      <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Al pulsar en el icono se simularan los resultados de la fase Regular del Campeonato efectuando sus reservas y se generara el calendario de fase Campeonato del Torneo (Efectuando prereservas para la fase Campeonato)</li>
                                  </ol>
                                  <p class="text-center"><a href="index.php?controller=categorias&amp;action=simulaRegular&amp;id_campeonato=<?= $_REQUEST["idcamp"] ?>"><i class="fas fa-poll-h fa-2x"></i></a></p>
                  <?php elseif($regularexist && $campexist && !$camp_semi_exist && !$camp_final_exist) : ?>
                                    <div class="card-header">
                                        <h4 class="card-title">Simular Resultados Torneo Campeonato y lanzar Calendario de Semifinales</h4>
                                        </div>
                                         <div class="card-body">
                                      <ol class="breadcrumb">
                                          <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Al pulsar en el icono se simularan los resultados de las Semifinales del Campeonato efectuando sus prereservas ()</li>
                                      </ol>
                                      <p class="text-center"><a href="index.php?controller=categorias&amp;action=simulaCamp&amp;id_campeonato=<?= $_REQUEST["idcamp"] ?>"><i class="fas fa-calendar-alt fa-2x"></i></a></p>
                  <?php elseif ($regularexist && $campexist && $camp_semi_exist && !$camp_final_exist) : ?>
                                                  <div class="card-header">
                                                    <h4 class="card-title">Simular Resultados Semifinales y lanzar Calendario de Final</h4>
                                                          </div>
                                                          <div class="card-body">
                                                          <ol class="breadcrumb">
                                                                 <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Al pulsar en el icono se simularan los resultados de las Semifinales del Campeonato efectuando sus prereservas ()</li>
                                                                  </ol>
                                                  <p class="text-center"><a href="index.php?controller=categorias&amp;action=simulaSemi&amp;id_campeonato=<?= $_REQUEST["idcamp"] ?>"><i class="fas fa-poll-h fa-2x"></i></a></p>
                  <?php elseif ($regularexist && $campexist && $camp_semi_exist && $camp_final_exist && !$camp_final_full) : ?>
                                            <div class="card-header">
                                                    <h4 class="card-title">Simular Resultados Final</h4>
                                                          </div>
                                                          <div class="card-body">
                                                          <ol class="breadcrumb">
                                                                 <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Al pulsar en el icono se simularan los resultados de las Semifinales del Campeonato efectuando sus prereservas ()</li>
                                                                  </ol>
                                                  <p class="text-center"><a href="index.php?controller=categorias&amp;action=simulaFinal&amp;id_campeonato=<?= $_REQUEST["idcamp"] ?>"><i class="fas fa-poll-h fa-2x"></i></a></p>
                  <?php elseif ($regularexist && $campexist && $camp_semi_exist && $camp_final_exist && $camp_final_full) : ?>
                                                          <div class="card-body">
                                                          <ol class="breadcrumb">
                                                                 <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Este Campeonato ha finalizado, puede consultar los resultados en la tabla de arriba</li>
                                                          </ol>

              <?php endif; ?>
          <?php else : ?>
                              <?php if($categorias != NULL) : ?>
                                            <div class="card-header">
                                                   <h4 class="card-title">Lanzar Calendario Regular Campeonato</h4>
                                               </div>
                                             <div class="card-body">
                                                     <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><i class="fas fa-exclamation-triangle"></i> &nbsp&nbsp&nbsp&nbsp Se necesita un minimo de entre 8-12 parejas por categoria para simular el calendario de la fase Regular del Campeonato</li>
                                </ol>
                            </div>
                            <?php endif; ?>

        <?php endif; ?>
    <?php endif; ?>



<?php else : ?>
    Se requiere Login
<?php endif; ?>
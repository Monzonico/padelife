<?php
require_once(__DIR__ . "/../../core/ViewManager.php");
$view = ViewManager::getInstance();
$view->setVariable("title", "Modificar Categoria");
$errors = $view->getVariable("errors");
$categoria = $view->getVariable("categoria");

?>

<div class="container">

  <?= isset($errors["general"]) ? $errors["general"] : "" ?>

  <div class="card-header">
    <h1 class="text-center"><?= $view->getVariable("title") ?></h1>
  </div>
  <div class="card-body">
    <form action="index.php?controller=categorias&amp;action=update&amp;idcategoria=<?= $categoria->getId() ?>" method="POST">

      <div class="form-group mb-5">
      <label for="nombre">Nombre</label>
        <select  id="inputNombre" class="form-control" name="nombre" placeholder="Nombre"required="required" aria-describedby="nombre" >
          <option value="Masculino"  <?php if($categoria->getNombre()=="Masculino") :?>selected<?php endif;?>>Masculino</option>
          <option value="Femenino"<?php if($categoria->getNombre()=="Femenino") :?>selected<?php endif;?>>Femenino</option>
          <option value="Mixto"<?php if($categoria->getNombre()=="Mixto") :?>selected<?php endif;?>>Mixto</option>
        </select>
        <small id="nombre" class="form-text text-muted">Nombre de la Categoria.</small>
        <?= isset($errors["nombre"]) ? $errors["nombre"] : "" ?>
      </div>

      <div class="form-row">
        <div class="col">
          <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Modificar</button>
        </div>
        <div class="col-sx-2">
          <a href="index.php?controller=categorias&amp;action=index&amp;idcamp=<?= $categoria->getId_Campeonato() ?>"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
        </div>
      </div>
  </div>
  </form>
</div>
</div>
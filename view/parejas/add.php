<?php
require_once(__DIR__ . "/../../core/ViewManager.php");
$view = ViewManager::getInstance();
$view->setVariable("title", "Inscribir Pareja");
$errors = $view->getVariable("errors");
?>

<div class="container">
    <div class="card-header">
        <h1 class="text-center"><?= $view->getVariable("title") ?></h1>
    </div>
    <div class="card-body">
        <form action="index.php?controller=parejas&amp;action=add" method="POST">

            <input id="campId" name="idcamp" type="hidden" value="<?= $_REQUEST["idcamp"] ?>">
            <input id="catId" name="idcategoria" type="hidden" value="<?= $_REQUEST["idcategoria"] ?>">
            <input id="userId" name="idcapitan" type="hidden" value="<?= $_SESSION["currentuserid"] ?>">

            <div class="form-group mb-5">
                <label for="nombre">Nombre Capitan</label>
                <input type="text" id="inputNombre" class="form-control" name="nombrecapitan" value="<?= $_SESSION["currentusername"] ?>" placeholder="Nombre" aria-describedby="nombre" readonly>
                <small id="nombre" class="form-text text-muted">Usted figurara como capitan en la Pareja.</small>
            </div>

            <div class="form-group my-5">
                <label for="normativa">Nombre Pareja</label>
                <input type="text" id="inputNombre" class="form-control" name="nombrejugador" placeholder="Nombre" aria-describedby="nombre" required="required">
                <small id="normativa" class="form-text text-muted">Introduzca el nombre de usuario de su Pareja.</small>
                <?= isset($errors["general"]) ? $errors["general"] : "" ?>
            </div>
            <?= isset($errors["pareja"]) ? $errors["pareja"] : "" ?>

            <div class="form-row">
                <div class="col">
                    <button class="btn btn-lg btn-outline-light btn-block btn-sm" type="submit">Inscribir</button>
                </div>
                <div class="col-sx-2">
                    <a href="index.php?controller=categorias&amp;action=index&amp;idcamp=<?= $_REQUEST["idcamp"] ?>"><button class="btn btn-lg btn-outline-light btn-block btn-sm" type="button">Cancelar</button></a>
                </div>
            </div>
    </div>

    </form>
</div>
</div>
<?php
require_once(__DIR__."/../core/ViewManager.php");
//require_once(__DIR__."/../core/I18n.php");
require_once(__DIR__."/../model/Pareja.php");
require_once(__DIR__."/../model/Inscrito.php");
require_once(__DIR__."/../model/Grupo.php");
require_once(__DIR__."/../model/GrupoMapper.php");
require_once(__DIR__."/../model/ParejaMapper.php");
require_once(__DIR__."/../model/Usuario.php");
require_once(__DIR__."/../model/UsuarioMapper.php");
require_once(__DIR__."/../controller/BaseController.php");



class ParejasController extends BaseController {


	private $ParejaMapper;
	public function __construct() {
		parent::__construct();

		$this->ParejaMapper = new ParejaMapper();
		$this->UsuarioMapper = new UsuarioMapper();
		$this->GrupoMapper = new GrupoMapper();
		$this->view->setLayout("default");
	}

	public function add() {
		$pareja = new Pareja();
		if (isset($_POST["idcategoria"]) && isset($_POST["idcapitan"]) && isset($_POST["nombrejugador"])){

			$pareja->set_IdCategoria($_POST["idcategoria"]);
			$pareja->set_IdCapitan($_POST["idcapitan"]);

			try{
					$errors = array();
					$pareja->checkIsValid();

					if ($this->UsuarioMapper->findByUsername($_POST["nombrejugador"])==NULL){
						$errors["general"] = "El usuario no existe";
						throw new ValidationException($errors, "");
					}else if($this->UsuarioMapper->findByUsername($_POST["nombrejugador"])->getId()==$_SESSION["currentuserid"]){
						$errors["pareja"] = "Emparejamiento no valido";
						throw new ValidationException($errors, "");
					}else if($this->ParejaMapper->isParejaExist($_POST["idcategoria"], $_POST["idcapitan"],$this->UsuarioMapper->findByUsername($_POST["nombrejugador"])->getId()) || $this->ParejaMapper->isParejaExist($_POST["idcategoria"], $this->UsuarioMapper->findByUsername($_POST["nombrejugador"])->getId(), $_POST["idcapitan"])){
						$errors["pareja"] = "La pareja ya se encuentra inscrita en esta categoria";
						throw new ValidationException($errors, "");
					}else if($this->ParejaMapper->isParejaExist2($_POST["idcategoria"], $this->UsuarioMapper->findByUsername($_POST["nombrejugador"])->getId())){
							$errors["pareja"] = "La pareja seleccionada ya se encuentra inscrita en esta categoria";
							throw new ValidationException($errors, "");
					}


					$jugador = $this->UsuarioMapper->findByUsername($_POST["nombrejugador"]);
					$pareja->set_IdJugador($jugador->getId());

					$this->ParejaMapper->save($pareja);

					$this->view->setFlash("Pareja ".$pareja->getId()."  agregada.");

					$this->view->redirect("categorias", "index", "idcamp=" . $_POST["idcamp"] . "");

			}catch(ValidationException $ex) {
				$errors = $ex->getErrors();

				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->render("parejas", "add");
		
	}
		
}

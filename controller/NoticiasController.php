<?php
require_once(__DIR__."/../core/ViewManager.php");
//require_once(__DIR__."/../core/I18n.php");
require_once(__DIR__."/../model/Noticia.php");
require_once(__DIR__."/../model/NoticiaMapper.php");
require_once(__DIR__."/../controller/BaseController.php");



class NoticiasController extends BaseController {


	private $NoticiaMapper;
	public function __construct() {
		parent::__construct();

		$this->NoticiaMapper = new NoticiaMapper();
		$this->view->setLayout("default");
	}

	public function index() {
		$noticias = $this->NoticiaMapper->findAll();
		$this->view->setVariable("noticias", $noticias);
		$this->view->render("noticias", "index");
	}

	public function delete() {
		if (!isset($_REQUEST["id"])) {
			throw new Exception("id noticia is mandatory");
		}
	
		$noticiaID = $_REQUEST["id"];
		$noticia = $this->NoticiaMapper->findById($noticiaID);

		if ($noticia == NULL) {
			throw new Exception("no such noticia with id_noticia: ".$noticiaID);
		}else
		{
			$this->NoticiaMapper->delete($noticia);
			$this->view->setFlash(sprintf("Noticia \"%s\" successfully deleted."),$noticia->getTitulo());	
		}
		$this->view->redirect("noticias", "index");
	}

	public function add() {
		$noticia = new Noticia();
		if (isset($_POST["titulo"]) && isset($_POST["texto"])){

			$noticia->setTitulo($_POST["titulo"]);
			$noticia->setTexto($_POST["texto"]);

			try{
				$noticia->checkIsValid();

					$this->NoticiaMapper->save($noticia);

					$this->view->setFlash("Noticia ".$noticia->getTitulo()."  agregada.");

					$this->view->redirect("noticias", "index");

			}catch(ValidationException $ex) {
				$errors = $ex->getErrors();

				$this->view->setVariable("errors", $errors);
			}
		}

		$this->view->render("noticias", "add");
		
	}

	public function update() {

        if (!isset($_REQUEST["id"])) {
            throw new Exception("Noticia id is mandatory");
        }

        $noticiaId = $_REQUEST["id"];
        $noticia = $this->NoticiaMapper->findById($noticiaId);

        if ($noticia == NULL) {
            throw new Exception("No existe noticia con id: ".$noticiaId);
		}

        if (isset($_POST["titulo"]) && isset($_POST["texto"])) {

          $noticia->setTitulo($_POST["titulo"]);
		  $noticia->setTexto($_POST["texto"]);

            try {
                    $noticia->checkIsValid();
                    $this->NoticiaMapper->update($noticia);
					$this->view->setFlash(sprintf("Noticia \"%s\" successfully updated.",$noticia->getTitulo()));
					$this->view->redirect("noticias", "index");
					
            }catch(ValidationException $ex) {
                $errors = $ex->getErrors();
                $this->view->setVariable("errors", $errors);
            }
        }
        $this->view->setVariable("noticia", $noticia);
        $this->view->render("noticias", "update");
	}
	
}

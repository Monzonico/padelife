<?php
require_once(__DIR__."/../core/ViewManager.php");
//require_once(__DIR__."/../core/I18n.php");
require_once(__DIR__."/../model/Usuario.php");
require_once(__DIR__."/../model/UsuarioMapper.php");
require_once(__DIR__."/../controller/BaseController.php");
/**
* Class UsersController
*
* Controller to login, logout and user registration
*
* @author lipido <lipido@gmail.com>
*/
class UsuariosController extends BaseController {
	/**
	* Reference to the UsuarioMapper to interact
	* with the database
	*
	* @var UsuarioMapper
	*/
	private $UsuarioMapper;
	public function __construct() {
		parent::__construct();
		$this->UsuarioMapper = new UsuarioMapper();
		// Users controller operates in a "welcome" layout
		// different to the "default" layout where the internal
		// menu is displayed
		$this->view->setLayout("default");
	}

	public function login() {
		if (isset($_POST["nombre"])){ // reaching via HTTP Post...
			//process login form
			if ($this->UsuarioMapper->isValidUser($_POST["nombre"], $_POST["contrasena"])) {
				$user = $this->UsuarioMapper->findByUsername($_POST["nombre"]);
				$_SESSION["currentusername"] = $user->getNombre();
				$_SESSION["currentuserid"] = $user->getId();
				$_SESSION["currentuserrole"] = $user->getRol();
				$_SESSION["currentuseremail"] = $user->getEmail();
				
				$this->view->redirect("noticias", "index");
			}else{
				$user = $this->UsuarioMapper->findByUsername($_POST["nombre"]);
				$errors = array();
				$errors["general"] = "Usuario o Password no valido";
				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->render("usuarios", "login");
	}

	public function registro() {
		$user = new Usuario();
		if (isset($_POST["nombre"]) && isset($_POST["email"]) && isset($_POST["contrasena"])){ // reaching via HTTP Post...
			// populate the User object with data form the form
			$user->setNombre($_POST["nombre"]);
			$user->setEmail($_POST["email"]);
			$user->setContrasena($_POST["contrasena"]);
			$user->setRol("deportista");
			try{
				$user->checkIsValidForRegister(); // if it fails, ValidationException
				// check if user exists in the database
				if (!$this->UsuarioMapper->usernameExists($_POST["nombre"])){
					// save the User object into the database
					if($_POST["contrasena"] === $_POST["ccontrasena"]){
					$this->UsuarioMapper->save($user);

					$this->view->setFlash("Usuario ".$user->getNombre()."  agregado. Ya puede hacer Login");

					$this->view->redirect("usuarios", "login");
					}else{
						$errors = array();
						$errors["contrasena"] = "Passwords no coinciden";
						$this->view->setVariable("errors", $errors);
					}
				} else {
					$errors = array();
					$errors["nombre"] = "Usuario ya existe";
					$this->view->setVariable("errors", $errors);
				}
			}catch(ValidationException $ex) {
				// Get the errors array inside the exepction...
				$errors = $ex->getErrors();
				// And put it to the view as "errors" variable
				$this->view->setVariable("errors", $errors);
			}
		}
		// Put the User object visible to the view
		$this->view->setVariable("user", $user);
		// render the view (/view/users/register.php)
		$this->view->render("usuarios", "registro");
	}


	public function add() {
		$user = new Usuario();
		if (isset($_POST["nombre"]) && isset($_POST["email"]) && isset($_POST["contrasena"]) && isset($_POST["rol"])){
			$user->setNombre($_POST["nombre"]);
			$user->setRol($_POST["rol"]);
			$user->setContrasena($_POST["contrasena"]);
			$user->setEmail($_POST["email"]);
			try{
				$user->checkIsValidForRegister(); // if it fails, ValidationException
				// check if user exists in the database
				if (!$this->UsuarioMapper->usernameExists($_POST["nombre"])){
					// save the User object into the database
					if($_POST["contrasena"] === $_POST["ccontrasena"]){
					$this->UsuarioMapper->save($user);

					$this->view->setFlash("Usuario ".$user->getNombre()."  agregado. Ya puede hacer Login");

					$this->view->redirect("usuarios", "index");
					}else{
						$errors = array();
						$errors["contrasena"] = "Passwords no coinciden";
						$this->view->setVariable("errors", $errors);
					}
				} else {
					$errors = array();
					$errors["nombre"] = "Usuario ya existe";
					$this->view->setVariable("errors", $errors);
				}
			}catch(ValidationException $ex) {
				// Get the errors array inside the exepction...
				$errors = $ex->getErrors();
				// And put it to the view as "errors" variable
				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->render("usuarios", "add");
	}

	public function delete() {
		if (!isset($_REQUEST["id"])) {
			throw new Exception("id usuario is mandatory");
		}

		$usuarioID = $_REQUEST["id"];
		$usuario = $this->UsuarioMapper->findById($usuarioID);

		if ($usuario == NULL) {
			throw new Exception("no such usuario with id_usuario: ".$usuarioID);
		}else
		{
			$this->UsuarioMapper->delete($usuario);
			$this->view->setFlash(sprintf("usuario \"%s\" successfully deleted."),$usuario->getNombre());
		}
		$this->view->redirect("usuarios", "index");
	}

	public function update() {

        if (!isset($_REQUEST["id"])) {
            throw new Exception("Usuario id is mandatory");
        }

        $usuarioID = $_REQUEST["id"];
        $usuario = $this->UsuarioMapper->findById($usuarioID);

        if ($usuario == NULL) {
            throw new Exception("No existe usuario con id: ".$usuarioID);
		}

        if (isset($_POST["nombre"]) && isset($_POST["rol"]) && isset($_POST["contrasena"]) && isset($_POST["email"])) {

		  $usuario->setNombre($_POST["nombre"]);
		  $usuario->setRol($_POST["rol"]);
		  $usuario->setContrasena($_POST["contrasena"]);
		  $usuario->setEmail($_POST["email"]);

            try {
					$this->UsuarioMapper->isValidUser($_POST["nombre"], $_POST["contrasena"]);
                    $this->UsuarioMapper->update($usuario);
					$this->view->setFlash(sprintf("Usuario \"%s\" successfully updated.",$usuario->getNombre()));
					$this->view->redirect("usuarios", "index");

            }catch(ValidationException $ex) {
                $errors = $ex->getErrors();
                $this->view->setVariable("errors", $errors);
            }
        }
        $this->view->setVariable("usuario", $usuario);
        $this->view->render("usuarios", "update");
	}


	public function index() {
		$usuarios = $this->UsuarioMapper->findAll();
		$this->view->setVariable("usuarios", $usuarios);
		$this->view->render("usuarios", "index");
	}

	public function logout() {
		session_destroy();
		$this->view->redirect("usuarios", "login");
	}
}
?>
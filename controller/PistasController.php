<?php
require_once(__DIR__."/../core/ViewManager.php");
//require_once(__DIR__."/../core/I18n.php");
require_once(__DIR__."/../model/Pista.php");
require_once(__DIR__."/../model/Reserva.php");
require_once(__DIR__."/../model/ReservaList.php");
require_once(__DIR__."/../model/ReservaEnf.php");
require_once(__DIR__."/../model/Hora.php");
require_once(__DIR__."/../model/Clase.php");
require_once(__DIR__."/../model/Asistente.php");
require_once(__DIR__."/../model/Partido.php");
require_once(__DIR__."/../model/PartidoList.php");
require_once(__DIR__."/../model/PartidoPromo.php");
require_once(__DIR__."/../model/PartidoPromoList.php");
require_once(__DIR__."/../model/Usuario.php");
require_once(__DIR__."/../model/UsuarioMapper.php");
require_once(__DIR__."/../model/PistaMapper.php");
require_once(__DIR__."/../model/Campeonato.php");
require_once(__DIR__."/../model/CampeonatoMapper.php");
require_once(__DIR__."/../controller/BaseController.php");



class PistasController extends BaseController {


	private $PistaMapper;
	public function __construct() {
		parent::__construct();

		$this->PistaMapper = new PistaMapper();
		$this->CampeonatoMapper = new CampeonatoMapper();
		$this->UsuarioMapper = new UsuarioMapper();
		$this->view->setLayout("default");
	}

	function console_log($msg){
		echo "<script type='text/javascript'>alert('$msg');</script>";
	}

	public function index() {
		$pistas = $this->PistaMapper->findAll();
		$this->view->setVariable("pistas", $pistas);
		$this->view->render("pistas", "index");
	}

	public function delete() {
		if (!isset($_REQUEST["id"]) && !isset($_REQUEST["idreserva"])) {
			throw new Exception("id pista o reserva is mandatory");
		}else 
		if(isset($_REQUEST["id"])){

		$pistaID = $_REQUEST["id"];
		echo "<script>console.log( 'controller id pista". $pistaID ." ' );</script>";
		$pista = $this->PistaMapper->findById($pistaID);

		if ($pista == NULL) {
			throw new Exception("no such pista with id_pista: ".$pistaID);
		}else{
			$this->PistaMapper->delete($pista);
			$this->view->setFlash(sprintf("Pista \"%s\" successfully deleted."),$pista->getNombre());
			}
		}else
		if(isset($_REQUEST["idreserva"])){

			$reservaID = $_REQUEST["idreserva"];
			echo "<script>console.log( 'controller id reserva". $reservaID ." ' );</script>";
			$reserva = $this->PistaMapper->findReservabyid($reservaID);
	
			if ($reserva == NULL) {
				throw new Exception("no such reserva with id: ".$reservaID);
			}else{
				$this->PistaMapper->deleteReserva($reserva);
				}
		}
		if(isset($_REQUEST["idreserva"])){
			$this->view->redirect("pistas", "reserva");
		}else{
			$this->view->redirect("pistas", "index");	
		}
	}

	public function clases_delete() {
		if (!isset($_REQUEST["id_reserva"])) {
			throw new Exception("id reserva es necesario");
		}else{
			$this->PistaMapper->deleteClaseByReserva($_REQUEST["id_reserva"]);	
		}
		$this->view->redirect("pistas", "clases");
	}

	public function add() {
		$pista = new Pista();
		if (isset($_POST["nombre"])){

			$pista->setNombre($_POST["nombre"]);


			try{
				$pista->checkIsValid();

					$this->PistaMapper->save($pista);

					$this->view->setFlash("Pista ".$pista->getNombre()."  agregada.");

					$this->view->redirect("pistas", "index");

			}catch(ValidationException $ex) {
				$errors = $ex->getErrors();

				$this->view->setVariable("errors", $errors);
			}
		}

		$this->view->render("pistas", "add");

	}

	public function update() {

        if (!isset($_REQUEST["id"])) {
            throw new Exception("Pista id is mandatory");
        }

        $pistaId = $_REQUEST["id"];
        $pista = $this->PistaMapper->findById($pistaId);

        if ($pista == NULL) {
            throw new Exception("No existe pista con id: ".$pistaId);
		}

        if (isset($_POST["nombre"])) {

          $pista->setNombre($_POST["nombre"]);


            try {
                    $pista->checkIsValid();
                    $this->PistaMapper->update($pista);
					$this->view->setFlash(sprintf("Pista \"%s\" successfully updated.",$pista->getNombre()));
					$this->view->redirect("pistas", "index");

            }catch(ValidationException $ex) {
                $errors = $ex->getErrors();
                $this->view->setVariable("errors", $errors);
            }
        }
        $this->view->setVariable("pista", $pista);
        $this->view->render("pistas", "update");
	}

	public function clases_update() {

		if($this->CampeonatoMapper->findAll() != NULL){
			$camps = $this->CampeonatoMapper->findAll();
			$diasaux = array();
			$fechascamp = array();

			foreach($camps as $camp){

				$ini = new DateTime($camp->getFecha_ini());
				$fin = new Datetime($camp->getFecha_fin());
				$fin = $fin->modify( '+1 day' ); 
				$interval = new DateInterval('P1D');
				$daterange = new DatePeriod($ini, $interval ,$fin);
			   array_push($diasaux,$daterange);
			}

			foreach($diasaux as $i=>$date){
				foreach($diasaux[$i] as $dia){
					array_push($fechascamp,$dia->format('Y-m-d'));
				}
			}

		}
		
		if (!isset($_REQUEST["id_clase"])) {
            throw new Exception("Id clase es necesario");
        }else{

			$clase = $this->PistaMapper->findByIdClase($_REQUEST["id_clase"]);

			if(isset($_POST["nombre"]) && isset($_POST["descripcion"]) && isset($_POST["capacidad"])){

				$clase->setNombre($_POST["nombre"]);
				$clase->setDescripcion($_POST["descripcion"]);
				$clase->setCapacidad($_POST["capacidad"]);

				try {
                    $clase->checkIsValid();
                    $this->PistaMapper->updateClase($clase);
					$this->view->redirect("pistas", "clases");

            	}catch(ValidationException $ex) {
					$errors = $ex->getErrors();
					$errors["general"] = "No se pudo crear la clase";
                	$this->view->setVariable("errors", $errors);
            	}
			}

		}
		$horas = $this->PistaMapper->findHoras();
		$capacidades = array("5","10","15","20");
		$this->view->setVariable("capacidades", $capacidades);
		$this->view->setVariable("clase", $clase);
        $this->view->render("pistas", "clases_update");
	}

	public function reserva() {


		if($this->CampeonatoMapper->findAll() != NULL){
			$camps = $this->CampeonatoMapper->findAll();
			$diasaux = array();
			$fechascamp = array();

			foreach($camps as $camp){

				$ini = new DateTime($camp->getFecha_ini());
				$fin = new Datetime($camp->getFecha_fin());
				$fin = $fin->modify( '+1 day' ); 
				$interval = new DateInterval('P1D');
				$daterange = new DatePeriod($ini, $interval ,$fin);
			   array_push($diasaux,$daterange);
			}

			foreach($diasaux as $i=>$date){
				foreach($diasaux[$i] as $dia){
					array_push($fechascamp,$dia->format('Y-m-d'));
				}
			}

		}

		if (isset($_POST["fecha"]) && isset($_POST["hora"]) && $_POST["fecha"] != null && !$this->PistaMapper->isFullReservas($_SESSION["currentuserid"])) {

			$reservado = false;
			$pistas = $this->PistaMapper->findAll();

			foreach ($pistas as $pista){
				if(!$this->PistaMapper->findReserva($pista->getId(),$this->PistaMapper->findIdhora($_POST["hora"]),$_POST["fecha"]) && !$reservado){

				$reserva = new Reserva();
				$reserva->setId_pista($pista->getId());
				$reserva->setId_usuario($_SESSION["currentuserid"]);
				$reserva->setId_hora($this->PistaMapper->findIdhora($_POST["hora"]));
				$reserva->setFecha($_POST["fecha"]);
				$this->PistaMapper->saveReserva($reserva);
				$reservado = true;
					echo "<script>console.log( 'no existe la reserva ' );</script>";
				}else{
					echo "<script>console.log( 'existe la reserva ' );</script>";
				}
			}
		}
		$horas = $this->PistaMapper->findHoras();
		$reservas = $this->PistaMapper->reservasByid($_SESSION["currentuserid"]);
		$this->view->setVariable("horas", $horas);
		$this->view->setVariable("fechascamp", $fechascamp);
		$this->view->setVariable("reservas", $reservas);
		$this->view->render("pistas", "reserva");
	}

	public function partido() {

		if($this->CampeonatoMapper->findAll() != NULL){
			$camps = $this->CampeonatoMapper->findAll();
			$diasaux = array();
			$fechascamp = array();

			foreach($camps as $camp){

				$ini = new DateTime($camp->getFecha_ini());
				$fin = new Datetime($camp->getFecha_fin());
				$fin = $fin->modify( '+1 day' ); 
				$interval = new DateInterval('P1D');
				$daterange = new DatePeriod($ini, $interval ,$fin);
			   array_push($diasaux,$daterange);
			}

			foreach($diasaux as $i=>$date){
				foreach($diasaux[$i] as $dia){
					array_push($fechascamp,$dia->format('Y-m-d'));
				}
			}

		}

        if (isset($_REQUEST["id"]) && !$this->PistaMapper->isFullReservas($_SESSION["currentuserid"])) {

			$partiserva = new Partido();
			$partiserva = $this->PistaMapper->PrereservasById($_REQUEST["id"]);

			$prereserva = new Partido();
			$prereserva->setId_usuario($_SESSION["currentuserid"]);
			$prereserva->setId_hora($partiserva->getId_hora());
			$prereserva->setId_pista($partiserva->getId_pista());
			$prereserva->setFecha($partiserva->getFecha());
			$this->PistaMapper->savePrereserva($prereserva); 
			
			if($this->PistaMapper->partidoIsFull($prereserva->getId_hora(),$prereserva->getId_pista(),$prereserva->getFecha())){
				echo "<script>console.log( ' El partido acaba de llegar a 4 o mas tuplas ' );</script>";
				if(!$this->PistaMapper->findReserva($prereserva->getId_pista(),$prereserva->getId_hora(),$prereserva->getFecha())){
					$reserva = new Reserva();
					$reserva->setId_pista($prereserva->getId_pista());
					$reserva->setId_usuario($prereserva->getId_usuario());
					$reserva->setId_hora($prereserva->getId_hora());
					$reserva->setFecha($prereserva->getFecha());
					echo "<script>console.log( ' Se efectua la reserva de ". $reserva->getId_hora(). $reserva->getId_pista() . $reserva->getFecha() ."' );</script>";
					$reservid = $this->PistaMapper->saveReserva($reserva);
					echo "<script>console.log( ' Se promociona el partido ' );</script>";
					$result = $this->PistaMapper->findIdPrereservas($prereserva->getId_hora(),$prereserva->getId_pista(),$prereserva->getFecha());
					$partidopromo = new PartidoPromo();
					$partidopromo->setId_reserva($reservid);
					$partidopromo->setId_pista($prereserva->getId_pista());
					$partidopromo->setId_hora($prereserva->getId_hora());
					$partidopromo->setId_usuario1($result[0]);
					$partidopromo->setId_usuario2($result[1]);
					$partidopromo->setId_usuario3($result[2]);
					$partidopromo->setId_usuario4($result[3]);
					$partidopromo->setFecha($prereserva->getFecha());
					$this->PistaMapper->savePartido($partidopromo);
				}
				echo "<script>console.log( ' Se eliminan los partido en promocion' );</script>";
				$this->PistaMapper->deletePrereservas($prereserva->getId_hora(),$prereserva->getId_pista(),$prereserva->getFecha());
			}
		}

		if (isset($_POST["fecha"]) && isset($_POST["hora"]) && $_POST["fecha"] != null && !$this->PistaMapper->isFullReservas($_SESSION["currentuserid"])) {

			$reservado = false;
			$pistas = $this->PistaMapper->findAll();

			foreach ($pistas as $pista){
				if(!$this->PistaMapper->findReserva($pista->getId(),$this->PistaMapper->findIdhora($_POST["hora"]),$_POST["fecha"]) && !$reservado){

				$partido = new Partido();
				$partido->setId_usuario($_SESSION["currentuserid"]);
				$partido->setId_hora($this->PistaMapper->findIdhora($_POST["hora"]));
				$partido->setId_pista($pista->getId());
				$partido->setFecha($_POST["fecha"]);
				$this->PistaMapper->savePrereserva($partido);
				$reservado = true;
					echo "<script>console.log( 'no existe la prereserva ' );</script>";
				}else{
					echo "<script>console.log( 'existe la prereserva ' );</script>";
				}
			}
		}
	$horas = $this->PistaMapper->findHoras();
	$mypartidos = $this->PistaMapper->findMyPrereservas($_SESSION["currentuserid"]);
	$promocionados = $this->PistaMapper->findMyPromocionesList($_SESSION["currentuserid"]);
	$partidos = $this->PistaMapper->findAllPartidos();
	$this->view->setVariable("horas", $horas);
	$this->view->setVariable("mypartidos", $mypartidos);
	$this->view->setVariable("promocionados", $promocionados);
	$this->view->setVariable("fechascamp", $fechascamp);
	$this->view->setVariable("partidos", $partidos);
	$this->view->render("pistas", "partido");
}

	public function clases() {
		if($this->PistaMapper->findAllClases() != NULL){
		$allclases = $this->PistaMapper->findAllClases();
		$clases = array();
		$meinclas = array();
		$clasfull = array();

		if($this->PistaMapper->MeInInscritos($_SESSION["currentuserid"])){
		$inscritos = $this->PistaMapper->findMyInscritos($_SESSION["currentuserid"]);
		$inscri_txt = array();

		foreach($inscritos as $inscri){
			$inscriclas = new Clase();
			$inscriclas->setId($inscri->getId());
			$inscriclas->setReserva($inscri->getReserva());
			$inscriclas->setEntrenador($this->UsuarioMapper->findById($inscri->getEntrenador())->getNombre());
			$inscriclas->setPista($this->PistaMapper->findById($inscri->getPista())->getNombre());
			$inscriclas->setHora($this->PistaMapper->findHorasById($inscri->getHora())->getHora_ini());
			$inscriclas->setFecha($inscri->getFecha());
			$inscriclas->setNombre($inscri->getNombre());
			$inscriclas->setDescripcion($inscri->getDescripcion());
			$inscriclas->setCapacidad($inscri->getCapacidad());
			array_push($inscri_txt,$inscriclas);
		}
		$this->view->setVariable("inscri_txt", $inscri_txt);
		}

		foreach($allclases as $clase){

			
			if($this->PistaMapper->isFullClase($clase)){
				array_push($clasfull,TRUE);
			}else{
				array_push($clasfull,FALSE);
			}

			if($this->PistaMapper->isClasExist($clase->getId(),$_SESSION["currentuserid"])){
				array_push($meinclas,TRUE);
			}else{
				array_push($meinclas,FALSE);
			}

				$nuevaclase = new Clase();
				$nuevaclase->setId($clase->getId());
				$nuevaclase->setReserva($clase->getReserva());
				$nuevaclase->setEntrenador($this->UsuarioMapper->findById($clase->getEntrenador())->getNombre());
				$nuevaclase->setPista($this->PistaMapper->findById($clase->getPista())->getNombre());
				$nuevaclase->setHora($this->PistaMapper->findHorasById($clase->getHora())->getHora_ini());
				$nuevaclase->setFecha($clase->getFecha());
				$nuevaclase->setNombre($clase->getNombre());
				$nuevaclase->setDescripcion($clase->getDescripcion());
				$nuevaclase->setCapacidad($clase->getCapacidad());
				array_push($clases,$nuevaclase);	
		}
		$this->view->setVariable("clases", $clases);
		$this->view->setVariable("meinclas", $meinclas);
		$this->view->setVariable("clasfull", $clasfull);
	}
		$this->view->render("pistas", "clases");
	}

	
	public function clases_add() {
		if($this->CampeonatoMapper->findAll() != NULL){
			$camps = $this->CampeonatoMapper->findAll();
			$diasaux = array();
			$fechascamp = array();

			foreach($camps as $camp){

				$ini = new DateTime($camp->getFecha_ini());
				$fin = new Datetime($camp->getFecha_fin());
				$fin = $fin->modify( '+1 day' ); 
				$interval = new DateInterval('P1D');
				$daterange = new DatePeriod($ini, $interval ,$fin);
			   array_push($diasaux,$daterange);
			}

			foreach($diasaux as $i=>$date){
				foreach($diasaux[$i] as $dia){
					array_push($fechascamp,$dia->format('Y-m-d'));
				}
			}

		}

		if (isset($_POST["nombre"]) && isset($_POST["descripcion"]) && isset($_POST["fecha"]) && isset($_POST["hora"]) && isset($_POST["capacidad"]) && $_POST["fecha"] != null && !$this->PistaMapper->isFullReservas($_SESSION["currentuserid"])) {

			$reservado = false;
			$pistas = $this->PistaMapper->findAll();

			foreach ($pistas as $pista){
				if(!$this->PistaMapper->findReserva($pista->getId(),$this->PistaMapper->findIdhora($_POST["hora"]),$_POST["fecha"]) && !$reservado){

				$reserva = new Reserva();
				$reserva->setId_pista($pista->getId());
				$reserva->setId_usuario($_SESSION["currentuserid"]);
				$reserva->setId_hora($this->PistaMapper->findIdhora($_POST["hora"]));
				$reserva->setFecha($_POST["fecha"]);
				$this->PistaMapper->saveReserva($reserva);

				$clase = new Clase();
				$clase->setEntrenador($_SESSION["currentuserid"]);
				$clase->setPista($pista->getId());
				$clase->setHora($this->PistaMapper->findIdhora($_POST["hora"]));
				$clase->setFecha($_POST["fecha"]);
				$clase->setNombre($_POST["nombre"]);
				$clase->setDescripcion($_POST["descripcion"]);
				$clase->setCapacidad($_POST["capacidad"]);
				$clase->setReserva($this->PistaMapper->findReservabyUserAll($clase->getEntrenador(),$clase->getHora(),$clase->getPista(),$clase->getFecha())->getId());
				$this->PistaMapper->saveClase($clase);
				$reservado = true;
				$this->view->redirect("pistas", "clases");
				}else{
					$errors = array();
					$errors["general"] = "No se pudo crear la clase";
					$this->view->setVariable("errors", $errors);
				}
			}
		}

		$horas = $this->PistaMapper->findHoras();
		$capacidades = array("5","10","15","20");
		$this->view->setVariable("horas", $horas);
		$this->view->setVariable("capacidades", $capacidades);
		$this->view->setVariable("fechascamp", $fechascamp);
		$this->view->render("pistas", "clases_add");
	}

	public function asist_add() {

		$asistente = new Asistente();

		if (isset($_REQUEST["id_clase"])){

			$asistente->setId_Clase($_REQUEST["id_clase"]);
			$asistente->setId_Usuario($_SESSION["currentuserid"]);

			try{
				$asistente->checkIsValid();

					$this->PistaMapper->saveAsist($asistente);

					$this->view->redirect("pistas", "clases");

			}catch(ValidationException $ex) {
				$errors = $ex->getErrors();

				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->render("pistas", "clases");
	}

	public function asist_delete() {

		if (!isset($_REQUEST["id_clase"])) {
			throw new Exception("id clase es necesario");
		}else{
			$this->PistaMapper->deleteInscriByClase($_REQUEST["id_clase"],$_SESSION["currentuserid"]);	
		}
		$this->view->redirect("pistas", "clases");
	}
	
}
?>

<?php
require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../model/Inscrito.php");
require_once(__DIR__ . "/../model/Enfrentamiento.php");
require_once(__DIR__ . "/../model/EnfrentamientoMapper.php");
require_once(__DIR__ . "/../model/Reserva.php");
require_once(__DIR__ . "/../model/Ganador.php");
require_once(__DIR__ . "/../model/ReservaEnfList.php");
require_once(__DIR__ . "/../model/ReservaEnf.php");
require_once(__DIR__ . "/../model/ReservaEnfMapper.php");
require_once(__DIR__ . "/../model/Hora.php");
require_once(__DIR__ . "/../model/Pista.php");
require_once(__DIR__ . "/../model/PistaMapper.php");
require_once(__DIR__ . "/../model/Campeonato.php");
require_once(__DIR__ . "/../model/CampeonatoMapper.php");
require_once(__DIR__ . "/../model/Categoria.php");
require_once(__DIR__ . "/../model/CategoriaMapper.php");
require_once(__DIR__ . "/../model/Pareja.php");
require_once(__DIR__ . "/../model/ParejaMapper.php");
require_once(__DIR__ . "/../model/Usuario.php");
require_once(__DIR__ . "/../model/UsuarioMapper.php");
require_once(__DIR__ . "/../controller/BaseController.php");



class CategoriasController extends BaseController
{
	private $CategoriaMapper;
	public function __construct()
	{
		parent::__construct();

		$this->CategoriaMapper = new CategoriaMapper();
		$this->CampeonatoMapper = new CampeonatoMapper();
		$this->ParejaMapper = new ParejaMapper();
		$this->UsuarioMapper = new UsuarioMapper();
		$this->EnfrentamientoMapper = new EnfrentamientoMapper();
		$this->PistaMapper = new PistaMapper();
		$this->ReservaEnfMapper = new ReservaEnfMapper();

		$this->view->setLayout("default");
	}

	function console_log($msg){
		echo "<script type='text/javascript'>alert('$msg');</script>";
	}

	function array_sort_by(&$arrIni, $col, $order = SORT_DESC){
    	$arrAux = array();
    	foreach ($arrIni as $key=> $row)
    	{
        	$arrAux[$key] = is_object($row) ? $arrAux[$key] = $row->$col : $row[$col];
        	$arrAux[$key] = strtolower($arrAux[$key]);
    	}
    	array_multisort($arrAux, $order, $arrIni);
	}

	  function calc_dias_from($fecha_inicio,$fecha_fin){

		$inicio = date_create($fecha_inicio);
		$fin = date_create($fecha_fin);
		$dias_duracion = date_diff($inicio,$fin)->format("%a");

		$fechas=array();
		$fecha=date('Y-m-d', strtotime($fecha_inicio));
		array_push($fechas,$fecha);

		for($i=1;$i<=$dias_duracion;$i++){
			array_push($fechas,date('Y-m-d', strtotime($fecha_inicio."+".$i." days")));
		}
		return $fechas;
	  }

	function calc_Set(){

		$min=0;
		$max=6;

		$num1=0;
		$num2=0;
		$setarray= array();

		$num1 = mt_rand ($min,$max);
		$num2 = mt_rand ($min,$max);

		if ($num1 > $num2) {
			array_push($setarray,$max ." - ". $num2,0);
		} elseif ($num1 == $num2) {
			array_push($setarray,$max+2 ." - ". $max,0);
		} else {
			array_push($setarray,$num1 ." - ". $max,1);
		}
		
		return $setarray;
	  }

	  function calc_Result($set1,$set2,$set3){

		$result= "";

		if (($set1 == $set2) && ($set1 == $set3) && ($set1 == 0)) {
			$result = "3 - 0";
		} elseif (($set1 == $set2) && ($set1 == $set3) && ($set1 == 1)) {
			$result = "0 - 3";
		} elseif ((($set1 == $set2) && ($set1 < $set3)) || (($set1 < $set2) && ($set1 == $set3)) || (($set1 > $set2) && ($set2 == $set3))) {
			$result = "2 - 1";
		}elseif ((($set1 == $set2) && ($set1 > $set3)) || (($set1 > $set2) && ($set1 == $set3)) || (($set1 < $set2) && ($set2 == $set3))){
			$result = "1 - 2";
		}

		return $result;
	  }

	public function index()
	{

		$min=8;
		$max=12;
		$numparejas=0;
		$valido=TRUE;
		$regularexist=FALSE;
		$campexist=FALSE;
		$camp_semi_exist=FALSE;
		$camp_final_exist=FALSE;
		$camp_final_full=FALSE;
		$meincat = array();
		$catfull = array();

		if (!isset($_REQUEST["idcamp"])) {
			throw new Exception("Campeonato id es necesario");
		}
		
		$campeonato = $this->CampeonatoMapper->findById($_REQUEST["idcamp"]);
		$categorias = $this->CategoriaMapper->findAllByIdCampeonato($campeonato->getId());
		$inscritos = $this->ParejaMapper->findAllInscritos($_SESSION["currentuserid"],$_REQUEST["idcamp"]);
		$lastfinal = $this->EnfrentamientoMapper->findLastEnfTipo($campeonato->getId(),"Final");

		foreach ($categorias as $categoria){
		
			$numparejas = $this->ParejaMapper->countParejasByCat($categoria->getId());
						
			if($numparejas < $min || $numparejas > $max){
				$valido=FALSE;
			} 
			if($numparejas == $max){
				array_push($catfull,TRUE);
			}else{
				array_push($catfull,FALSE);
			}

			if($regularexist = $this->EnfrentamientoMapper->validExistEnfTipo($categoria->getId(),"Regular")){
				$regularexist=TRUE;
			}

			if($campexist = $this->EnfrentamientoMapper->validExistEnfTipo($categoria->getId(),"Campeonato")){
				$campexist=TRUE;
			}

			if($camp_semi_exist = $this->EnfrentamientoMapper->validExistEnfTipo($categoria->getId(),"Semifinal")){
				$camp_semi_exist=TRUE;
			}

			if($camp_final_exist = $this->EnfrentamientoMapper->validExistEnfTipo($categoria->getId(),"Final")){
				$camp_final_exist=TRUE;
			}

			if($camp_final_exist){
				if($lastfinal->getResultado() != " - "){
					$camp_final_full=TRUE;
				}
			}

			if($this->ParejaMapper->isCatExist($categoria->getId(),$_SESSION["currentuserid"])){
				array_push($meincat,TRUE);
			}else{
				array_push($meincat,FALSE);
			}
		}

		$this->view->setVariable("campeonato", $campeonato);
		$this->view->setVariable("categorias", $categorias);
		$this->view->setVariable("inscritos", $inscritos);
		$this->view->setVariable("valido", $valido);
		$this->view->setVariable("campexist", $campexist);
		$this->view->setVariable("regularexist", $regularexist);
		$this->view->setVariable("camp_semi_exist", $camp_semi_exist);
		$this->view->setVariable("camp_final_exist", $camp_final_exist);
		$this->view->setVariable("camp_final_full", $camp_final_full);
		$this->view->setVariable("meincat", $meincat);
		$this->view->setVariable("catfull", $catfull);
		$this->view->render("categorias", "index");
	}

	public function index_r()
	{

		if (!isset($_REQUEST["id_categoria"])) {
			throw new Exception("Categoria id es necesaria");
		}

		$categoria = $this->CategoriaMapper->findById($_REQUEST["id_categoria"]);
		$campeonato = $this->CampeonatoMapper->findById($categoria->getId_Campeonato());
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCatTipo($_REQUEST["id_categoria"],"Regular");
		$enfrentamientos_txt = array();

		foreach($enfrentamientos as $enf){
						$enfrentamiento = new Enfrentamiento();
						$enfrentamiento->set_IdCategoria($this->CategoriaMapper->findById($enf->getId_Categoria())->getNombre());
						
						$enfrentamiento->set_IdPareja1($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);

						$enfrentamiento->set_IdPareja2($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);
						$enfrentamiento->set_IdPista($this->PistaMapper->findById($enf->getId_Pista())->getNombre());
						$enfrentamiento->setId_Hora($this->PistaMapper->findHorasById($enf->getId_Hora())->getHora_ini());
						$enfrentamiento->set_Set1($enf->getSet_1());
						$enfrentamiento->set_Set2($enf->getSet_2());
						$enfrentamiento->set_Set3($enf->getSet_3());
						$enfrentamiento->set_Resultado($enf->getResultado());
						$enfrentamiento->set_Tipo($enf->getTipo());
						$enfrentamiento->set_Fecha($enf->getFecha());
						array_push($enfrentamientos_txt,$enfrentamiento);
						
		}

		$this->view->setVariable("categoria", $categoria);
		$this->view->setVariable("campeonato", $campeonato);
		$this->view->setVariable("enfrentamientos", $enfrentamientos_txt);
		$this->view->render("categorias", "index_r");
	}

	public function index_c()
	{

		if (!isset($_REQUEST["id_categoria"])) {
			throw new Exception("Categoria id es necesaria");
		}

		$categoria = $this->CategoriaMapper->findById($_REQUEST["id_categoria"]);
		$campeonato = $this->CampeonatoMapper->findById($categoria->getId_Campeonato());
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCatTipo($_REQUEST["id_categoria"],"Campeonato");
		$enfrentamientos_txt = array();

		foreach($enfrentamientos as $enf){
						$enfrentamiento = new Enfrentamiento();
						$enfrentamiento->set_IdCategoria($this->CategoriaMapper->findById($enf->getId_Categoria())->getNombre());
						
						$enfrentamiento->set_IdPareja1($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);

						$enfrentamiento->set_IdPareja2($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);
						$enfrentamiento->set_IdPista($this->PistaMapper->findById($enf->getId_Pista())->getNombre());
						$enfrentamiento->setId_Hora($this->PistaMapper->findHorasById($enf->getId_Hora())->getHora_ini());
						$enfrentamiento->set_Set1($enf->getSet_1());
						$enfrentamiento->set_Set2($enf->getSet_2());
						$enfrentamiento->set_Set3($enf->getSet_3());
						$enfrentamiento->set_Resultado($enf->getResultado());
						$enfrentamiento->set_Tipo($enf->getTipo());
						$enfrentamiento->set_Fecha($enf->getFecha());
						array_push($enfrentamientos_txt,$enfrentamiento);
						
		}

		$this->view->setVariable("categoria", $categoria);
		$this->view->setVariable("campeonato", $campeonato);
		$this->view->setVariable("enfrentamientos", $enfrentamientos_txt);
		$this->view->render("categorias", "index_c");
	}

	public function index_s()
	{

		if (!isset($_REQUEST["id_categoria"])) {
			throw new Exception("Categoria id es necesaria");
		}

		$categoria = $this->CategoriaMapper->findById($_REQUEST["id_categoria"]);
		$campeonato = $this->CampeonatoMapper->findById($categoria->getId_Campeonato());
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCatTipo($_REQUEST["id_categoria"],"Semifinal");
		$enfrentamientos_txt = array();

		foreach($enfrentamientos as $enf){
						$enfrentamiento = new Enfrentamiento();
						$enfrentamiento->set_IdCategoria($this->CategoriaMapper->findById($enf->getId_Categoria())->getNombre());
						
						$enfrentamiento->set_IdPareja1($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);

						$enfrentamiento->set_IdPareja2($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);
						$enfrentamiento->set_IdPista($this->PistaMapper->findById($enf->getId_Pista())->getNombre());
						$enfrentamiento->setId_Hora($this->PistaMapper->findHorasById($enf->getId_Hora())->getHora_ini());
						$enfrentamiento->set_Set1($enf->getSet_1());
						$enfrentamiento->set_Set2($enf->getSet_2());
						$enfrentamiento->set_Set3($enf->getSet_3());
						$enfrentamiento->set_Resultado($enf->getResultado());
						$enfrentamiento->set_Tipo($enf->getTipo());
						$enfrentamiento->set_Fecha($enf->getFecha());
						array_push($enfrentamientos_txt,$enfrentamiento);
						
		}

		$this->view->setVariable("categoria", $categoria);
		$this->view->setVariable("campeonato", $campeonato);
		$this->view->setVariable("enfrentamientos", $enfrentamientos_txt);
		$this->view->render("categorias", "index_c");
	}

	public function index_f()
	{

		if (!isset($_REQUEST["id_categoria"])) {
			throw new Exception("Categoria id es necesaria");
		}

		$categoria = $this->CategoriaMapper->findById($_REQUEST["id_categoria"]);
		$campeonato = $this->CampeonatoMapper->findById($categoria->getId_Campeonato());
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCatTipo($_REQUEST["id_categoria"],"Final");
		$enfrentamientos_txt = array();

		foreach($enfrentamientos as $enf){
						$enfrentamiento = new Enfrentamiento();
						$enfrentamiento->set_IdCategoria($this->CategoriaMapper->findById($enf->getId_Categoria())->getNombre());
						
						$enfrentamiento->set_IdPareja1($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja1(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);

						$enfrentamiento->set_IdPareja2($this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Capitan())->getNombre()
						. " - " .
						$this->UsuarioMapper->findById($this->ParejaMapper->findByCatPar($enf->getId_Pareja2(),$enf->getId_Categoria())->getId_Jugador())->getNombre()
						);
						$enfrentamiento->set_IdPista($this->PistaMapper->findById($enf->getId_Pista())->getNombre());
						$enfrentamiento->setId_Hora($this->PistaMapper->findHorasById($enf->getId_Hora())->getHora_ini());
						$enfrentamiento->set_Set1($enf->getSet_1());
						$enfrentamiento->set_Set2($enf->getSet_2());
						$enfrentamiento->set_Set3($enf->getSet_3());
						$enfrentamiento->set_Resultado($enf->getResultado());
						$enfrentamiento->set_Tipo($enf->getTipo());
						$enfrentamiento->set_Fecha($enf->getFecha());
						array_push($enfrentamientos_txt,$enfrentamiento);
						
		}

		$this->view->setVariable("categoria", $categoria);
		$this->view->setVariable("campeonato", $campeonato);
		$this->view->setVariable("enfrentamientos", $enfrentamientos_txt);
		$this->view->render("categorias", "index_c");
	}

	public function add()
	{

		if (!isset($_POST)) {
			if (!isset($_REQUEST["idcamp"])) {
				throw new Exception("Campeonato id es necesario");
			}
		}

		if (isset($_POST["nombre"]) && isset($_POST["nivel"]) && isset($_POST["idcamp"])) {

			$categoria = new Categoria();
			$categoria->setId_Campeonato($_POST["idcamp"]);
			$categoria->setNombre($_POST["nombre"]);
			$categoria->setNivel($_POST["nivel"]);

			try {
				$categoria->checkIsValid();

				if (!$this->CategoriaMapper->categoriaExists($_POST["idcamp"],$_POST["nombre"],$_POST["nivel"])){
				$this->CategoriaMapper->save($categoria);
				}
				
				$this->view->redirect("categorias", "index", "idcamp=" . $categoria->getId_Campeonato() . "");

			} catch (ValidationException $ex) {
				$errors = $ex->getErrors();
				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->render("categorias", "add");
	}

	public function delete()
	{
		if (!isset($_REQUEST["idcategoria"])) {
			throw new Exception("id noticia is mandatory");
		}

		$idcategoria = $_REQUEST["idcategoria"];
		$categoria = $this->CategoriaMapper->findById($idcategoria);

		if ($categoria == NULL) {
			throw new Exception("No existe categoria con id: " . $idcategoria);
		} else {
			$this->CategoriaMapper->delete($categoria);
			$this->view->setFlash(sprintf("Categoria \"%s\"  eliminada."), $categoria->getNombre());
		}
		$this->view->redirect("categorias", "index", "idcamp=" . $categoria->getId_Campeonato() . "");
	}

	public function deleteCat()
	{
		if (!isset($_REQUEST["id_categoria"])) {
			throw new Exception("id categoria is mandatory");
		}
		if (!isset($_REQUEST["nombre_capitan"])) {
			throw new Exception("nombre capitan is mandatory");
		}
		if (!isset($_REQUEST["nombre_pareja"])) {
			throw new Exception("nombre pareja is mandatory");
		}
		if (!isset($_REQUEST["id_campeonato"])) {
			throw new Exception("id campeonato is mandatory");
		}

		$id_categoria = $_REQUEST["id_categoria"];

		$nombre_capitan = $_REQUEST["nombre_capitan"];
		$id_capitan = $this->UsuarioMapper->findByUsername($nombre_capitan)->getId();

		$nombre_pareja = $_REQUEST["nombre_pareja"];
		$id_pareja = $this->UsuarioMapper->findByUsername($nombre_pareja)->getId();

		$id_campeonato = $_REQUEST["id_campeonato"];


		$pareja = $this->ParejaMapper->findByCat($id_categoria,$id_capitan,$id_pareja);


		if ($pareja == NULL) {
			throw new Exception("No existe pareja con id categoria: " . $id_categoria);
		} else {
			$this->ParejaMapper->delete($pareja);
			$this->view->setFlash(sprintf("Pareja \"%s\"  eliminada."), $pareja->getId());
		}
		$this->view->redirect("categorias", "index", "idcamp=" . $id_campeonato . "");
	}

	public function creaRegular()
	{

		if (!isset($_REQUEST["id_campeonato"])) {
			throw new Exception("id campeonato is mandatory");
		}
		$id_campeonato = $_REQUEST["id_campeonato"];

		$campeonato = $this->CampeonatoMapper->findById($id_campeonato);
		$categorias = $this->CategoriaMapper->findAllByIdCampeonato($campeonato->getId());

		//Creamos un Array de las fechas del Campeonato

		$fechas = $this->calc_dias_from($campeonato->getFecha_ini()."",$campeonato->getFecha_fin()."");

		//Creamos Calendario Regular Vacio


			foreach ($categorias as $categoria){
			$parejas = $this->ParejaMapper->findAllByCat($categoria->getId());
			foreach ($parejas as $pareja){
				foreach ($parejas as $pareja1){

					if($pareja->getId() < $pareja1->getId()){
						$enfrentamiento = new Enfrentamiento();
						$enfrentamiento->set_IdCategoria($categoria->getId());
						$enfrentamiento->set_IdPareja1($pareja->getId());
						$enfrentamiento->set_IdPareja2($pareja1->getId());
						$enfrentamiento->set_IdPista($this->PistaMapper->findAll()[0]->getId());
						$enfrentamiento->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
						$enfrentamiento->set_Set1(" - ");
						$enfrentamiento->set_Set2(" - ");
						$enfrentamiento->set_Set3(" - ");
						$enfrentamiento->set_Resultado(" - ");
						$enfrentamiento->set_Tipo("Regular");
						$enfrentamiento->set_Fecha($campeonato->getFecha_ini());

						try {
							$enfrentamiento->checkIsValid();
			
							if (!$this->EnfrentamientoMapper->enfExists($categoria->getId(),$pareja->getId(),$pareja1->getId())){
							$this->EnfrentamientoMapper->save($enfrentamiento);
							}
			
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
					}
				}
			}

		}
					//Rellenamos Calendario Regular con Pista,Hora, y Fecha

					$enfrentamientos = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Regular");
					$horas = $this->PistaMapper->findHoras();
					$pistas = $this->PistaMapper->findAll();


					//Pone Horas
					$conth = -1;
					foreach($enfrentamientos as $enf){
		
						if($conth < count($horas)-1){
							$conth++;
						}else{
							$conth = 0;
						}
						$this->EnfrentamientoMapper->update_Hora_Enf($enf->getId(),$horas[$conth]->getId());
					}

					//Pone Pistas
					$enfrentamientos1 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Regular");
					$sumacont=FALSE;
					$sumareset=FALSE;
					$contp = 0;
					$contd=0;
					foreach($enfrentamientos1 as $enf1){
						if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() != end($pistas)->getId())){
							$sumacont=TRUE;
						}
						
						if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() == end($pistas)->getId())){
							$sumareset=TRUE;
						}
						$this->EnfrentamientoMapper->update_Pista_Enf($enf1->getId(),$pistas[$contp]->getId());
						$this->EnfrentamientoMapper->update_Pista_Day($enf1->getId(),$fechas[$contd]);

						if($sumacont){
							$contp++;
							$sumacont=FALSE;
						}

						if($sumareset){
							$contp=0;
							$contd++;
							$sumareset=FALSE;
						}

					}
					
					//Prereservamos las pistas,fechas y horas para los enfrentamientos

					$enfrentamientos2 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Regular");
			
								foreach($enfrentamientos2 as $enf2){
			
									$prereservaenf = new ReservaEnf();
									$prereservaenf->setId_enfrentamiento($enf2->getId());
			
									try {
										$prereservaenf->checkIsValid();
						
										if (!$this->ReservaEnfMapper->enfExists($enf2->getId())){
										$this->ReservaEnfMapper->save($prereservaenf);
										}
						
									} catch (ValidationException $ex) {
										$errors = $ex->getErrors();
										$this->view->setVariable("errors", $errors);
									}
			
								}

		$this->view->redirect("categorias", "index", "idcamp=" . $campeonato->getId() . "");
	}


	public function simulaRegular()
	{

		if (!isset($_REQUEST["id_campeonato"])) {
			throw new Exception("id campeonato is mandatory");
		}
		$id_campeonato = $_REQUEST["id_campeonato"];

		$campeonato = $this->CampeonatoMapper->findById($id_campeonato);


		//Simulamos los resultados para la enfrentamientos Regulares del Campeonato y generas un array para los ganadores

		$ganadores = array();
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Regular");

		foreach($enfrentamientos as $enf){
			$results = array();
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Result($results[0][1],$results[1][1],$results[2][1]));

			if($results[3][0] > $results[3][4]){
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja1(),0));
			}else{
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja2(),0));
			}

			$this->EnfrentamientoMapper->update_Result_Enf($enf->getId(),$results[0][0],$results[1][0],$results[2][0],$results[3]);
		}



		//Simulamos promocionar a los ganadores de cada categoria a la siguiente fase

	
		$aux = array();

		foreach($ganadores as $i=>$ganador){
			if($ganador != end($ganadores)){
				if(($ganador->getId_Categoria() != $ganadores[$i+1]->getId_Categoria())){

					array_push($aux,$ganador);
					$repeticiones=array();
					$auxganador = array();
					foreach($aux as $i=>$aux1){
						$cont=0;
						foreach($aux as $aux2){
							if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
								$cont++;
							}

							if($aux2 == end($aux)){
								array_push($repeticiones,$cont);
							}
						}
						array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
					}

					//Filtro los ganadores del grupo repetidos

					foreach($auxganador as $a=>$auxf){
						$flag=FALSE;
						foreach($auxganador as $auxf1){
							if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
								if($flag){
								unset($auxganador[$a]);
								}else{
									$flag=TRUE;
								}
							}
						}
					}

					//Filtramos el array de ganadores a los 8 campeones


					$this->array_sort_by($auxganador,'repeticiones');
					$cont1=0;
					foreach($auxganador as $b=>$auxf2){

						if($cont1==8){
							unset($auxganador[$b]);	
						}else{
							$cont1++;
						}
					}


					//Promocionamos a los 8 primeros ganadores al calendario de Torneo

					for ($u = 0; $u < 4; $u++) {
						$enfrent = new Enfrentamiento();
						$enfrent->set_IdCategoria($auxganador[$u]->getId_Categoria());
						$enfrent->set_IdPareja1($auxganador[$u]->getId_Pareja());
						$enfrent->set_IdPareja2($auxganador[(count($auxganador)-1)-$u]->getId_Pareja());
						$enfrent->set_IdPista($this->PistaMapper->findAll()[0]->getId());
						$enfrent->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
						$enfrent->set_Set1(" - ");
						$enfrent->set_Set2(" - ");
						$enfrent->set_Set3(" - ");
						$enfrent->set_Resultado(" - ");
						$enfrent->set_Tipo("Campeonato");
						$enfrent->set_Fecha($campeonato->getFecha_ini());
	
						try {
							$enfrent->checkIsValid();
			
							if (!$this->EnfrentamientoMapper->enfExistsCamp($auxganador[$u]->getId_Categoria(),$enfrent->getId_Pareja1(),$enfrent->getId_Pareja2())){
							$this->EnfrentamientoMapper->save($enfrent);
							}
			
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
					}

					$aux = array();
				}else{
					array_push($aux,$ganador);
				}
			}else{

				array_push($aux,$ganador);
				$repeticiones=array();
				$auxganador = array();
				foreach($aux as $i=>$aux1){
					$cont=0;
					foreach($aux as $aux2){
						if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
							$cont++;
						}

						if($aux2 == end($aux)){
							array_push($repeticiones,$cont);
						}
					}
					array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
				}

				//Filtro los ganadores del grupo repetidos

				foreach($auxganador as $a=>$auxf){
					$flag=FALSE;
					foreach($auxganador as $auxf1){
						if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
							if($flag){
							unset($auxganador[$a]);
							}else{
								$flag=TRUE;
							}
						}
					}
				}

				//Filtramos el array de ganadores a los 8 campeones


				$this->array_sort_by($auxganador,'repeticiones');
				$cont1=0;
				foreach($auxganador as $b=>$auxf2){

					if($cont1==8){
						unset($auxganador[$b]);	
					}else{
						$cont1++;
					}
				}


				//Promocionamos a los 8 primeros ganadores al calendario de Torneo

				for ($u = 0; $u < 4; $u++) {
					$enfrent = new Enfrentamiento();
					$enfrent->set_IdCategoria($auxganador[$u]->getId_Categoria());
					$enfrent->set_IdPareja1($auxganador[$u]->getId_Pareja());
					$enfrent->set_IdPareja2($auxganador[(count($auxganador)-1)-$u]->getId_Pareja());
					$enfrent->set_IdPista($this->PistaMapper->findAll()[0]->getId());
					$enfrent->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
					$enfrent->set_Set1(" - ");
					$enfrent->set_Set2(" - ");
					$enfrent->set_Set3(" - ");
					$enfrent->set_Resultado(" - ");
					$enfrent->set_Tipo("Campeonato");
					$enfrent->set_Fecha($campeonato->getFecha_ini());

					try {
						$enfrent->checkIsValid();
		
						if (!$this->EnfrentamientoMapper->enfExistsCamp($auxganador[$u]->getId_Categoria(),$enfrent->getId_Pareja1(),$enfrent->getId_Pareja2())){
						$this->EnfrentamientoMapper->save($enfrent);
						}
		
					} catch (ValidationException $ex) {
						$errors = $ex->getErrors();
						$this->view->setVariable("errors", $errors);
					}
				}

				$aux = array();
			}
		}


		//Recorrer los enfrentamientos por tipo campeonato y hacer update de horas,pista y fecha

		
		//Rellenamos Calendario Regular con Pista,Hora, y Fecha

		$enfrentamientos0 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Campeonato");
		$lastenf = $this->EnfrentamientoMapper->findLastEnfTipo($campeonato->getId(),"Regular");
		$horas = $this->PistaMapper->findHoras();
		$pistas = $this->PistaMapper->findAll();

		/*//calculamos el index de la ultima hora
		$horindex=0;
		$horaux=0;
		foreach($horas as $hor){
			if($hor->getId() == $lastenf->getId_Hora()){
				$horindex=$horaux;
			}
			$horaux++;
		}

		//calculamos el index de la ultima pista
		$pistindex=0;
		$pistaux=0;
		foreach($pistas as $pist){
			if($pist->getId() == $lastenf->getId_Pista()){
				$pistindex=$pistaux;
			}
			$pistaux++;
		}*/
		

		//Pone Horas
		//$conth = $horindex;
		$conth = -1;
		foreach($enfrentamientos0 as $enf0){

			if($conth < count($horas)-1){
				$conth++;
			}else{
				$conth = 0;
			}
			$this->EnfrentamientoMapper->update_Hora_Enf($enf0->getId(),$horas[$conth]->getId());
		}

		$fechas = $this->calc_dias_from($lastenf->getFecha()."",$campeonato->getFecha_fin()."");

		
		//calculamos el index de la fecha
		$dateindex=0;
		$dateaux=0;
		foreach($fechas as $fech){
			if($fech == $lastenf->getFecha()){
				$dateindex=$dateaux;
			}
			$dateaux++;
		}

		//Pone Pistas
		$enfrentamientos1 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Campeonato");
		$sumacont=FALSE;
		$sumareset=FALSE;
		//$contp = $pistindex;
		$contp = 0;
		$contd=$dateindex+1;

		//compruebo si anteriormente se salto de dia o se estaba en la ultima hora de alguna pista (No utilizado por saltar el dia para cada fase)
		/*if(($lastenf->getId_Hora() == end($horas)->getId()) && ($lastenf->getId_Pista() == end($pistas)->getId())){
			$contp=0;
			$contd++;
		}else if($lastenf->getId_Hora() == end($horas)->getId()){
			$contp++;
		}*/

		foreach($enfrentamientos1 as $enf1){
			if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() != end($pistas)->getId())){
				$sumacont=TRUE;
			}
			
			if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() == end($pistas)->getId())){
				$sumareset=TRUE;
			}
			$this->EnfrentamientoMapper->update_Pista_Enf($enf1->getId(),$pistas[$contp]->getId());
			$this->EnfrentamientoMapper->update_Pista_Day($enf1->getId(),$fechas[$contd]);

			if($sumacont){
				$contp++;
				$sumacont=FALSE;
			}

			if($sumareset){
				$contp=0;
				$contd++;
				$sumareset=FALSE;
			}

		}
			
		//Prereservamos las pistas,fechas y horas para los enfrentamientos de Campeonato

		$enfrentamientos2 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Campeonato");
			
		foreach($enfrentamientos2 as $enf2){
		
		$prereservaenf = new ReservaEnf();
		$prereservaenf->setId_enfrentamiento($enf2->getId());
		
		try {
			$prereservaenf->checkIsValid();
					
			if (!$this->ReservaEnfMapper->enfExists($enf2->getId())){
				$this->ReservaEnfMapper->save($prereservaenf);
			}
					
			} catch (ValidationException $ex) {
			$errors = $ex->getErrors();
			$this->view->setVariable("errors", $errors);
			}
		
		}
		

		//Obtenemos todas las prereservas para la categoria Regular del campeonato y hacemos efectiva la reserva y eliminamos las prereservas

		$reservasenfs = $this->ReservaEnfMapper->findByIdCampTipo($campeonato->getId(),"Regular");

		foreach($reservasenfs as $resenf){

			$reservafinal = new Reserva();
			$reservafinal->setId_pista($resenf->getId_pista());
			$reservafinal->setId_usuario($resenf->getId_Usuario());
			$reservafinal->setId_hora($resenf->getId_Hora());
			$reservafinal->setFecha($resenf->getFecha());
			
					try {
						$reservafinal->checkIsValid();
						
						if (!$this->PistaMapper->findReserva($resenf->getId_pista(),$resenf->getId_hora(),$resenf->getFecha())){

							$this->PistaMapper->saveReserva($reservafinal);
						}
						$reservaborrar = new ReservaEnf();
						$reservaborrar->setId($resenf->getId());
						$reservaborrar->setId_enfrentamiento($resenf->getId_Enfrentamiento());
						$this->ReservaEnfMapper->delete($reservaborrar);
						
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
		}
		$this->view->redirect("categorias", "index", "idcamp=" . $campeonato->getId() . "");
	}


	public function simulaCamp(){


	if (!isset($_REQUEST["id_campeonato"])) {
			throw new Exception("id campeonato is mandatory");
		}
		$id_campeonato = $_REQUEST["id_campeonato"];

		$campeonato = $this->CampeonatoMapper->findById($id_campeonato);


		//Simulamos los resultados para la enfrentamientos Regulares del Campeonato y generas un array para los ganadores

		$ganadores = array();
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Campeonato");

		foreach($enfrentamientos as $enf){
			$results = array();
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Result($results[0][1],$results[1][1],$results[2][1]));

			if($results[3][0] > $results[3][4]){
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja1(),0));
			}else{
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja2(),0));
			}

			$this->EnfrentamientoMapper->update_Result_Enf($enf->getId(),$results[0][0],$results[1][0],$results[2][0],$results[3]);
		}


		$aux = array();

		foreach($ganadores as $i=>$ganador){
			if($ganador != end($ganadores)){
				if(($ganador->getId_Categoria() != $ganadores[$i+1]->getId_Categoria())){

					array_push($aux,$ganador);
					$repeticiones=array();
					$auxganador = array();
					foreach($aux as $i=>$aux1){
						$cont=0;
						foreach($aux as $aux2){
							if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
								$cont++;
							}

							if($aux2 == end($aux)){
								array_push($repeticiones,$cont);
							}
						}
						array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
					}

					//Filtro los ganadores del grupo repetidos

					foreach($auxganador as $a=>$auxf){
						$flag=FALSE;
						foreach($auxganador as $auxf1){
							if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
								if($flag){
								unset($auxganador[$a]);
								}else{
									$flag=TRUE;
								}
							}
						}
					}

					//Filtramos el array de ganadores a los 4 campeones


					$this->array_sort_by($auxganador,'repeticiones');
					$cont1=0;
					foreach($auxganador as $b=>$auxf2){

						if($cont1==4){
							unset($auxganador[$b]);	
						}else{
							$cont1++;
						}
					}


					//Promocionamos a los 4 primeros ganadores al la siguiente fase

					for ($u = 0; $u < 2; $u++) {
						$enfrent = new Enfrentamiento();
						$enfrent->set_IdCategoria($auxganador[$u]->getId_Categoria());
						$enfrent->set_IdPareja1($auxganador[$u]->getId_Pareja());
						$enfrent->set_IdPareja2($auxganador[(count($auxganador)-1)-$u]->getId_Pareja());
						$enfrent->set_IdPista($this->PistaMapper->findAll()[0]->getId());
						$enfrent->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
						$enfrent->set_Set1(" - ");
						$enfrent->set_Set2(" - ");
						$enfrent->set_Set3(" - ");
						$enfrent->set_Resultado(" - ");
						$enfrent->set_Tipo("Semifinal");
						$enfrent->set_Fecha($campeonato->getFecha_ini());
	
						try {
							$enfrent->checkIsValid();
			
							if (!$this->EnfrentamientoMapper->enfExistsCamp($auxganador[$u]->getId_Categoria(),$enfrent->getId_Pareja1(),$enfrent->getId_Pareja2())){
							$this->EnfrentamientoMapper->save($enfrent);
							}
			
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
					}

					$aux = array();
				}else{
					array_push($aux,$ganador);
				}
			}else{

				array_push($aux,$ganador);
				$repeticiones=array();
				$auxganador = array();
				foreach($aux as $i=>$aux1){
					$cont=0;
					foreach($aux as $aux2){
						if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
							$cont++;
						}

						if($aux2 == end($aux)){
							array_push($repeticiones,$cont);
						}
					}
					array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
				}

				//Filtro los ganadores del grupo repetidos

				foreach($auxganador as $a=>$auxf){
					$flag=FALSE;
					foreach($auxganador as $auxf1){
						if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
							if($flag){
							unset($auxganador[$a]);
							}else{
								$flag=TRUE;
							}
						}
					}
				}

				//Filtramos el array de ganadores a los 4 campeones


				$this->array_sort_by($auxganador,'repeticiones');
				$cont1=0;
				foreach($auxganador as $b=>$auxf2){

					if($cont1==4){
						unset($auxganador[$b]);	
					}else{
						$cont1++;
					}
				}


				//Promocionamos a los 4 primeros ganadores al calendario de Torneo

				for ($u = 0; $u < 2; $u++) {
					$enfrent = new Enfrentamiento();
					$enfrent->set_IdCategoria($auxganador[$u]->getId_Categoria());
					$enfrent->set_IdPareja1($auxganador[$u]->getId_Pareja());
					$enfrent->set_IdPareja2($auxganador[(count($auxganador)-1)-$u]->getId_Pareja());
					$enfrent->set_IdPista($this->PistaMapper->findAll()[0]->getId());
					$enfrent->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
					$enfrent->set_Set1(" - ");
					$enfrent->set_Set2(" - ");
					$enfrent->set_Set3(" - ");
					$enfrent->set_Resultado(" - ");
					$enfrent->set_Tipo("Semifinal");
					$enfrent->set_Fecha($campeonato->getFecha_ini());

					try {
						$enfrent->checkIsValid();
		
						if (!$this->EnfrentamientoMapper->enfExistsCamp($auxganador[$u]->getId_Categoria(),$enfrent->getId_Pareja1(),$enfrent->getId_Pareja2())){
						$this->EnfrentamientoMapper->save($enfrent);
						}
		
					} catch (ValidationException $ex) {
						$errors = $ex->getErrors();
						$this->view->setVariable("errors", $errors);
					}
				}

				$aux = array();
			}
		}
		

		//Recorrer los enfrentamientos por tipo campeonato y hacer update de horas,pista y fecha

		//Rellenamos Calendario Regular con Pista,Hora, y Fecha

		$enfrentamientos0 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Semifinal");
		$lastenf = $this->EnfrentamientoMapper->findLastEnfTipo($campeonato->getId(),"Campeonato");
		$horas = $this->PistaMapper->findHoras();
		$pistas = $this->PistaMapper->findAll();

		/*//calculamos el index de la ultima hora
		$horindex=0;
		$horaux=0;
		foreach($horas as $hor){
			if($hor->getId() == $lastenf->getId_Hora()){
				$horindex=$horaux;
			}
			$horaux++;
		}

		//calculamos el index de la ultima pista
		$pistindex=0;
		$pistaux=0;
		foreach($pistas as $pist){
			if($pist->getId() == $lastenf->getId_Pista()){
				$pistindex=$pistaux;
			}
			$pistaux++;
		}*/
		

		//Pone Horas
		//$conth = $horindex;
		$conth = -1;
		foreach($enfrentamientos0 as $enf0){

			if($conth < count($horas)-1){
				$conth++;
			}else{
				$conth = 0;
			}
			$this->EnfrentamientoMapper->update_Hora_Enf($enf0->getId(),$horas[$conth]->getId());
		}

		$fechas = $this->calc_dias_from($lastenf->getFecha()."",$campeonato->getFecha_fin()."");

		
		//calculamos el index de la fecha
		$dateindex=0;
		$dateaux=0;
		foreach($fechas as $fech){
			if($fech == $lastenf->getFecha()){
				$dateindex=$dateaux;
			}
			$dateaux++;
		}

		//Pone Pistas
		$enfrentamientos1 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Semifinal");
		$sumacont=FALSE;
		$sumareset=FALSE;
		//$contp = $pistindex;
		$contp = 0;
		$contd=$dateindex+1;

		//compruebo si anteriormente se salto de dia o se estaba en la ultima hora de alguna pista (No utilizado por saltar el dia para cada fase)
		/*if(($lastenf->getId_Hora() == end($horas)->getId()) && ($lastenf->getId_Pista() == end($pistas)->getId())){
			$contp=0;
			$contd++;
		}else if($lastenf->getId_Hora() == end($horas)->getId()){
			$contp++;
		}*/

		foreach($enfrentamientos1 as $enf1){


			if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() != end($pistas)->getId())){
				$sumacont=TRUE;
			}
			
			if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() == end($pistas)->getId())){
				$sumareset=TRUE;
			}
			$this->EnfrentamientoMapper->update_Pista_Enf($enf1->getId(),$pistas[$contp]->getId());
			$this->EnfrentamientoMapper->update_Pista_Day($enf1->getId(),$fechas[$contd]);

			if($sumacont){
				$contp++;
				$sumacont=FALSE;
			}

			if($sumareset){
				$contp=0;
				$contd++;
				$sumareset=FALSE;
			}

		}

		//Prereservamos las pistas,fechas y horas para los enfrentamientos de Campeonato

		$enfrentamientos2 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Semifinal");
			
		foreach($enfrentamientos2 as $enf2){
		
		$prereservaenf = new ReservaEnf();
		$prereservaenf->setId_enfrentamiento($enf2->getId());
		
		try {
			$prereservaenf->checkIsValid();
					
			if (!$this->ReservaEnfMapper->enfExists($enf2->getId())){
				$this->ReservaEnfMapper->save($prereservaenf);
			}
					
			} catch (ValidationException $ex) {
			$errors = $ex->getErrors();
			$this->view->setVariable("errors", $errors);
			}
		
		}
		

		//Obtenemos todas las prereservas para la categoria Regular del campeonato y hacemos efectiva la reserva y eliminamos las prereservas

		$reservasenfs = $this->ReservaEnfMapper->findByIdCampTipo($campeonato->getId(),"Campeonato");

		foreach($reservasenfs as $resenf){

			$reservafinal = new Reserva();
			$reservafinal->setId_pista($resenf->getId_pista());
			$reservafinal->setId_usuario($resenf->getId_Usuario());
			$reservafinal->setId_hora($resenf->getId_Hora());
			$reservafinal->setFecha($resenf->getFecha());
			
					try {
						$reservafinal->checkIsValid();
						
						if (!$this->PistaMapper->findReserva($resenf->getId_pista(),$resenf->getId_hora(),$resenf->getFecha())){

							$this->PistaMapper->saveReserva($reservafinal);
						}
						$reservaborrar = new ReservaEnf();
						$reservaborrar->setId($resenf->getId());
						$reservaborrar->setId_enfrentamiento($resenf->getId_Enfrentamiento());
						$this->ReservaEnfMapper->delete($reservaborrar);
						
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
		}
		

		$this->view->redirect("categorias", "index", "idcamp=" . $campeonato->getId() . "");
	}


	public function simulaSemi()
	{

		if (!isset($_REQUEST["id_campeonato"])) {
			throw new Exception("id campeonato is mandatory");
		}
		$id_campeonato = $_REQUEST["id_campeonato"];

		$campeonato = $this->CampeonatoMapper->findById($id_campeonato);


		//Simulamos los resultados para la enfrentamientos Regulares del Campeonato y generas un array para los ganadores

		$ganadores = array();
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Semifinal");

		foreach($enfrentamientos as $enf){
			$results = array();
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Result($results[0][1],$results[1][1],$results[2][1]));

			if($results[3][0] > $results[3][4]){
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja1(),0));
			}else{
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja2(),0));
			}

			$this->EnfrentamientoMapper->update_Result_Enf($enf->getId(),$results[0][0],$results[1][0],$results[2][0],$results[3]);
		}


		$aux = array();

		foreach($ganadores as $i=>$ganador){
			if($ganador != end($ganadores)){
				if(($ganador->getId_Categoria() != $ganadores[$i+1]->getId_Categoria())){

					array_push($aux,$ganador);
					$repeticiones=array();
					$auxganador = array();
					foreach($aux as $i=>$aux1){
						$cont=0;
						foreach($aux as $aux2){
							if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
								$cont++;
							}

							if($aux2 == end($aux)){
								array_push($repeticiones,$cont);
							}
						}
						array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
					}

					//Filtro los ganadores del grupo repetidos

					foreach($auxganador as $a=>$auxf){
						$flag=FALSE;
						foreach($auxganador as $auxf1){
							if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
								if($flag){
								unset($auxganador[$a]);
								}else{
									$flag=TRUE;
								}
							}
						}
					}

					//Filtramos el array de ganadores a los 4 campeones


					$this->array_sort_by($auxganador,'repeticiones');
					$cont1=0;
					foreach($auxganador as $b=>$auxf2){

						if($cont1==2){
							unset($auxganador[$b]);	
						}else{
							$cont1++;
						}
					}


					//Promocionamos a los 4 primeros ganadores al la siguiente fase

					for ($u = 0; $u < 1; $u++) {
						$enfrent = new Enfrentamiento();
						$enfrent->set_IdCategoria($auxganador[$u]->getId_Categoria());
						$enfrent->set_IdPareja1($auxganador[$u]->getId_Pareja());
						$enfrent->set_IdPareja2($auxganador[(count($auxganador)-1)-$u]->getId_Pareja());
						$enfrent->set_IdPista($this->PistaMapper->findAll()[0]->getId());
						$enfrent->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
						$enfrent->set_Set1(" - ");
						$enfrent->set_Set2(" - ");
						$enfrent->set_Set3(" - ");
						$enfrent->set_Resultado(" - ");
						$enfrent->set_Tipo("Final");
						$enfrent->set_Fecha($campeonato->getFecha_ini());
	
						try {
							$enfrent->checkIsValid();
			
							if (!$this->EnfrentamientoMapper->enfExistsCamp($auxganador[$u]->getId_Categoria(),$enfrent->getId_Pareja1(),$enfrent->getId_Pareja2())){
							$this->EnfrentamientoMapper->save($enfrent);
							}
			
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
					}

					$aux = array();
				}else{
					array_push($aux,$ganador);
				}
			}else{

				array_push($aux,$ganador);
				$repeticiones=array();
				$auxganador = array();
				foreach($aux as $i=>$aux1){
					$cont=0;
					foreach($aux as $aux2){
						if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
							$cont++;
						}

						if($aux2 == end($aux)){
							array_push($repeticiones,$cont);
						}
					}
					array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
				}

				//Filtro los ganadores del grupo repetidos

				foreach($auxganador as $a=>$auxf){
					$flag=FALSE;
					foreach($auxganador as $auxf1){
						if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
							if($flag){
							unset($auxganador[$a]);
							}else{
								$flag=TRUE;
							}
						}
					}
				}

				//Filtramos el array de ganadores a los 4 campeones


				$this->array_sort_by($auxganador,'repeticiones');
				$cont1=0;
				foreach($auxganador as $b=>$auxf2){

					if($cont1==2){
						unset($auxganador[$b]);	
					}else{
						$cont1++;
					}
				}


				//Promocionamos a los 4 primeros ganadores al calendario de Torneo

				for ($u = 0; $u < 1; $u++) {
					$enfrent = new Enfrentamiento();
					$enfrent->set_IdCategoria($auxganador[$u]->getId_Categoria());
					$enfrent->set_IdPareja1($auxganador[$u]->getId_Pareja());
					$enfrent->set_IdPareja2($auxganador[(count($auxganador)-1)-$u]->getId_Pareja());
					$enfrent->set_IdPista($this->PistaMapper->findAll()[0]->getId());
					$enfrent->setId_Hora($this->PistaMapper->findHoras()[0]->getId());
					$enfrent->set_Set1(" - ");
					$enfrent->set_Set2(" - ");
					$enfrent->set_Set3(" - ");
					$enfrent->set_Resultado(" - ");
					$enfrent->set_Tipo("Final");
					$enfrent->set_Fecha($campeonato->getFecha_ini());

					try {
						$enfrent->checkIsValid();
		
						if (!$this->EnfrentamientoMapper->enfExistsCamp($auxganador[$u]->getId_Categoria(),$enfrent->getId_Pareja1(),$enfrent->getId_Pareja2())){
						$this->EnfrentamientoMapper->save($enfrent);
						}
		
					} catch (ValidationException $ex) {
						$errors = $ex->getErrors();
						$this->view->setVariable("errors", $errors);
					}
				}

				$aux = array();
			}
		}
		

		//Recorrer los enfrentamientos por tipo campeonato y hacer update de horas,pista y fecha

		//Rellenamos Calendario Regular con Pista,Hora, y Fecha

		$enfrentamientos0 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Final");
		$lastenf = $this->EnfrentamientoMapper->findLastEnfTipo($campeonato->getId(),"Semifinal");
		$horas = $this->PistaMapper->findHoras();
		$pistas = $this->PistaMapper->findAll();

		/*//calculamos el index de la ultima hora
		$horindex=0;
		$horaux=0;
		foreach($horas as $hor){
			if($hor->getId() == $lastenf->getId_Hora()){
				$horindex=$horaux;
			}
			$horaux++;
		}

		//calculamos el index de la ultima pista
		$pistindex=0;
		$pistaux=0;
		foreach($pistas as $pist){
			if($pist->getId() == $lastenf->getId_Pista()){
				$pistindex=$pistaux;
			}
			$pistaux++;
		}*/
		

		//Pone Horas
		//$conth = $horindex;
		$conth = -1;
		foreach($enfrentamientos0 as $enf0){

			if($conth < count($horas)-1){
				$conth++;
			}else{
				$conth = 0;
			}
			$this->EnfrentamientoMapper->update_Hora_Enf($enf0->getId(),$horas[$conth]->getId());
		}

		$fechas = $this->calc_dias_from($lastenf->getFecha()."",$campeonato->getFecha_fin()."");

		
		//calculamos el index de la fecha
		$dateindex=0;
		$dateaux=0;
		foreach($fechas as $fech){
			if($fech == $lastenf->getFecha()){
				$dateindex=$dateaux;
			}
			$dateaux++;
		}

		//Pone Pistas
		$enfrentamientos1 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Final");
		$sumacont=FALSE;
		$sumareset=FALSE;
		//$contp = $pistindex;
		$contp = 0;
		$contd=$dateindex+1;

		//compruebo si anteriormente se salto de dia o se estaba en la ultima hora de alguna pista (No utilizado por saltar el dia para cada fase)
		/*if(($lastenf->getId_Hora() == end($horas)->getId()) && ($lastenf->getId_Pista() == end($pistas)->getId())){
			$contp=0;
			$contd++;
		}else if($lastenf->getId_Hora() == end($horas)->getId()){
			$contp++;
		}*/
		
		foreach($enfrentamientos1 as $enf1){
			if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() != end($pistas)->getId())){
				$sumacont=TRUE;
			}
			
			if(($enf1->getId_Hora() == end($horas)->getId()) && ($pistas[$contp]->getId() == end($pistas)->getId())){
				$sumareset=TRUE;
			}
			$this->EnfrentamientoMapper->update_Pista_Enf($enf1->getId(),$pistas[$contp]->getId());
			$this->EnfrentamientoMapper->update_Pista_Day($enf1->getId(),$fechas[$contd]);

			if($sumacont){
				$contp++;
				$sumacont=FALSE;
			}

			if($sumareset){
				$contp=0;
				$contd++;
				$sumareset=FALSE;
			}

		}

		//Prereservamos las pistas,fechas y horas para los enfrentamientos de Campeonato

		$enfrentamientos2 = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Final");
			
		foreach($enfrentamientos2 as $enf2){
		
		$prereservaenf = new ReservaEnf();
		$prereservaenf->setId_enfrentamiento($enf2->getId());
		
		try {
			$prereservaenf->checkIsValid();
					
			if (!$this->ReservaEnfMapper->enfExists($enf2->getId())){
				$this->ReservaEnfMapper->save($prereservaenf);
			}
					
			} catch (ValidationException $ex) {
			$errors = $ex->getErrors();
			$this->view->setVariable("errors", $errors);
			}
		
		}
		

		//Obtenemos todas las prereservas para la categoria Regular del campeonato y hacemos efectiva la reserva y eliminamos las prereservas

		$reservasenfs = $this->ReservaEnfMapper->findByIdCampTipo($campeonato->getId(),"Semifinal");

		foreach($reservasenfs as $resenf){

			$reservafinal = new Reserva();
			$reservafinal->setId_pista($resenf->getId_pista());
			$reservafinal->setId_usuario($resenf->getId_Usuario());
			$reservafinal->setId_hora($resenf->getId_Hora());
			$reservafinal->setFecha($resenf->getFecha());
			
					try {
						$reservafinal->checkIsValid();
						
						if (!$this->PistaMapper->findReserva($resenf->getId_pista(),$resenf->getId_hora(),$resenf->getFecha())){

							$this->PistaMapper->saveReserva($reservafinal);
						}
						$reservaborrar = new ReservaEnf();
						$reservaborrar->setId($resenf->getId());
						$reservaborrar->setId_enfrentamiento($resenf->getId_Enfrentamiento());
						$this->ReservaEnfMapper->delete($reservaborrar);
						
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
		}
		
		$this->view->redirect("categorias", "index", "idcamp=" . $campeonato->getId() . "");

	}

	public function simulaFinal()
	{

		if (!isset($_REQUEST["id_campeonato"])) {
			throw new Exception("id campeonato is mandatory");
		}
		$id_campeonato = $_REQUEST["id_campeonato"];

		$campeonato = $this->CampeonatoMapper->findById($id_campeonato);


		//Simulamos los resultados para la enfrentamientos Regulares del Campeonato y generas un array para los ganadores

		$ganadores = array();
		$enfrentamientos = $this->EnfrentamientoMapper->findByIdCampTipo($campeonato->getId(),"Final");

		foreach($enfrentamientos as $enf){
			$results = array();
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Set());
			array_push($results,$this->calc_Result($results[0][1],$results[1][1],$results[2][1]));

			if($results[3][0] > $results[3][4]){
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja1(),0));
			}else{
				array_push($ganadores,new Ganador($enf->getId(),$enf->getId_Categoria(),$enf->getId_Pareja2(),0));
			}

			$this->EnfrentamientoMapper->update_Result_Enf($enf->getId(),$results[0][0],$results[1][0],$results[2][0],$results[3]);
		}


		$aux = array();

		foreach($ganadores as $i=>$ganador){
			if($ganador != end($ganadores)){
				if(($ganador->getId_Categoria() != $ganadores[$i+1]->getId_Categoria())){

					array_push($aux,$ganador);
					$repeticiones=array();
					$auxganador = array();
					foreach($aux as $i=>$aux1){
						$cont=0;
						foreach($aux as $aux2){
							if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
								$cont++;
							}

							if($aux2 == end($aux)){
								array_push($repeticiones,$cont);
							}
						}
						array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
					}

					//Filtro los ganadores del grupo repetidos

					foreach($auxganador as $a=>$auxf){
						$flag=FALSE;
						foreach($auxganador as $auxf1){
							if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
								if($flag){
								unset($auxganador[$a]);
								}else{
									$flag=TRUE;
								}
							}
						}
					}

				//Filtramos el array de ganadores del campeoneonato


					$this->array_sort_by($auxganador,'repeticiones');
					$cont1=0;
					foreach($auxganador as $b=>$auxf2){

						if($cont1==2){
							unset($auxganador[$b]);	
						}else{
							$cont1++;
						}
					}


					$aux = array();
				}else{
					array_push($aux,$ganador);
				}
			}else{

				array_push($aux,$ganador);
				$repeticiones=array();
				$auxganador = array();
				foreach($aux as $i=>$aux1){
					$cont=0;
					foreach($aux as $aux2){
						if($aux1->getId_Pareja() == $aux2->getId_Pareja()){
							$cont++;
						}

						if($aux2 == end($aux)){
							array_push($repeticiones,$cont);
						}
					}
					array_push($auxganador,new Ganador($aux1->getId(),$aux1->getId_Categoria(),$aux1->getId_Pareja(),$repeticiones[$i]));
				}

				//Filtro los ganadores del grupo repetidos

				foreach($auxganador as $a=>$auxf){
					$flag=FALSE;
					foreach($auxganador as $auxf1){
						if($auxf->getId_Pareja() == $auxf1->getId_Pareja()){
							if($flag){
							unset($auxganador[$a]);
							}else{
								$flag=TRUE;
							}
						}
					}
				}

				//Filtramos el array de ganadores del campeoneonato


				$this->array_sort_by($auxganador,'repeticiones');
				$cont1=0;
				foreach($auxganador as $b=>$auxf2){

					if($cont1==2){
						unset($auxganador[$b]);	
					}else{
						$cont1++;
					}
				}

				$aux = array();
			}
		}
		

		//Obtenemos todas las prereservas para la categoria Regular del campeonato y hacemos efectiva la reserva y eliminamos las prereservas

		$reservasenfs = $this->ReservaEnfMapper->findByIdCampTipo($campeonato->getId(),"Final");

		foreach($reservasenfs as $resenf){

			$reservafinal = new Reserva();
			$reservafinal->setId_pista($resenf->getId_pista());
			$reservafinal->setId_usuario($resenf->getId_Usuario());
			$reservafinal->setId_hora($resenf->getId_Hora());
			$reservafinal->setFecha($resenf->getFecha());
			
					try {
						$reservafinal->checkIsValid();
						
						if (!$this->PistaMapper->findReserva($resenf->getId_pista(),$resenf->getId_hora(),$resenf->getFecha())){

							$this->PistaMapper->saveReserva($reservafinal);
						}
						$reservaborrar = new ReservaEnf();
						$reservaborrar->setId($resenf->getId());
						$reservaborrar->setId_enfrentamiento($resenf->getId_Enfrentamiento());
						$this->ReservaEnfMapper->delete($reservaborrar);
						
						} catch (ValidationException $ex) {
							$errors = $ex->getErrors();
							$this->view->setVariable("errors", $errors);
						}
		}
		
		$this->view->redirect("categorias", "index", "idcamp=" . $campeonato->getId() . "");

	}



	public function update()
	{

		if (!isset($_REQUEST["idcategoria"])) {
			throw new Exception("Categoria id is mandatory");
		}

		$idcategoria = $_REQUEST["idcategoria"];
		$categoria = $this->CategoriaMapper->findById($idcategoria);

		if ($categoria == NULL) {
			throw new Exception("No existe categoria con id: " . $idcategoria);
		}

		if (isset($_POST["nombre"])) {

			$categoria->setNombre($_POST["nombre"]);

			try {
				$categoria->checkIsValid();

				$this->CategoriaMapper->update($categoria);
				$this->view->setFlash(sprintf("Categoria \"%s\" modificada.", $categoria->getNombre()));
				$this->view->redirect("categorias", "index", "idcamp=" . $categoria->getId_Campeonato() . "");
			} catch (ValidationException $ex) {
				$errors = $ex->getErrors();
				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->setVariable("categoria", $categoria);
		$this->view->render("categorias", "update");
	}
}

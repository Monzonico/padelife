<?php
require_once(__DIR__."/../core/ViewManager.php");
//require_once(__DIR__."/../core/I18n.php");
require_once(__DIR__."/../model/Campeonato.php");
require_once(__DIR__."/../model/CampeonatoMapper.php");
require_once(__DIR__."/../model/Pista.php");
require_once(__DIR__."/../model/PistaMapper.php");
require_once(__DIR__."/../controller/BaseController.php");



class CampeonatosController extends BaseController {


	private $CampeonatoMapper;
	public function __construct() {
		parent::__construct();

		$this->CampeonatoMapper = new CampeonatoMapper();
		$this->PistaMapper = new PistaMapper();
		$this->view->setLayout("default");
	}

	function console_log($msg){
		echo "<script type='text/javascript'>alert('$msg');</script>";
	}

	public function index() {
		$campeonatos = $this->CampeonatoMapper->findAll();
		$this->view->setVariable("campeonatos", $campeonatos);
		$this->view->render("campeonatos", "index");
	}

	public function add() {
		
		$campeonato = new Campeonato();


		if (isset($_POST["nombre"]) && isset($_POST["normativa"]) && isset($_POST["fechaIniCamp"]) && isset($_POST["fechaFinCamp"]) && isset($_POST["fechIniInscri"]) && isset($_POST["fechaFinInscri"])){

			$campeonato->setNombre($_POST["nombre"]);
			$campeonato->setNormativa($_POST["normativa"]);
			$campeonato->setFecha_ini($_POST["fechaIniCamp"]);
			$campeonato->setFecha_fin($_POST["fechaFinCamp"]);
			$campeonato->setFecha_ini_inscri($_POST["fechIniInscri"]);
			$campeonato->setFecha_fin_inscri($_POST["fechaFinInscri"]);

			$diasaux=array();
			$fechascamp = array();
			$flag=TRUE;

			$ini = new DateTime($_POST["fechaIniCamp"]);
			$fin = new Datetime($_POST["fechaFinCamp"]);
			$fin = $fin->modify( '+1 day' ); 
			$interval = new DateInterval('P1D');
			$daterange = new DatePeriod($ini, $interval ,$fin);
			array_push($diasaux,$daterange);
			   
			foreach($diasaux as $i=>$date){
				foreach($diasaux[$i] as $dia){
					array_push($fechascamp,$dia->format('Y-m-d'));
				}
			}

			foreach($fechascamp as $fech){
				if($this->PistaMapper->findReservaDate($fech)){
					$flag=FALSE;
				}
			}

			try{
				$campeonato->checkIsValid();

				if ($flag){
					$this->CampeonatoMapper->save($campeonato);
					$this->view->setFlash("Campeonato ".$campeonato->getNombre()."  agregado.");
					$this->view->redirect("campeonatos", "index");
				}else{
					$errors = array();
					$errors["general"] = "No se puede crear el campeonato en fechas con reservas";
					$this->view->setVariable("errors", $errors);
				}

			}catch(ValidationException $ex) {
				$errors = $ex->getErrors();

				$this->view->setVariable("errors", $errors);
			}

		}
		$this->view->render("campeonatos", "add");

	}

	public function delete() {
		if (!isset($_REQUEST["id"])) {
			throw new Exception("id campeonato es necesario");
		}
	
		$campeonatoID = $_REQUEST["id"];
		$campeonato = $this->CampeonatoMapper->findById($campeonatoID);

		if ($campeonato == NULL) {
			throw new Exception("No existe campeonato con id_campeonato: ".$campeonatoID);
		}else
		{
			$this->CampeonatoMapper->delete($campeonato);
			$this->view->setFlash(sprintf("Campeonato \"%s\" eliminado."),$campeonato->getNombre());	
		}
		$this->view->redirect("campeonatos", "index");
	}

	public function update() {

        if (!isset($_REQUEST["id"])) {
            throw new Exception("Campeonato id es necesario");
		}

        $campeonatoID = $_REQUEST["id"];
		$campeonato = $this->CampeonatoMapper->findById($campeonatoID);

        if ($campeonato == NULL) {
            throw new Exception("No existe Campeonato con id: ".$campeonatoID);
		}

		if (isset($_POST["nombre"]) && isset($_POST["normativa"]) && isset($_POST["fechaIniCamp"]) && isset($_POST["fechaFinCamp"]) && isset($_POST["fechIniInscri"]) && isset($_POST["fechaFinInscri"])){

			$campeonato->setNombre($_POST["nombre"]);
			$campeonato->setNormativa($_POST["normativa"]);
			$campeonato->setFecha_ini($_POST["fechaIniCamp"]);
			$campeonato->setFecha_fin($_POST["fechaFinCamp"]);
			$campeonato->setFecha_ini_inscri($_POST["fechIniInscri"]);
			$campeonato->setFecha_fin_inscri($_POST["fechaFinInscri"]);

            try {
					$campeonato->checkIsValid();

                    $this->CampeonatoMapper->update($campeonato);
					$this->view->setFlash(sprintf("Campeonato \"%s\" actualizado.",$campeonato->getNombre()));
					$this->view->redirect("campeonatos", "index");
					
            }catch(ValidationException $ex) {
                $errors = $ex->getErrors();
                $this->view->setVariable("errors", $errors);
            }
		}
		$this->view->setVariable("campeonato", $campeonato);
		$this->view->render("campeonatos", "update");
	}

}

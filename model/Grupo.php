<?php

require_once(__DIR__."/../core/ValidationException.php");

class Grupo {
	
	private $id;
	private $id_categoria;
	private $nombre;

	public function __construct($id=NULL, $id_categoria=NULL, $nombre=NULL) {
		$this->id = $id;
		$this->id_categoria = $id_categoria;
		$this->nombre = $nombre;

	}
	 
	public function getId(){
		return $this->id;
	}

	public function getId_Categoria(){
		return $this->id_categoria;
	}

	public function getNombre() {
		return $this->nombre;
	}

	public function setId($id){
		$this->id = $id;
	} 

	public function set_IdCategoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	} 

	public function checkIsValid() {

	}
}
?>
<?php

require_once(__DIR__."/../core/PDOConnection.php");

class PistaMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Pista $pista) {
		$stmt = $this->db->prepare("INSERT INTO pistas(nombre) values (?)");
		$stmt->execute(array($pista->getNombre()));
		return $this->db->lastInsertId();
	}

	public function delete(Pista $pista){
        $sql = $this->db->prepare("DELETE FROM pistas where id=?");
        $sql->execute(array($pista->getId()));
	}

	public function deleteReserva($reserva){
        $sql = $this->db->prepare("DELETE FROM reservas where id=?");
        $sql->execute(array($reserva->getId()));
	}

	public function deletePrereservas($id_hora, $id_pista, $fecha){
		$stmt = $this->db->query("DELETE FROM prereservas where id_hora = $id_hora and id_pista = $id_pista and fecha = '$fecha' ");
		$stmt->execute(array($id_hora, $id_pista, $fecha));
	}

	public function deleteClaseByReserva($id_reserva){
		$stmt = $this->db->prepare("DELETE FROM reservas WHERE id=?");
		$stmt->execute(array($id_reserva));
	}

	public function deleteInscriByClase($id_clase,$id_usuario){
		$stmt = $this->db->prepare("DELETE FROM clases_asistentes WHERE id_clase=? AND id_usuario=?");
		$stmt->execute(array($id_clase,$id_usuario));
	}

	public function update(Pista $pista){
		$sql = $this->db->prepare("UPDATE pistas SET nombre=? where id=?");
		$sql->execute(array($pista->getNombre(), $pista->getId()));
	}

	public function updateClase(Clase $clase){
		$sql = $this->db->prepare("UPDATE clases SET nombre=?, descripcion=?, capacidad=? where id=?");
		$sql->execute(array($clase->getNombre(), $clase->getDescripcion(), $clase->getCapacidad(),$clase->getId()));
	}

	public function findReserva($id_pista,$id_hora, $fecha){
		$stmt = $this->db->query("SELECT id FROM reservas where id_pista=$id_pista and id_hora=$id_hora and fecha='$fecha'");
		$stmt->execute(array($id_pista, $id_hora, $fecha));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result != null) {
			return true;
		}else{
			return false;
		}
	}

	public function findReservaDate($fecha){
		$stmt = $this->db->query("SELECT id FROM reservas where fecha='$fecha'");
		$stmt->execute(array($fecha));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result != null) {
			return true;
		}else{
			return false;
		}
	}

	public function findPrereserva($id_pista,$id_hora, $fecha){
		$stmt = $this->db->query("SELECT id FROM prereservas where id_pista=$id_pista and id_hora=$id_hora and fecha='$fecha'");
		$stmt->execute(array($id_pista, $id_hora, $fecha));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result != null) {
			return true;
		}else{
			return false;
		}
	}

	public function partidoIsFull($id_hora,$id_pista, $fecha){
		$stmt = $this->db->query("SELECT COUNT(id) as cuenta FROM prereservas WHERE id_hora = $id_hora AND id_pista= $id_pista AND fecha= '$fecha' GROUP BY id_hora, id_pista, fecha HAVING (cuenta) >= 4");
		$stmt->execute(array($id_hora, $id_pista, $fecha));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result["cuenta"] >= 4) {
			return true;
		}else{
			return false;
		}
	}

	public function isFullReservas($id_usuario){
		$stmt = $this->db->query("SELECT COUNT(id) as cuenta FROM reservas WHERE id_usuario = $id_usuario HAVING (cuenta) >= 5");
		$stmt->execute(array($id_usuario));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result["cuenta"] >= 5) {
			return true;
		}else{
			return false;
		}
	}

	public function isFullClase(Clase $clase){
		$stmt = $this->db->prepare("SELECT COUNT(id) as cuenta FROM clases_asistentes WHERE id_clase = ? HAVING (cuenta) >= ?");
		$stmt->execute(array($clase->getId(),$clase->getCapacidad()));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($result["cuenta"] >= $clase->getCapacidad()) {
			return true;
		}else{
			return false;
		}
	}

	public function isClasExist($id_clase,$id_usuario) {
		$stmt = $this->db->prepare("SELECT COUNT(*) FROM clases_asistentes WHERE id_clase = ? AND id_usuario = ?");
		$stmt->execute(array($id_clase, $id_usuario));
		if($stmt->fetchColumn()>0){
			return true;
		}else
			return false;
	}

	public function MeInInscritos($id_usuario) {
		$stmt = $this->db->prepare("SELECT COUNT(*) FROM clases_asistentes WHERE id_usuario=?");
		$stmt->execute(array($id_usuario));
		if($stmt->fetchColumn()>0){
			return true;
		}else
			return false;
	}

	public function saveReserva(Reserva $reserva) {
		$stmt = $this->db->prepare("INSERT INTO reservas(id_pista,id_usuario,id_hora,fecha) values (?,?,?,?)");
		$stmt->execute(array($reserva->getId_pista(),$reserva->getId_usuario(),$reserva->getId_hora(),$reserva->getFecha()));
		return $this->db->lastInsertId();
	}

	public function savePrereserva(Partido $partido) {
		$stmt = $this->db->prepare("INSERT INTO prereservas(id_usuario,id_hora,id_pista,fecha) values (?,?,?,?)");
		$stmt->execute(array($partido->getId_usuario(),$partido->getId_hora(),$partido->getId_pista(),$partido->getFecha()));
		return $this->db->lastInsertId();
	}

	public function savePartido(PartidoPromo $partido) {
		$stmt = $this->db->prepare("INSERT INTO partidos(id_reserva,id_pista,id_hora,id_usuario1,id_usuario2,id_usuario3,id_usuario4,fecha) values (?,?,?,?,?,?,?,?)");
		$stmt->execute(array($partido->getId_reserva(),$partido->getId_pista(),$partido->getId_hora(),$partido->getId_usuario1(),$partido->getId_usuario2(),$partido->getId_usuario3(),$partido->getId_usuario4(),$partido->getFecha()));
		return $this->db->lastInsertId();
		echo "<script>console.log( ' ENTRA EN SAVEPARTIDO' );</script>";
	}

	public function saveClase(Clase $clase) {
		$stmt = $this->db->prepare("INSERT INTO clases(id_entrenador,id_reserva,id_pista,id_hora,fecha,nombre,descripcion,capacidad) values (?,?,?,?,?,?,?,?)");
		$stmt->execute(array($clase->getEntrenador(),$clase->getReserva(),$clase->getPista(),$clase->getHora(),$clase->getFecha(),$clase->getNombre(),$clase->getDescripcion(),$clase->getCapacidad()));
		return $this->db->lastInsertId();
	}

	public function saveAsist(Asistente $asistente) {
		$stmt = $this->db->prepare("INSERT INTO clases_asistentes(id_clase,id_usuario) values (?,?)");
		$stmt->execute(array($asistente->getId_Clase(),$asistente->getId_Usuario()));
		return $this->db->lastInsertId();
	}

	public function findIdhora($hora){
		$stmt = $this->db->query("SELECT id FROM horas where hora_ini='$hora'");
		$stmt->execute(array($hora));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if($result != null) {
			return $result["id"];
		} else {
			return NULL;
		}
	}

	public function findIdPrereservas($id_hora,$id_pista, $fecha){
		$stmt = $this->db->query("SELECT id_usuario FROM prereservas where id_hora = $id_hora and id_pista = $id_pista and fecha = '$fecha'");
		$stmt->execute(array($id_hora, $id_pista, $fecha));
		$result_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$ids = array();
		if($result_db != null) {
			foreach ($result_db as $id) {
				array_push($ids, $id['id_usuario']);
			}
			return $ids;
		} else {
			return NULL;
		}
	}

	public function findMyPrereservas($id_usuario){
		$stmt = $this->db->prepare("SELECT prereservas.id,pistas.nombre as 'nombrepista',prereservas.fecha,horas.hora_ini,horas.hora_fin FROM ( prereservas INNER JOIN horas ON prereservas.id_hora = horas.id INNER JOIN usuarios ON prereservas.id_usuario = usuarios.id INNER JOIN pistas ON prereservas.id_pista = pistas.id) WHERE id_usuario = $id_usuario");
		$stmt->execute();
		$mypartidos_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$mypartidos = array();

		foreach ($mypartidos_db as $mypartido) {
			array_push($mypartidos, new PartidoList($mypartido["id"], $mypartido["nombrepista"], $mypartido["fecha"], $mypartido["hora_ini"], $mypartido["hora_fin"]));
		}
		return $mypartidos;
	}

	public function findAllClases(){
		$stmt = $this->db->prepare("SELECT * FROM clases");
		$stmt->execute();
		$clases_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$clases = array();

		if($clases_db != null) {
			foreach ($clases_db as $clase) {
				array_push($clases, new Clase($clase["id"], $clase["id_reserva"], $clase["id_entrenador"], $clase["id_pista"], $clase["id_hora"], $clase["fecha"], $clase["nombre"], $clase["descripcion"], $clase["capacidad"]));
			}
			return $clases;
		} else {
			return NULL;
		}
	}

	public function findMyInscritos($id_usuario){
		$stmt = $this->db->prepare("SELECT clases.id as 'id', clases.id_reserva as 'reserva', clases.id_entrenador as 'entrenador', clases.id_pista as 'pista', clases.id_hora as 'hora', clases.fecha as 'fecha', clases.nombre as 'nombre', clases.descripcion as 'descripcion', clases.capacidad as 'capacidad' FROM clases INNER JOIN clases_asistentes ON clases.id = clases_asistentes.id_clase WHERE clases_asistentes.id_usuario = ?");
		$stmt->execute(array($id_usuario));
		$clases_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$clases = array();

		if($clases_db != null) {
			foreach ($clases_db as $clase) {
				array_push($clases, new Clase($clase["id"], $clase["reserva"], $clase["entrenador"], $clase["pista"], $clase["hora"], $clase["fecha"], $clase["nombre"], $clase["descripcion"], $clase["capacidad"]));
			}
			return $clases;
		} else {
			return NULL;
		}
	}

		public function reservasByid($id_usuario){
		$stmt = $this->db->prepare("SELECT reservas.id,pistas.nombre,reservas.fecha,horas.hora_ini,horas.hora_fin FROM ((reservas INNER JOIN pistas ON reservas.id_pista = pistas.id) INNER JOIN horas ON reservas.id_hora = horas.id) WHERE id_usuario = ?");
		$stmt->execute(array($id_usuario));
		$reserva_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$reservas = array();

		if($reserva_db != null) {
			foreach ($reserva_db as $reserva) {
				array_push($reservas, new ReservaList($reserva["id"], $reserva["nombre"], $reserva["fecha"], $reserva["hora_ini"], $reserva["hora_fin"]));
			}
			return $reservas;
		} else {
			return NULL;
		}
	}
	
	public function findClases(){
		$stmt = $this->db->prepare("SELECT usuarios.nombre as 'entrenador',pistas.nombre as 'pista',reservas.fecha,horas.hora_ini,horas.hora_fin FROM (reservas INNER JOIN pistas ON reservas.id_pista = pistas.id INNER JOIN usuarios ON reservas.id_usuario = usuarios.id INNER JOIN horas ON reservas.id_hora = horas.id) WHERE usuarios.rol = 'entrenador'");
		$stmt->execute();
		$clases_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$clases = array();

		if($clases_db != null) {
			foreach ($clases_db as $clase) {
				array_push($clases, new Clase($clase["entrenador"], $clase["pista"], $clase["fecha"], $clase["hora_ini"], $clase["hora_fin"]));
			}
			return $clases;
		} else {
			return NULL;
		}
	}

	public function findAll(){
		$stmt = $this->db->query("SELECT * FROM pistas");
		$pista_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$pistas = array();

		foreach ($pista_db as $pista) {
			array_push($pistas, new Pista($pista["id"], $pista["nombre"]));
		}
		return $pistas;
	}

	public function findAllPartidos(){
		$stmt = $this->db->prepare("SELECT prereservas.id,pistas.nombre as 'nombrepista',prereservas.fecha,horas.hora_ini,horas.hora_fin FROM ( prereservas INNER JOIN horas ON prereservas.id_hora = horas.id INNER JOIN usuarios ON prereservas.id_usuario = usuarios.id INNER JOIN pistas ON prereservas.id_pista = pistas.id) GROUP BY prereservas.id_hora,prereservas.id_pista,prereservas.fecha ORDER BY prereservas.fecha ASC;");
		$stmt->execute();
		$partido_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$partidos = array();

		foreach ($partido_db as $partido) {
			array_push($partidos, new PartidoList($partido["id"], $partido["nombrepista"], $partido["fecha"], $partido["hora_ini"], $partido["hora_fin"]));
		}
		return $partidos;
	}

	public function findAllPromociones(){
		$stmt = $this->db->query("SELECT * FROM partidos");
		$promocionados_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$promocionados = array();

		foreach ($promocionados_db as $promocionado) {
			array_push($promocionados, new PartidoPromo($promocionado["id"], $promocionado["id_pista"], $promocionado["id_hora"], $promocionado["id_usuario1"], $promocionado["id_usuario2"], $promocionado["id_usuario3"], $promocionado["id_usuario4"], $promocionado["fecha"]));
		}
		return $promocionados;
	}

	public function findAllReservaEnf(){
		$stmt = $this->db->query("SELECT * FROM prereservas_enf");
		$reservasenf_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$reservassenf = array();

		foreach ($reservasenf_db as $reservaenf) {
			array_push($reservassenf, new ReservaEnf($reservaenf["id"], $reservaenf["id_enfrentamiento"], $reservaenf["id_pista"], $reservaenf["id_hora"], $reservaenf["fecha"]));
		}
		return $reservassenf;
	}

	public function findMyPromociones($id_usuario){
		$stmt = $this->db->query("SELECT * FROM partidos WHERE id_usuario1 = $id_usuario OR id_usuario2 = $id_usuario OR id_usuario3 = $id_usuario OR id_usuario4 = $id_usuario");
		$promocionados_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$promocionados = array();

		foreach ($promocionados_db as $promocionado) {
			array_push($promocionados, new PartidoPromo($promocionado["id"], $promocionado["id_reserva"],$promocionado["id_pista"], $promocionado["id_hora"], $promocionado["id_usuario1"], $promocionado["id_usuario2"], $promocionado["id_usuario3"], $promocionado["id_usuario4"], $promocionado["fecha"]));
		}
		return $promocionados;
	}

	public function findMyPromocionesList($id_usuario){
		$stmt = $this->db->query("SELECT partidos.id,pistas.nombre as pista,reservas.fecha,horas.hora_ini,horas.hora_fin,usu1.nombre as jugador_1,usu2.nombre as jugador_2,usu3.nombre as jugador_3,usu4.nombre as jugador_4 FROM (partidos INNER JOIN pistas ON partidos.id_pista = pistas.id INNER JOIN reservas ON partidos.id_reserva = reservas.id INNER JOIN usuarios as usu1 ON partidos.id_usuario1 = usu1.id INNER JOIN usuarios as usu2 ON partidos.id_usuario2 = usu2.id INNER JOIN usuarios as usu3 ON partidos.id_usuario3 = usu3.id INNER JOIN usuarios as usu4 ON partidos.id_usuario4 = usu4.id INNER JOIN horas ON partidos.id_hora = horas.id) WHERE id_usuario1 = $id_usuario OR id_usuario2 = $id_usuario OR id_usuario3 = $id_usuario OR id_usuario4 = $id_usuario");
		$promocionados_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$promocionados = array();

		foreach ($promocionados_db as $promocionado) {
			array_push($promocionados, new PartidoPromoList($promocionado["id"], $promocionado["pista"],$promocionado["fecha"], $promocionado["hora_ini"], $promocionado["hora_fin"], $promocionado["jugador_1"], $promocionado["jugador_2"], $promocionado["jugador_3"], $promocionado["jugador_4"]));
		}
		return $promocionados;
	}

	public function findHoras(){
		$stmt = $this->db->query("SELECT * FROM horas");
		$hora_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$horas = array();

		foreach ($hora_db as $hora) {
			array_push($horas, new Hora($hora["id"], $hora["hora_ini"], $hora["hora_fin"]));
		}
		return $horas;
	}

	public function findHorasById($id_hora){
		$stmt = $this->db->prepare("SELECT * FROM horas WHERE id=?");
		$stmt->execute(array($id_hora));
		$hora = $stmt->fetch(PDO::FETCH_ASSOC);
		if($hora != null) {
			return new Hora(
			$hora["id"],
			$hora["hora_ini"],
			$hora["hora_fin"]);
		} else {
			return NULL;
		}
	}

	public function findByIdClase($id_clase){
		$stmt = $this->db->prepare("SELECT * FROM clases WHERE id=?");
		$stmt->execute(array($id_clase));
		$clase = $stmt->fetch(PDO::FETCH_ASSOC);
		if($clase != null) {
			return new Clase(
			$clase["id"],
			$clase["id_reserva"],
			$clase["id_entrenador"],
			$clase["id_pista"],
			$clase["id_hora"],
			$clase["fecha"],
			$clase["nombre"],
			$clase["descripcion"],
			$clase["capacidad"]);
		} else {
			return NULL;
		}
	}

	public function findById($id){
		$stmt = $this->db->prepare("SELECT * FROM pistas WHERE id=?");
		$stmt->execute(array($id));
		$pista = $stmt->fetch(PDO::FETCH_ASSOC);
		if($pista != null) {
			return new Pista(
			$pista["id"],
			$pista["nombre"]);
		} else {
			return NULL;
		}
	}

	public function findReservabyid($idreserva){
		$stmt = $this->db->prepare("SELECT * FROM reservas WHERE id=?");
		$stmt->execute(array($idreserva));
		$reserva = $stmt->fetch(PDO::FETCH_ASSOC);
		if($reserva != null) {
			return new Reserva(
			$reserva["id"],
			$reserva["id_pista"],
			$reserva["id_usuario"],
			$reserva["id_hora"],
			$reserva["fecha"]);
		} else {
			return NULL;
		}
	}

	public function findReservabyUserAll($id_usuario,$id_hora,$id_pista,$fecha){
		$stmt = $this->db->prepare("SELECT * FROM reservas WHERE id_pista=? AND id_usuario=? AND id_hora=? AND fecha=?");
		$stmt->execute(array($id_pista,$id_usuario,$id_hora,$fecha));
		$reserva = $stmt->fetch(PDO::FETCH_ASSOC);
		if($reserva != null) {
			return new Reserva(
			$reserva["id"],
			$reserva["id_pista"],
			$reserva["id_usuario"],
			$reserva["id_hora"],
			$reserva["fecha"]);
		} else {
			return NULL;
		}
	}

	public function PrereservasById($idprereserva){
		$stmt = $this->db->prepare("SELECT * FROM prereservas WHERE id=?");
		$stmt->execute(array($idprereserva));
		$partido = $stmt->fetch(PDO::FETCH_ASSOC);
		if($partido != null) {
			return new Partido(
			$partido["id"],
			$partido["id_usuario"],
			$partido["id_hora"],
			$partido["id_pista"],
			$partido["fecha"]);
		} else {
			return NULL;
		}
	}

	public function isValidPista($pista) {
		$stmt = $this->db->prepare("SELECT count(nombre) FROM pistas where nombre=?");
		$stmt->execute(array($pista));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}
}
?>

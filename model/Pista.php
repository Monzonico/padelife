<?php

require_once(__DIR__."/../core/ValidationException.php");

class Pista{
  private $id;
  private $nombre;

  public function __construct($id=NULL, $nombre=NULL) {
		$this->id = $id;
		$this->nombre = $nombre;
	}

	public function getId(){
		return $this->id;
	}

	public function getNombre() {
		return $this->nombre;
	}
  public function setId($id){
		$this->id = $id;
	}

	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
  public function checkIsValid() {
    $errors = array();

    if (strlen($this->nombre) < 3) {
      $errors["nombre"] = "debe tener una longitud minima de 3";
    }
  
    if (sizeof($errors)>0){
      throw new ValidationException($errors, "Pista no valida");
    }
  }

}
 ?>

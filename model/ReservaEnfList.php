<?php

require_once(__DIR__."/../core/ValidationException.php");

class ReservaEnfList{
  private $id;
  private $id_enfrentamiento;
  private $id_pista;
  private $id_usuario;
  private $id_hora;
  private $fecha;

  public function __construct($id=NULL, $id_enfrentamiento=NULL, $id_pista=NULL, $id_usuario=NULL, $id_hora=NULL, $fecha=NULL) {
		$this->id = $id;
        $this->id_enfrentamiento = $id_enfrentamiento;
        $this->id_pista = $id_pista;
        $this->id_usuario = $id_usuario;
        $this->id_hora = $id_hora;
        $this->fecha = $fecha;
	}

	public function getId(){
		return $this->id;
	}

	public function getId_Enfrentamiento() {
		return $this->id_enfrentamiento;
    }
    public function getId_Pista() {
		return $this->id_pista;
    }
    public function getId_Usuario() {
		return $this->id_usuario;
    }
    public function getId_Hora() {
		return $this->id_hora;
    }
    public function getFecha() {
		return $this->fecha;
	}

  	public function setId($id){
		$this->id = $id;
    }
    
    public function setId_enfrentamiento($id_enfrentamiento) {
		$this->id_enfrentamiento = $id_enfrentamiento;
	}

	public function setId_Pista($id_pista) {
		$this->id_pista = $id_pista;
    }
    public function setId_Usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
    }

    public function setId_Hora($id_hora) {
		$this->id_hora = $id_hora;
    }

    public function setFecha($fecha) {
		$this->fecha = $fecha;
    }

  	public function checkIsValid() {
    $errors = array();
  }

}
 ?>
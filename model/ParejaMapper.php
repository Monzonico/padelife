<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class ParejaMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Pareja $pareja) {
		$stmt = $this->db->prepare("INSERT INTO parejas(id_categoria, id_capitan, id_jugador) values (?,?,?)");
		$stmt->execute(array($pareja->getId_Categoria(), $pareja->getId_Capitan(), $pareja->getId_Jugador()));
		return $this->db->lastInsertId();
	}

	public function saveGroup(Grupo $grupo) {
		$stmt = $this->db->prepare("INSERT INTO grupos(id_categoria, nombre) values (?,?)");
		$stmt->execute(array($grupo->getId_Categoria(), $grupo->getNombre()));
		return $this->db->lastInsertId();
	}

	public function delete(Pareja $pareja){
        $sql = $this->db->prepare("DELETE FROM parejas where id=?");
        $sql->execute(array($pareja->getId()));
	}

	public function findAllByCat($id_categoria){
		$stmt = $this->db->prepare("SELECT * FROM parejas WHERE id_categoria=?");
		$stmt->execute(array($id_categoria));
		$pareja_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$parejas = array();

		foreach ($pareja_db as $pareja) {
			array_push($parejas, new Pareja($pareja["id"], $pareja["id_categoria"], $pareja["id_capitan"], $pareja["id_jugador"]));
		}
		return $parejas;
	}

	public function findAllByIdCampeonato($campeonatoID){
		$stmt = $this->db->prepare("SELECT * FROM categorias WHERE id_campeonato=?");
		$stmt->execute(array($campeonatoID));
		$categoria_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$categorias = array();

		foreach ($categoria_db as $categoria) {
			array_push($categorias, new Categoria($categoria["id"], $categoria["id_campeonato"], $categoria["nombre"]));
		}
		return $categorias;
	}

	public function findAllInscritos($id_usuario,$id_campeonato){
		$stmt = $this->db->prepare("SELECT categorias.id AS id_categoria, categorias.nombre, categorias.nivel, U1.nombre AS capitan, U2.nombre AS pareja FROM categorias RIGHT JOIN parejas ON parejas.id_categoria = categorias.id RIGHT JOIN usuarios AS U1 ON parejas.id_capitan = U1.id RIGHT JOIN usuarios AS U2 ON parejas.id_jugador = U2.id WHERE (id_capitan=? OR id_jugador=?) AND categorias.id_campeonato=?");
		$stmt->execute(array($id_usuario,$id_usuario,$id_campeonato));
		$inscritos_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$inscritos = array();

		foreach ($inscritos_db as $inscrito) {
			array_push($inscritos, new Inscrito($inscrito["id_categoria"], $inscrito["nombre"], $inscrito["nivel"], $inscrito["capitan"], $inscrito["pareja"]));
		}
		return $inscritos;
	}

	public function findById($idcategoria){
		$stmt = $this->db->prepare("SELECT * FROM categorias WHERE id=?");
		$stmt->execute(array($idcategoria));
		$categoria = $stmt->fetch(PDO::FETCH_ASSOC);
		if($categoria != null) {
			return new Categoria(
			$categoria["id"],
			$categoria["id_campeonato"],
			$categoria["nombre"]);
		} else {
			return NULL;
		}
	}

	public function findByCat($id_categoria,$id_capitan,$id_pareja){
		$stmt = $this->db->prepare("SELECT * FROM parejas WHERE id_categoria=? AND id_capitan=? AND id_jugador=?");
		$stmt->execute(array($id_categoria,$id_capitan,$id_pareja));
		$pareja = $stmt->fetch(PDO::FETCH_ASSOC);
		if($pareja != null) {
			return new Pareja(
			$pareja["id"],
			$pareja["id_categoria"],
			$pareja["id_capitan"],
			$pareja["id_pareja"]);
		} else {
			return NULL;
		}
	}

	public function findByCatPar($id,$id_categoria){
		$stmt = $this->db->prepare("SELECT * FROM parejas WHERE id=? AND id_categoria=?");
		$stmt->execute(array($id,$id_categoria));
		$pareja = $stmt->fetch(PDO::FETCH_ASSOC);
		if($pareja != null) {
			return new Pareja(
			$pareja["id"],
			$pareja["id_categoria"],
			$pareja["id_capitan"],
			$pareja["id_jugador"]);
		} else {
			return NULL;
		}
	}
	
	public function countParejasByCat($id_categoria) {
		$stmt = $this->db->prepare("SELECT count(id) FROM parejas where id_categoria=?");
		$stmt->execute(array($id_categoria));
		return $stmt->fetchColumn();
	}

	public function isParejaExist($id_categoria, $id_capitan, $id_jugador) {
		$stmt = $this->db->prepare("SELECT COUNT(*) FROM parejas WHERE id_categoria = ? AND id_capitan = ? AND id_jugador = ?");
		$stmt->execute(array($id_categoria, $id_capitan, $id_jugador));
		if($stmt->fetchColumn()>0){
			return true;
		}else
			return false;
	}

	public function isParejaExist2($id_categoria, $id_jugador) {
		$stmt = $this->db->prepare("SELECT COUNT(*) FROM parejas WHERE id_categoria = ? AND id_jugador = ?");
		$stmt->execute(array($id_categoria, $id_jugador));
		if($stmt->fetchColumn()>0){
			return true;
		}else
			return false;
	}

	public function isCatExist($id_categoria,$id_jugador) {
		$stmt = $this->db->prepare("SELECT COUNT(*) FROM parejas WHERE id_categoria = ? AND (id_capitan = ? OR id_jugador = ?)");
		$stmt->execute(array($id_categoria, $id_jugador, $id_jugador));
		if($stmt->fetchColumn()>0){
			return true;
		}else
			return false;
	}



}

<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class NoticiaMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Noticia $noticia) {
		$stmt = $this->db->prepare("INSERT INTO noticias(titulo, texto) values (?,?)");
		$stmt->execute(array($noticia->getTitulo(), $noticia->getTexto()));
		return $this->db->lastInsertId();
	}

	public function delete(Noticia $noticia){
        $sql = $this->db->prepare("DELETE FROM noticias where id=?");
        $sql->execute(array($noticia->getId()));
	}
	
	public function update(Noticia $noticia){
		$sql = $this->db->prepare("UPDATE noticias SET titulo=?, texto=? where id=?");
		$sql->execute(array($noticia->getTitulo(), $noticia->getTexto(), $noticia->getId()));
	}

	public function findAll(){
		$stmt = $this->db->query("SELECT * FROM noticias");
		$noticias_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$noticias = array();

		foreach ($noticias_db as $noticia) {
			array_push($noticias, new Noticia($noticia["id"],$noticia["hora"], $noticia["titulo"], $noticia["texto"]));
		}
		return $noticias;
	}

	public function findById($id){
		$stmt = $this->db->prepare("SELECT * FROM noticias WHERE id=?");
		$stmt->execute(array($id));
		$noticia = $stmt->fetch(PDO::FETCH_ASSOC);
		if($noticia != null) {
			return new Noticia(
			$noticia["id"],
			$noticia["hora"],
			$noticia["titulo"],
			$noticia["texto"]);
		} else {
			return NULL;
		}
	}
	
	public function isValidNotice($noticia) {
		$stmt = $this->db->prepare("SELECT count(titulo) FROM noticias where titulo=?");
		$stmt->execute(array($noticia));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}
}
?>
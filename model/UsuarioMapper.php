<?php
// file: model/UserMapper.php
require_once(__DIR__."/../core/PDOConnection.php");
 
class UsuarioMapper {
	/**
	* Reference to the PDO connection
	* @var PDO
	*/
	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}
	/**
	* Saves a comment
	*
	* @param Comment $comment The comment to save
	* @throws PDOException if a database error occurs
	* @return int The new comment id
	*/
	public function save(Usuario $usuario) {
		$stmt = $this->db->prepare("INSERT INTO usuarios(nombre, rol, contrasena, email) values (?,?,?,?)");
		$stmt->execute(array($usuario->getNombre(), $usuario->getRol(), $usuario->getContrasena(), $usuario->getEmail()));
		return $this->db->lastInsertId();
	}

	public function delete(Usuario $usuario){
        $sql = $this->db->prepare("DELETE FROM usuarios where id=?");
        $sql->execute(array($usuario->getId()));
	}

	public function update(Usuario $usuario){
		$sql = $this->db->prepare("UPDATE usuarios SET nombre=?, rol=?, contrasena=?, email=? where id=?");
		$sql->execute(array($usuario->getNombre(), $usuario->getRol(), $usuario->getContrasena(), $usuario->getEmail(), $usuario->getId()));
}
	
	public function findAll(){
		$stmt = $this->db->query("SELECT * FROM usuarios");
		$usuario_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$usuarios = array();

		foreach ($usuario_db as $usuario) {
			array_push($usuarios, new Usuario($usuario["id"], $usuario["nombre"], $usuario["rol"], $usuario["contrasena"], $usuario["email"]));
		}
		return $usuarios;
	}

	public function findById($id){
		$stmt = $this->db->prepare("SELECT * FROM usuarios WHERE id=?");
		$stmt->execute(array($id));
		$usuario = $stmt->fetch(PDO::FETCH_ASSOC);
		if($usuario != null) {
			return new Usuario(
			$usuario["id"],
			$usuario["nombre"],
			$usuario["rol"],
			$usuario["contrasena"],
			$usuario["email"]);
		} else {
			return NULL;
		}
	}

	public function findByUsername($username){
        $sql = $this->db->prepare("SELECT * FROM usuarios WHERE nombre=?");
        $sql->execute(array($username));
        $user = $sql->fetch(PDO::FETCH_ASSOC);

        if($user != NULL) {
            return new Usuario($user["id"], $user["nombre"], $user["rol"], NULL, $user["email"]);
        } else {
            return NULL;
        }
    }
	

	public function isValidUser($username, $password) {
		$stmt = $this->db->prepare("SELECT count(nombre) FROM usuarios where nombre=? and contrasena=?");
		$stmt->execute(array($username, $password));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function usernameExists($username) {
		$stmt = $this->db->prepare("SELECT count(nombre) FROM usuarios where nombre=?");
		$stmt->execute(array($username));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	
}
?>
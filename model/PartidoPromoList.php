<?php

require_once(__DIR__."/../core/ValidationException.php");

class PartidoPromoList{
  private $id;
  private $pista;
  private $fecha;
  private $hora_ini;
  private $hora_fin;
  private $usuario1;
  private $usuario2;
  private $usuario3;
  private $usuario4;

  public function __construct($id=NULL, $pista=NULL, $fecha=NULL, $hora_ini=NULL, $hora_fin=NULL, $usuario1=NULL, $usuario2=NULL, $usuario3=NULL, $usuario4=NULL) {
		$this->id = $id;
		$this->pista = $pista;
		$this->fecha = $fecha;
		$this->hora_ini = $hora_ini;
		$this->hora_fin = $hora_fin;
		$this->usuario1 = $usuario1;
		$this->usuario2 = $usuario2;
		$this->usuario3 = $usuario3;
		$this->usuario4 = $usuario4;
	}

	public function getId(){
		return $this->id;
	}

	public function getPista(){
		return $this->pista;
	}

	public function getFecha() {
		return $this->fecha;
	}

	public function getHora_ini() {
		return $this->hora_ini;
	}

	public function getHora_fin() {
		return $this->hora_fin;
	}

	public function getUsuario1() {
		return $this->usuario1;
	}

	public function getUsuario2() {
		return $this->usuario2;
	}

	public function getUsuario3() {
		return $this->usuario3;
	}

	public function getUsuario4() {
		return $this->usuario4;
	}

	public function setId($id){
		$this->id = $id;
	}

  	public function setPista($pista){
		$this->pista = $pista;
	}

	public function setFecha($fecha) {
		$this->fecha = $fecha;
	}

	public function setHora_ini($hora_ini) {
		$this->hora_ini = $hora_ini;
	}

	public function setHora_fin($hora_fin) {
		$this->hora_fin = $hora_fin;
	}

	public function setUsuario1($usuario1) {
		$this->usuario1 = $usuario1;
	}
	
	public function setUsuario2($usuario2) {
		$this->usuario2 = $usuario2;
	}

	public function setUsuario3($usuario3) {
		$this->usuario3 = $usuario3;
	}

	public function setUsuario4($usuario4) {
		$this->usuario4 = $usuario4;
	}

  	public function checkIsValid() {
    $errors = array();
  }
}
 ?>
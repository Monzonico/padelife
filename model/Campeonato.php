<?php

require_once(__DIR__."/../core/ValidationException.php");

class Campeonato {
	
	private $id;
	private $nombre;
	private $normativa;
	private $fecha_ini;
	private $fecha_fin;
	private $fecha_ini_inscri;
	private $fecha_fin_inscri;


	public function __construct($id=NULL, $nombre=NULL, $normativa=NULL, $fecha_ini=NULL, $fecha_fin=NULL, $fecha_ini_inscri=NULL, $fecha_fin_inscri=NULL) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->normativa = $normativa;
		$this->fecha_ini = $fecha_ini;
		$this->fecha_fin = $fecha_fin;
		$this->fecha_ini_inscri = $fecha_ini_inscri;
		$this->fecha_fin_inscri = $fecha_fin_inscri;
	}
	 
	public function getId(){
		return $this->id;
	} 

	public function getNombre() {
		return $this->nombre;
	}

	public function getNormativa(){
		return $this->normativa;
	}

	public function getFecha_ini(){
		return $this->fecha_ini;
	} 

	public function getFecha_fin(){
		return $this->fecha_fin;
	} 

	public function getFecha_ini_inscri(){
		return $this->fecha_ini_inscri;
	} 

	public function getFecha_fin_inscri(){
		return $this->fecha_fin_inscri;
	}

	public function setId($id){
		$this->id = $id;
	} 

	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	public function setNormativa($normativa){
		$this->normativa = $normativa;
	} 

	public function setFecha_ini($fecha_ini) {
		$this->fecha_ini = $fecha_ini;
	}

	public function setFecha_fin($fecha_fin){
		$this->fecha_fin = $fecha_fin;
	}

	public function setFecha_ini_inscri($fecha_ini_inscri){
		$this->fecha_ini_inscri = $fecha_ini_inscri;
	} 

	public function setFecha_fin_inscri($fecha_fin_inscri){
		$this->fecha_fin_inscri = $fecha_fin_inscri;
	}

	public function checkIsValid() {

	}
}
?>
<?php

require_once(__DIR__."/../core/ValidationException.php");

class Categoria{
  private $id;
  private $id_campeonato;
  private $nombre;
  private $nivel;


  public function __construct($id=NULL, $id_campeonato=NULL, $nombre=NULL, $nivel=NULL) {
        $this->id = $id;
        $this->id_campeonato = $id_campeonato;
        $this->nombre = $nombre;
        $this->nivel = $nivel;
      }

	public function getId(){
		return $this->id;
    }

    public function getId_Campeonato(){
		return $this->id_campeonato;
    }

    public function getNombre(){
		return $this->nombre;
  }
  
  public function getNivel(){
		return $this->nivel;
	}

    public function setId($id){
		$this->id = $id;
    }

    public function setId_Campeonato($id_campeonato){
		$this->id_campeonato = $id_campeonato;
    }
    
    public function setNombre($nombre){
		$this->nombre = $nombre;
    }

    public function setNivel($nivel){
      $this->nivel = $nivel;
      }

  	public function checkIsValid() {
    $errors = array();
  }
}
 ?>

<?php

require_once(__DIR__."/../core/ValidationException.php");

class Ganador {
	
	private $id;
	private $id_categoria;
	private $id_pareja;
	public $repeticiones;


	public function __construct($id=NULL, $id_categoria=NULL, $id_pareja=NULL, $repeticiones=NULL) {
		$this->id = $id;
		$this->id_categoria = $id_categoria;
		$this->id_pareja = $id_pareja;
		$this->repeticiones = $repeticiones;

	}
	 
	public function getId(){
		return $this->id;
	}

	public function getId_Categoria(){
		return $this->id_categoria;
	}

	public function getId_Pareja() {
		return $this->id_pareja;
	}

	public function getRepeticiones() {
		return $this->repeticiones;
	}

	public function setId($id){
		$this->id = $id;
	} 

	public function set_IdCategoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	public function set_IdPareja($id_pareja){
		$this->id_pareja = $id_pareja;
	}

	public function setRepeticiones($repeticiones){
		$this->repeticiones = $repeticiones;
	} 

	public function checkIsValid() {

	}
}
?>
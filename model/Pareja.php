<?php

require_once(__DIR__."/../core/ValidationException.php");

class Pareja {
	
	private $id;
	private $id_categoria;
	private $id_capitan;
	private $id_jugador;

	public function __construct($id=NULL, $id_categoria=NULL, $id_capitan=NULL, $id_jugador=NULL) {
		$this->id = $id;
		$this->id_categoria = $id_categoria;
		$this->id_capitan = $id_capitan;
		$this->id_jugador = $id_jugador;

	}
	 
	public function getId(){
		return $this->id;
	}

	public function getId_Categoria(){
		return $this->id_categoria;
	}

	public function getId_Capitan() {
		return $this->id_capitan;
	}

	public function getId_Jugador() {
		return $this->id_jugador;
	}

	public function setId($id){
		$this->id = $id;
	} 

	public function set_IdCategoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	public function set_IdCapitan($id_capitan){
		$this->id_capitan = $id_capitan;
	} 

	public function set_IdJugador($id_jugador) {
		$this->id_jugador = $id_jugador;
	}

	public function checkIsValid() {

	}
}
?>
<?php

require_once(__DIR__."/../core/ValidationException.php");

class Clase{
  private $id;
  private $reserva;
  private $entrenador;
  private $pista;
  private $hora;
  private $fecha;
  private $nombre;
  private $descripcion;
  private $capacidad;

  public function __construct($id=NULL, $reserva=NULL, $entrenador=NULL, $pista=NULL, $hora=NULL, $fecha=NULL, $nombre=NULL, $descripcion=NULL, $capacidad=NULL) {
		$this->id = $id;
		$this->reserva = $reserva;
        $this->entrenador = $entrenador;
		$this->pista = $pista;
		$this->hora = $hora;
        $this->fecha = $fecha;
		$this->nombre = $nombre;
		$this->descripcion = $descripcion;
		$this->capacidad = $capacidad;
	}

	public function getId(){
		return $this->id;
	}
	
	public function getReserva(){
		return $this->reserva;
    }

    public function getEntrenador(){
		return $this->entrenador;
    }

	public function getPista() {
		return $this->pista;
	}

	public function getHora() {
		return $this->hora;
	}

    public function getFecha(){
		return $this->fecha;
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function getCapacidad(){
		return $this->capacidad;
	}

    public function setId($id){
		$this->id = $id;
	}
	
	public function setReserva($reserva){
		$this->reserva = $reserva;
    }

	public function setEntrenador($entrenador) {
		$this->entrenador = $entrenador;
	}

    public function setPista($pista){
		$this->pista = $pista;
    }
	
	public function setHora($hora) {
		$this->hora = $hora;
	}

    public function setFecha($fecha){
		$this->fecha = $fecha;
	}
	
	public function setNombre($nombre){
		$this->nombre = $nombre;
	}
	
	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}
	
	public function setCapacidad($capacidad){
		$this->capacidad = $capacidad;
    }

  	public function checkIsValid() {
    $errors = array();
  }

}
 ?>

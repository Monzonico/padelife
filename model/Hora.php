<?php

require_once(__DIR__."/../core/ValidationException.php");

class Hora{
  private $id;
  private $hora_ini;
  private $hora_fin;

  public function __construct($id=NULL, $hora_ini=NULL, $hora_fin=NULL) {
		$this->id = $id;
		$this->hora_ini = $hora_ini;
		$this->hora_fin = $hora_fin;
	}

	public function getId(){
		return $this->id;
	}

	public function getHora_ini() {
		return $this->hora_ini;
	}

	public function getHora_fin() {
		return $this->hora_fin;
	}

  	public function setId($id){
		$this->id = $id;
	}

	public function setHora_ini($hora_ini) {
		$this->hora_ini = $hora_ini;
	}

	public function setHora_fin($hora_fin) {
		$this->hora_fin = $hora_fin;
	}

  	public function checkIsValid() {
    $errors = array();
  }

}
 ?>

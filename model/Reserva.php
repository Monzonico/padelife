<?php

require_once(__DIR__."/../core/ValidationException.php");

class Reserva{
  private $id;
  private $id_pista;
  private $id_usuario;
  private $id_hora;
  private $fecha;

  public function __construct($id=NULL, $id_pista=NULL, $id_usuario=NULL, $id_hora=NULL, $fecha=NULL) {
		$this->id = $id;
		$this->id_pista = $id_pista;
		$this->id_usuario = $id_usuario;
		$this->id_hora = $id_hora;
		$this->fecha = $fecha;
	}

	public function getId(){
		return $this->id;
	}

	public function getId_pista() {
		return $this->id_pista;
	}

	public function getId_usuario() {
		return $this->id_usuario;
	}

	public function getId_hora() {
		return $this->id_hora;
	}

	public function getFecha() {
		return $this->fecha;
	}

  	public function setId($id){
		$this->id = $id;
	}

	public function setId_pista($id_pista) {
		$this->id_pista = $id_pista;
	}

	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
	}

	public function setId_hora($id_hora) {
		$this->id_hora = $id_hora;
	}

	public function setFecha($fecha) {
		$this->fecha = $fecha;
	}

  	public function checkIsValid() {
    $errors = array();
  }

}
 ?>

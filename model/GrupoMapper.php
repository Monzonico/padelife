<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class GrupoMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Grupo $grupo) {
		$stmt = $this->db->prepare("INSERT INTO grupos(id_categoria, nombre) values (?,?)");
		$stmt->execute(array($grupo->getId_Categoria(), $grupo->getNombre()));
		return $this->db->lastInsertId();
	}

	public function delete(Grupo $grupo){
        $sql = $this->db->prepare("DELETE FROM grupos where id=?");
        $sql->execute(array($grupo->getId()));
	}

	


}

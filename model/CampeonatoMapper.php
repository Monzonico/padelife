<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class CampeonatoMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Campeonato $campeonato) {
		$stmt = $this->db->prepare("INSERT INTO campeonatos(nombre, normativa, fecha_ini, fecha_fin, fecha_ini_inscri, fecha_fin_inscri) values (?,?,?,?,?,?)");
		$stmt->execute(array($campeonato->getNombre(), $campeonato->getNormativa(), $campeonato->getFecha_ini(), $campeonato->getFecha_fin(), $campeonato->getFecha_ini_inscri(), $campeonato->getFecha_fin_inscri()));
		return $this->db->lastInsertId();
	}

	public function delete(Campeonato $campeonato){
        $sql = $this->db->prepare("DELETE FROM campeonatos where id=?");
        $sql->execute(array($campeonato->getId()));
	}

	public function update(Campeonato $campeonato){
		$sql = $this->db->prepare("UPDATE campeonatos SET nombre=?, normativa=?, fecha_ini=?, fecha_fin=?, fecha_ini_inscri=?, fecha_fin_inscri=? where id=?");
		$sql->execute(array($campeonato->getNombre(), $campeonato->getNormativa(), $campeonato->getFecha_ini(), $campeonato->getFecha_fin(), $campeonato->getFecha_ini_inscri(), $campeonato->getFecha_fin_inscri(), $campeonato->getId()));
	}

	public function findAll(){
		$stmt = $this->db->query("SELECT * FROM campeonatos");
		$campeonato_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$campeonatos = array();

		foreach ($campeonato_db as $campeonato) {
			array_push($campeonatos, new Campeonato($campeonato["id"], $campeonato["nombre"], $campeonato["normativa"], $campeonato["fecha_ini"], $campeonato["fecha_fin"], $campeonato["fecha_ini_inscri"], $campeonato["fecha_fin_inscri"]));
		}
		return $campeonatos;
	}

	public function findById($id){
		$stmt = $this->db->prepare("SELECT * FROM campeonatos WHERE id=?");
		$stmt->execute(array($id));
		$campeonato = $stmt->fetch(PDO::FETCH_ASSOC);
		if($campeonato != null) {
			return new Campeonato(
			$campeonato["id"],
			$campeonato["nombre"],
			$campeonato["normativa"],
			$campeonato["fecha_ini"],
			$campeonato["fecha_fin"],
			$campeonato["fecha_ini_inscri"],
			$campeonato["fecha_fin_inscri"]);
		} else {
			return NULL;
		}
	}

}
?>
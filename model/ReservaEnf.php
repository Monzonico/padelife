<?php

require_once(__DIR__."/../core/ValidationException.php");

class ReservaEnf{
  private $id;
  private $id_enfrentamiento;

  public function __construct($id=NULL, $id_enfrentamiento=NULL) {
		$this->id = $id;
		$this->id_enfrentamiento = $id_enfrentamiento;
	}

	public function getId(){
		return $this->id;
	}

	public function getId_enfrentamiento() {
		return $this->id_enfrentamiento;
	}

  	public function setId($id){
		$this->id = $id;
	}

	public function setId_enfrentamiento($id_enfrentamiento) {
		$this->id_enfrentamiento = $id_enfrentamiento;
	}

  	public function checkIsValid() {
    $errors = array();
  }

}
 ?>

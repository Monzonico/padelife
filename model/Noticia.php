<?php

require_once(__DIR__."/../core/ValidationException.php");

class Noticia {
	
	private $id;
	private $hora;
	private $titulo;
	private $texto;


	public function __construct($id=NULL, $hora=NULL, $titulo=NULL, $texto=NULL) {
		$this->id = $id;
		$this->hora = $hora;
		$this->titulo = $titulo;
		$this->texto = $texto;
	}
	 
	public function getId(){
		return $this->id;
	}

	public function getHora(){
		return $this->hora;
	} 

	public function getTitulo() {
		return $this->titulo;
	}

	public function getTexto(){
		return $this->texto;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function setHora($hora){
		$this->hora = $hora;
	} 

	public function setTitulo($titulo) {
		$this->titulo = $titulo;
	}

	public function setTexto($texto){
		$this->texto = $texto;
	}

	public function checkIsValid() {
		$errors = array();

		if (strlen($this->titulo) < 3) {
			$errors["titulo"] = "debe tener una longitud minima de 3";
		}

		if (strlen($this->texto) < 5) {
			$errors["texto"] = "debe tener una longitud minima de 5";
		}

		if (sizeof($errors)>0){
			throw new ValidationException($errors, "Noticia no valida");
		}
	}
}
?>
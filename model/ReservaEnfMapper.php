<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class ReservaEnfMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(ReservaEnf $reservaenf) {
		$stmt = $this->db->prepare("INSERT INTO prereservas_enf(id_enfrentamiento) values (?)");
		$stmt->execute(array($reservaenf->getId_enfrentamiento()));
		return $this->db->lastInsertId();
	}

	public function delete(ReservaEnf $reservaenf){
        $sql = $this->db->prepare("DELETE FROM prereservas_enf where id=?");
        $sql->execute(array($reservaenf->getId()));
	}

	public function update(ReservaEnf $reservaenf){
		$sql = $this->db->prepare("UPDATE prereservas_enf SET id_enfrentamiento=? where id=?");
		$sql->execute(array($reservaenf->getId_enfrentamiento(), $reservaenf->getId()));
    }
    
	public function findAll(){
		$stmt = $this->db->query("SELECT * FROM prereservas_enf");
		$reservasenf_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$reservasenf = array();

		foreach ($reservasenf_db as $reservaenf) {
			array_push($reservasenf, new ReservaEnf($reservaenf["id"],$reservaenf["id_enfrentamiento"]));
		}
		return $reservasenf;
    }
    
    public function enfExists($id_enf) {
		$stmt = $this->db->prepare("SELECT count(*) FROM prereservas_enf where id_enfrentamiento=?");
		$stmt->execute(array($id_enf));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
    }
    
    public function findByIdCampTipo($id_camp,$tipo){
		$stmt = $this->db->prepare("SELECT prereservas_enf.id,prereservas_enf.id_enfrentamiento,enfrentamientos.id_pista,parejas.id_capitan,enfrentamientos.id_hora,enfrentamientos.fecha FROM prereservas_enf INNER JOIN enfrentamientos ON prereservas_enf.id_enfrentamiento = enfrentamientos.id INNER JOIN categorias ON enfrentamientos.id_categoria = categorias.id INNER JOIN campeonatos ON categorias.id_campeonato = campeonatos.id INNER JOIN parejas ON enfrentamientos.id_pareja1 = parejas.id WHERE campeonatos.id=? AND enfrentamientos.tipo=?");
		$stmt->execute(array($id_camp,$tipo));
		$reservaenf_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$reservaenfs = array();

		foreach ($reservaenf_db as $reservaenf) {
			array_push($reservaenfs, new ReservaEnfList($reservaenf["id"], $reservaenf["id_enfrentamiento"], $reservaenf["id_pista"], $reservaenf["id_capitan"], $reservaenf["id_hora"], $reservaenf["fecha"]));
		}
		return $reservaenfs;
	}

}
?>
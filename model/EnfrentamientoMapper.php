<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class EnfrentamientoMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Enfrentamiento $enfrentamiento) {
		$stmt = $this->db->prepare("INSERT INTO enfrentamientos(id_categoria, id_pareja1, id_pareja2, id_pista, id_hora, set1, set2, set3, resultado, tipo, fecha) values (?,?,?,?,?,?,?,?,?,?,?)");
		$stmt->execute(array($enfrentamiento->getId_Categoria(), $enfrentamiento->getId_Pareja1(), $enfrentamiento->getId_Pareja2(), $enfrentamiento->getId_Pista(),$enfrentamiento->getId_Hora(), $enfrentamiento->getSet_1(), $enfrentamiento->getSet_2(), $enfrentamiento->getSet_3(), $enfrentamiento->getResultado(), $enfrentamiento->getTipo(), $enfrentamiento->getFecha()));
		return $this->db->lastInsertId();
	}
	
	public function update_Hora_Enf($enf_id,$id_hora){
		$sql = $this->db->prepare("UPDATE enfrentamientos SET id_hora=? where id=?");
		$sql->execute(array($id_hora,$enf_id));
	}

	public function update_Pista_Enf($enf_id,$id_pista){
		$sql = $this->db->prepare("UPDATE enfrentamientos SET id_pista=? where id=?");
		$sql->execute(array($id_pista,$enf_id));
	}

	public function update_Pista_Day($enf_id,$fecha){
		$sql = $this->db->prepare("UPDATE enfrentamientos SET fecha=? where id=?");
		$sql->execute(array($fecha,$enf_id));
	}

	public function update_Result_Enf($id,$set1,$set2,$set3,$result){
		$sql = $this->db->prepare("UPDATE enfrentamientos SET set1=?, set2=?, set3=?, resultado=? where id=?");
		$sql->execute(array($set1,$set2,$set3,$result,$id));
	}
    
	public function enfExists($id_categoria,$id_pareja1,$id_pareja2) {
		$stmt = $this->db->prepare("SELECT count(*) FROM enfrentamientos where id_categoria=? and id_pareja1=? and id_pareja2=?");
		$stmt->execute(array($id_categoria,$id_pareja1,$id_pareja2));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function enfExistsCamp($id_categoria,$id_pareja1,$id_pareja2) {
		$stmt = $this->db->prepare("SELECT count(*) FROM enfrentamientos where id_categoria=? and id_pareja1=? and id_pareja2=? and tipo='Campeonato'");
		$stmt->execute(array($id_categoria,$id_pareja1,$id_pareja2));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function validExistEnf($id_categoria) {
		$stmt = $this->db->prepare("SELECT count(*) FROM enfrentamientos where id_categoria=?");
		$stmt->execute(array($id_categoria));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function validExistEnfTipo($id_categoria,$tipo) {
		$stmt = $this->db->prepare("SELECT count(*) FROM enfrentamientos where id_categoria=? and tipo=?");
		$stmt->execute(array($id_categoria,$tipo));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function findByIdCat($id_categoria){
		$stmt = $this->db->prepare("SELECT * FROM enfrentamientos WHERE id_categoria=?");
		$stmt->execute(array($id_categoria));
		$enfrentamiento_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$enfrentamientos = array();

		foreach ($enfrentamiento_db as $enfrentamiento) {
			array_push($enfrentamientos, new Enfrentamiento($enfrentamiento["id"], $enfrentamiento["id_categoria"], $enfrentamiento["id_pareja1"], $enfrentamiento["id_pareja2"], $enfrentamiento["id_pista"], $enfrentamiento["id_hora"], $enfrentamiento["set1"], $enfrentamiento["set2"], $enfrentamiento["set3"], $enfrentamiento["resultado"], $enfrentamiento["tipo"], $enfrentamiento["fecha"]));
		}
		return $enfrentamientos;
	}

	public function findByIdCatTipo($id_categoria,$tipo){
		$stmt = $this->db->prepare("SELECT * FROM enfrentamientos WHERE id_categoria=? and tipo=?");
		$stmt->execute(array($id_categoria,$tipo));
		$enfrentamiento_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$enfrentamientos = array();

		foreach ($enfrentamiento_db as $enfrentamiento) {
			array_push($enfrentamientos, new Enfrentamiento($enfrentamiento["id"], $enfrentamiento["id_categoria"], $enfrentamiento["id_pareja1"], $enfrentamiento["id_pareja2"], $enfrentamiento["id_pista"], $enfrentamiento["id_hora"], $enfrentamiento["set1"], $enfrentamiento["set2"], $enfrentamiento["set3"], $enfrentamiento["resultado"], $enfrentamiento["tipo"], $enfrentamiento["fecha"]));
		}
		return $enfrentamientos;
	}

	public function findByIdCampTipo($id,$tipo){
		$stmt = $this->db->prepare("SELECT enfrentamientos.id, enfrentamientos.id_categoria, enfrentamientos.id_pareja1,enfrentamientos.id_pareja2,enfrentamientos.id_pista,enfrentamientos.id_hora,enfrentamientos.set1,enfrentamientos.set2,enfrentamientos.set3,enfrentamientos.resultado,enfrentamientos.tipo,enfrentamientos.fecha FROM enfrentamientos LEFT JOIN categorias ON enfrentamientos.id_categoria = categorias.id LEFT JOIN campeonatos ON categorias.id_campeonato = campeonatos.id WHERE campeonatos.id=? AND enfrentamientos.tipo=?");
		$stmt->execute(array($id,$tipo));
		$enfrentamiento_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$enfrentamientos = array();

		foreach ($enfrentamiento_db as $enfrentamiento) {
			array_push($enfrentamientos, new Enfrentamiento($enfrentamiento["id"], $enfrentamiento["id_categoria"], $enfrentamiento["id_pareja1"], $enfrentamiento["id_pareja2"], $enfrentamiento["id_pista"], $enfrentamiento["id_hora"], $enfrentamiento["set1"], $enfrentamiento["set2"], $enfrentamiento["set3"], $enfrentamiento["resultado"], $enfrentamiento["tipo"], $enfrentamiento["fecha"]));
		}
		return $enfrentamientos;
	}

	public function findLastEnfTipo($id,$tipo){
		$stmt = $this->db->prepare("SELECT enfrentamientos.id, enfrentamientos.id_categoria, enfrentamientos.id_pareja1,enfrentamientos.id_pareja2,enfrentamientos.id_pista,enfrentamientos.id_hora,enfrentamientos.set1,enfrentamientos.set2,enfrentamientos.set3,enfrentamientos.resultado,enfrentamientos.tipo,enfrentamientos.fecha FROM enfrentamientos LEFT JOIN categorias ON enfrentamientos.id_categoria = categorias.id LEFT JOIN campeonatos ON categorias.id_campeonato = campeonatos.id WHERE campeonatos.id=? AND enfrentamientos.tipo=? order by enfrentamientos.id desc limit 1 ");
		$stmt->execute(array($id,$tipo));
		$campeonato = $stmt->fetch(PDO::FETCH_ASSOC);
		if($campeonato != null) {
			return new Enfrentamiento(
			$campeonato["id"],
			$campeonato["id_categoria"],
			$campeonato["id_pareja1"],
			$campeonato["id_pareja2"],
			$campeonato["id_pista"],
			$campeonato["id_hora"],
			$campeonato["set1"],
			$campeonato["set2"],
			$campeonato["set3"],
			$campeonato["resultado"],
			$campeonato["tipo"],
			$campeonato["fecha"]);
		} else {
			return NULL;
		}
	}

}
?>
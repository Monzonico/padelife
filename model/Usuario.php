<?php

require_once(__DIR__."/../core/ValidationException.php");

class Usuario {
	
	private $id;
	private $nombre;
	private $rol;
	private $contrasena;
	private $email;


	public function __construct($id=NULL, $nombre=NULL, $rol=NULL, $contrasena=NULL, $email=NULL) {
		$this->id = $id;
		$this->nombre = $nombre;
		$this->rol = $rol;
		$this->contrasena = $contrasena;
		$this->email = $email;
	}
	 
	public function getId(){
		return $this->id;
	} 

	public function getNombre() {
		return $this->nombre;
	}

	public function getRol(){
		return $this->rol;
	}

	public function getContrasena(){
		return $this->contrasena;
	} 

	public function getEmail(){
		return $this->email;
	} 

	public function setId($id){
		$this->id = $id;
	} 

	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	public function setRol($rol){
		$this->rol = $rol;
	} 

	public function setContrasena($contrasena) {
		$this->contrasena = $contrasena;
	}

	public function setEmail($email){
		$this->email = $email;
	} 

	public function checkIsValidForRegister() {
		$errors = array();

		if (strlen($this->nombre) < 5) {
			$errors["nombre"] = "debe tener una longitud de 5";
		}

		if (strlen($this->contrasena) < 5) {
			$errors["contrasena"] = "debe tener una longitud de 5";
		}

		if (sizeof($errors)>0){
			throw new ValidationException($errors, "usuario no valido");
		}
	}
}
?>
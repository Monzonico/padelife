<?php

require_once(__DIR__."/../core/ValidationException.php");

class Asistente {
	
	private $id;
	private $id_clase;
	private $id_usuario;


	public function __construct($id=NULL, $id_clase=NULL, $id_usuario=NULL) {
		$this->id = $id;
		$this->id_clase = $id_clase;
		$this->id_usuario = $id_usuario;

	}
	 
	public function getId(){
		return $this->id;
	} 

	public function getId_Clase() {
		return $this->id_clase;
	}

	public function getId_Usuario(){
		return $this->id_usuario;
	}

	public function setId($id){
		$this->id = $id;
	} 

	public function setId_Clase($id_clase) {
		$this->id_clase = $id_clase;
	}

	public function setId_Usuario($id_usuario){
		$this->id_usuario = $id_usuario;
	} 

	public function checkIsValid() {

	}
}
?>
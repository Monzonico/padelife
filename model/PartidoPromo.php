<?php

require_once(__DIR__."/../core/ValidationException.php");

class PartidoPromo{
  private $id;
  private $id_reserva;
  private $id_pista;
  private $id_hora;
  private $id_usuario1;
  private $id_usuario2;
  private $id_usuario3;
  private $id_usuario4;
  private $fecha;

  public function __construct($id=NULL, $id_reserva=NULL, $id_pista=NULL, $id_hora=NULL, $id_usuario1=NULL, $id_usuario2=NULL, $id_usuario3=NULL, $id_usuario4=NULL, $fecha=NULL) {
		$this->id = $id;
		$this->id_reserva = $id_reserva;
		$this->id_pista = $id_pista;
		$this->id_hora = $id_hora;
		$this->id_usuario1 = $id_usuario1;
		$this->id_usuario2 = $id_usuario2;
		$this->id_usuario3 = $id_usuario3;
		$this->id_usuario4 = $id_usuario4;
		$this->fecha = $fecha;
	}

	public function getId(){
		return $this->id;
	}

	public function getId_reserva(){
		return $this->id_reserva;
	}

	public function getId_pista(){
		return $this->id_pista;
	}

	public function getId_hora() {
		return $this->id_hora;
	}

	public function getId_usuario1() {
		return $this->id_usuario1;
	}

	public function getId_usuario2() {
		return $this->id_usuario2;
	}

	public function getId_usuario3() {
		return $this->id_usuario3;
	}

	public function getId_usuario4() {
		return $this->id_usuario4;
	}

	public function getFecha() {
		return $this->fecha;
	}

	public function setId($id){
		$this->id = $id;
	}
	
	public function setId_reserva($id_reserva){
		$this->id_reserva = $id_reserva;
	}

  	public function setId_pista($id_pista){
		$this->id_pista = $id_pista;
	}

	public function setId_hora($id_hora) {
		$this->id_hora = $id_hora;
	}

	public function setId_usuario1($id_usuario1) {
		$this->id_usuario1 = $id_usuario1;
	}
	
	public function setId_usuario2($id_usuario2) {
		$this->id_usuario2 = $id_usuario2;
	}

	public function setId_usuario3($id_usuario3) {
		$this->id_usuario3 = $id_usuario3;
	}

	public function setId_usuario4($id_usuario4) {
		$this->id_usuario4 = $id_usuario4;
	}

	public function setFecha($fecha) {
		$this->fecha = $fecha;
	}

  	public function checkIsValid() {
    $errors = array();
  }
}
 ?>
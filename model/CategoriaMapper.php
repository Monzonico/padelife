<?php

require_once(__DIR__."/../core/PDOConnection.php");
 
class CategoriaMapper {

	private $db;
	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function save(Categoria $categoria) {
		$stmt = $this->db->prepare("INSERT INTO categorias(id_campeonato, nombre, nivel) values (?,?,?)");
		$stmt->execute(array($categoria->getId_Campeonato(), $categoria->getNombre(), $categoria->getNivel()));
		return $this->db->lastInsertId();
	}

	public function delete(Categoria $categoria){
        $sql = $this->db->prepare("DELETE FROM categorias where id=?");
        $sql->execute(array($categoria->getId()));
	}

	public function update(Categoria $categoria){
		$sql = $this->db->prepare("UPDATE categorias SET nombre=? where id=?");
		$sql->execute(array($categoria->getNombre(), $categoria->getId()));
	}

	public function findAllByIdCampeonato($campeonatoID){
		$stmt = $this->db->prepare("SELECT * FROM categorias WHERE id_campeonato=?");
		$stmt->execute(array($campeonatoID));
		$categoria_db = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$categorias = array();

		foreach ($categoria_db as $categoria) {
			array_push($categorias, new Categoria($categoria["id"], $categoria["id_campeonato"], $categoria["nombre"], $categoria["nivel"]));
		}
		return $categorias;
	}

	public function categoriaExists($id_campeonato,$nombre,$nivel) {
		$stmt = $this->db->prepare("SELECT count(*) FROM categorias where nombre=? AND nivel=? AND id_campeonato=?");
		$stmt->execute(array($nombre,$nivel,$id_campeonato));
		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function findById($idcategoria){
		$stmt = $this->db->prepare("SELECT * FROM categorias WHERE id=?");
		$stmt->execute(array($idcategoria));
		$categoria = $stmt->fetch(PDO::FETCH_ASSOC);
		if($categoria != null) {
			return new Categoria(
			$categoria["id"],
			$categoria["id_campeonato"],
			$categoria["nombre"],
			$categoria["nivel"]);
		} else {
			return NULL;
		}
	}

}
?>
<?php

require_once(__DIR__."/../core/ValidationException.php");

class Inscrito {

	private $id_categoria;
	private $nombre;
	private $nivel;
	private $capitan;
	private $pareja;

	public function __construct($id_categoria=NULL,$nombre=NULL, $nivel=NULL, $capitan=NULL, $pareja=NULL) {
		$this->id_categoria = $id_categoria;
		$this->nombre = $nombre;
		$this->nivel = $nivel;
		$this->capitan = $capitan;
		$this->pareja = $pareja;

	}

	public function getIdCategoria(){
		return $this->id_categoria;
	}
	 
	public function getNombre(){
		return $this->nombre;
	}

	public function getNivel(){
		return $this->nivel;
	}
	
	public function getCapitan(){
		return $this->capitan;
	}
    
    public function getPareja(){
		return $this->pareja;
	}

	public function setIdCategoria($id_categoria){
		$this->id_categoria = $id_categoria;
	} 

	public function setId($nombre){
		$this->nombre = $nombre;
	} 

	public function setNivel($nivel) {
		$this->nivel = $nivel;
	}
	
	public function setCapitan($capitan) {
		$this->capitan = $capitan;
    }
    
    public function setPareja($pareja) {
		$this->pareja = $pareja;
	}

	public function checkIsValid() {

	}
}
?>
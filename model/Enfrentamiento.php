<?php

require_once(__DIR__."/../core/ValidationException.php");

class Enfrentamiento {
	
	private $id;
	private $id_categoria;
	private $id_pareja1;
	private $id_pareja2;
	private $id_pista;
	private $id_hora;
    private $set_1;
    private $set_2;
    private $set_3;
    private $resultado;
	private $tipo;
	private $fecha;


	public function __construct($id=NULL, $id_categoria=NULL, $id_pareja1=NULL, $id_pareja2=NULL, $id_pista=NULL, $id_hora=NULL, $set_1=NULL, $set_2=NULL, $set_3=NULL, $resultado=NULL, $tipo=NULL, $fecha=NULL) {
		$this->id = $id;
		$this->id_categoria = $id_categoria;
		$this->id_pareja1 = $id_pareja1;
		$this->id_pareja2 = $id_pareja2;
		$this->id_pista = $id_pista;
		$this->id_hora = $id_hora;
        $this->set_1 = $set_1;
        $this->set_2 = $set_2;
        $this->set_3 = $set_3;
        $this->resultado = $resultado;
		$this->tipo = $tipo;
		$this->fecha = $fecha;
	}
	 
	public function getId(){
		return $this->id;
	}

	public function getId_Categoria(){
		return $this->id_categoria;
	}

	public function getId_Pareja1() {
		return $this->id_pareja1;
	}

	public function getId_Pareja2() {
		return $this->id_pareja2;
	}
	
	public function getId_Pista() {
		return $this->id_pista;
	}

	public function getId_Hora() {
		return $this->id_hora;
	}

    public function getSet_1() {
		return $this->set_1;
    }
    
    public function getSet_2() {
		return $this->set_2;
    }
    
    public function getSet_3() {
		return $this->set_3;
    }
    
    public function getResultado() {
		return $this->resultado;
    }
    
    public function getTipo() {
		return $this->tipo;
	}

	public function getFecha() {
		return $this->fecha;
	}

	public function setId($id){
		$this->id = $id;
	} 

	public function set_IdCategoria($id_categoria) {
		$this->id_categoria = $id_categoria;
	}

	public function set_IdPareja1($id_pareja1){
		$this->id_pareja1 = $id_pareja1;
	} 

	public function set_IdPareja2($id_pareja2) {
		$this->id_pareja2 = $id_pareja2;
	}
	
	public function set_IdPista($id_pista) {
		$this->id_pista = $id_pista;
	}
	
	public function setId_Hora($id_hora) {
		$this->id_hora = $id_hora;
	}
    
    public function set_Set1($set_1) {
		$this->set_1 = $set_1;
    }
    
    public function set_Set2($set_2) {
		$this->set_2 = $set_2;
	}

    public function set_Set3($set_3) {
		$this->set_3 = $set_3;
	}

    public function set_Resultado($resultado) {
		$this->resultado = $resultado;
	}

    public function set_Tipo($tipo) {
		$this->tipo = $tipo;
	}

	public function set_Fecha($fecha) {
		$this->fecha = $fecha;
	}

	public function checkIsValid() {

	}
}
?>
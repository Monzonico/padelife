<?php

require_once(__DIR__."/../core/ValidationException.php");

class ReservaList{
  private $idreserva;
  private $nombre;
  private $fecha;
  private $hora_ini;
  private $hora_fin;

  public function __construct($idreserva=NULL, $nombre=NULL, $fecha=NULL, $hora_ini=NULL, $hora_fin=NULL) {
		$this->idreserva = $idreserva;
		$this->nombre = $nombre;
		$this->fecha = $fecha;
		$this->hora_ini = $hora_ini;
		$this->hora_fin = $hora_fin;
	}

	public function getId_reserva(){
		return $this->idreserva;
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function getFecha() {
		return $this->fecha;
	}

	public function getHora_ini() {
		return $this->hora_ini;
	}

	public function getHora_fin() {
		return $this->hora_fin;
	}

	public function setId_reserva($idreserva){
		$this->idreserva = $idreserva;
	}

  	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function setFecha($fecha) {
		$this->fecha = $fecha;
	}

	public function setHora_ini($hora_ini) {
		$this->hora_ini = $hora_ini;
	}

	public function setHora_fin($hora_fin) {
		$this->hora_fin = $hora_fin;
	}

  	public function checkIsValid() {
    $errors = array();
  }
}
 ?>

<?php

require_once(__DIR__."/../core/ValidationException.php");

class PartidoList{
  private $id;
  private $pista;
  private $fecha;
  private $hora_ini;
  private $hora_fin;

  public function __construct($id=NULL, $pista=NULL, $fecha=NULL, $hora_ini=NULL, $hora_fin=NULL) {
		$this->id = $id;
		$this->pista = $pista;
		$this->fecha = $fecha;
		$this->hora_ini = $hora_ini;
		$this->hora_fin = $hora_fin;
	}

	public function getId(){
		return $this->id;
	}

	public function getPista(){
		return $this->pista;
	}

	public function getFecha() {
		return $this->fecha;
	}

	public function getHora_ini() {
		return $this->hora_ini;
	}

	public function getHora_fin() {
		return $this->hora_fin;
	}

	public function setId($id){
		$this->id = $id;
	}

  	public function setPista($pista){
		$this->pista = $pista;
	}

	public function setFecha($fecha) {
		$this->fecha = $fecha;
	}

	public function setHora_ini($hora_ini) {
		$this->hora_ini = $hora_ini;
	}

	public function setHora_fin($hora_fin) {
		$this->hora_fin = $hora_fin;
	}

  	public function checkIsValid() {
    $errors = array();
  }
}
 ?>
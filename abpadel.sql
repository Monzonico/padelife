-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-01-2020 a las 17:25:12
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abpadel`
--
CREATE DATABASE IF NOT EXISTS `abpadel` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `abpadel`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campeonatos`
--

CREATE TABLE `campeonatos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `normativa` varchar(280) NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `fecha_ini_inscri` date NOT NULL,
  `fecha_fin_inscri` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `campeonatos`
--

INSERT INTO `campeonatos` (`id`, `nombre`, `normativa`, `fecha_ini`, `fecha_fin`, `fecha_ini_inscri`, `fecha_fin_inscri`) VALUES
(11, 'Padelife - Copa Santiago X', 'Obligatorio uso de material propio', '2020-01-24', '2020-01-28', '2020-01-01', '2020-01-23'),
(12, 'Torneo ESEI - III Edicion', 'Reglamento Estandar', '2020-02-14', '2020-02-16', '2020-01-20', '2020-02-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `id_campeonato` int(11) NOT NULL,
  `nombre` enum('Masculino','Femenino','Mixto','') NOT NULL DEFAULT 'Masculino',
  `nivel` enum('1','2','3','') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `id_campeonato`, `nombre`, `nivel`) VALUES
(23, 11, 'Masculino', '2'),
(24, 11, 'Femenino', '2'),
(34, 11, 'Mixto', '1'),
(35, 12, 'Mixto', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE `clases` (
  `id` int(11) NOT NULL,
  `id_reserva` int(11) NOT NULL,
  `id_entrenador` int(11) NOT NULL,
  `id_pista` int(11) NOT NULL,
  `id_hora` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `nombre` varchar(11) NOT NULL,
  `descripcion` varchar(11) NOT NULL,
  `capacidad` enum('5','10','15','20') NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`id`, `id_reserva`, `id_entrenador`, `id_pista`, `id_hora`, `fecha`, `nombre`, `descripcion`, `capacidad`) VALUES
(2, 2, 43, 1, 6, '2020-02-05', 'Palas', 'Nv1', '20'),
(3, 3, 8, 1, 5, '2020-01-21', 'Paleo', 'Nv2', '5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases_asistentes`
--

CREATE TABLE `clases_asistentes` (
  `id` int(11) NOT NULL,
  `id_clase` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clases_asistentes`
--

INSERT INTO `clases_asistentes` (`id`, `id_clase`, `id_usuario`) VALUES
(9, 2, 4),
(12, 3, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfrentamientos`
--

CREATE TABLE `enfrentamientos` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_pareja1` int(11) NOT NULL,
  `id_pareja2` int(11) NOT NULL,
  `id_pista` int(11) NOT NULL,
  `id_hora` int(11) NOT NULL,
  `set1` varchar(50) NOT NULL,
  `set2` varchar(50) NOT NULL,
  `set3` varchar(50) NOT NULL,
  `resultado` varchar(50) NOT NULL,
  `tipo` enum('Regular','Campeonato','Semifinal','Final') NOT NULL DEFAULT 'Regular',
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horas`
--

CREATE TABLE `horas` (
  `id` int(11) NOT NULL,
  `hora_ini` time NOT NULL,
  `hora_fin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horas`
--

INSERT INTO `horas` (`id`, `hora_ini`, `hora_fin`) VALUES
(1, '09:00:00', '10:30:00'),
(2, '10:30:00', '12:00:00'),
(3, '12:00:00', '13:30:00'),
(4, '13:30:00', '15:00:00'),
(5, '15:00:00', '16:30:00'),
(6, '16:30:00', '18:00:00'),
(7, '18:00:00', '19:30:00'),
(8, '19:30:00', '21:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `hora` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `titulo` varchar(50) NOT NULL,
  `texto` varchar(280) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `hora`, `titulo`, `texto`) VALUES
(1, '2020-01-06 21:36:02', 'Campeonato Proximamente!', 'Se aproxima el campeonato');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parejas`
--

CREATE TABLE `parejas` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_capitan` int(11) NOT NULL,
  `id_jugador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parejas`
--

INSERT INTO `parejas` (`id`, `id_categoria`, `id_capitan`, `id_jugador`) VALUES
(1, 24, 41, 30),
(2, 24, 33, 10),
(3, 24, 39, 38),
(4, 24, 36, 33),
(5, 23, 16, 4),
(6, 23, 18, 27),
(7, 23, 5, 26),
(8, 23, 17, 28),
(9, 34, 24, 32),
(10, 34, 21, 34),
(11, 34, 18, 30),
(12, 34, 16, 40),
(21, 35, 34, 26),
(22, 35, 4, 32),
(23, 35, 27, 24),
(24, 35, 5, 13),
(25, 34, 33, 5),
(26, 34, 30, 26),
(27, 34, 4, 34),
(28, 34, 32, 24),
(29, 24, 35, 37),
(30, 24, 32, 12),
(31, 24, 34, 31),
(32, 24, 30, 32),
(33, 23, 26, 7),
(34, 23, 24, 19),
(35, 23, 16, 4),
(36, 23, 27, 28),
(37, 35, 47, 31),
(38, 35, 49, 50),
(39, 35, 38, 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidos`
--

CREATE TABLE `partidos` (
  `id` int(11) NOT NULL,
  `id_reserva` int(11) NOT NULL,
  `id_pista` int(11) NOT NULL,
  `id_hora` int(11) NOT NULL,
  `id_usuario1` int(11) NOT NULL,
  `id_usuario2` int(11) NOT NULL,
  `id_usuario3` int(11) NOT NULL,
  `id_usuario4` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pistas`
--

CREATE TABLE `pistas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pistas`
--

INSERT INTO `pistas` (`id`, `nombre`) VALUES
(1, 'Uno'),
(2, 'Dos'),
(3, 'Tres'),
(4, 'Cuatro'),
(5, 'Cinco'),
(6, 'Seis'),
(7, 'Siete'),
(8, 'Ocho');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prereservas`
--

CREATE TABLE `prereservas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_hora` int(11) NOT NULL,
  `id_pista` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prereservas_enf`
--

CREATE TABLE `prereservas_enf` (
  `id` int(11) NOT NULL,
  `id_enfrentamiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `id` int(11) NOT NULL,
  `id_pista` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_hora` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `rol` enum('deportista','administrador','entrenador','') NOT NULL DEFAULT 'deportista',
  `contrasena` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `rol`, `contrasena`, `email`) VALUES
(1, 'Fernando', 'administrador', '12345', 'fe@r.com'),
(4, 'Alex', 'deportista', '12345', 'ale@x.com'),
(5, 'Lucas', 'deportista', '12345', 'luca@s.com'),
(7, 'Parga', 'deportista', '12345', 'parg@a.com'),
(8, 'Berte', 'entrenador', '12345', 'bert@e.com'),
(9, 'Maria', 'deportista', '12345', 'mar@ia.com'),
(10, 'Sara', 'deportista', '12345', 'sar@a.com'),
(11, 'Luna', 'deportista', '12345', 'lun@a.com'),
(12, 'Mariola', 'deportista', '12345', 'mariol@a.com'),
(13, 'Julia', 'deportista', '12345', 'juli@a.com'),
(14, 'Marcos', 'deportista', '12345', 'marco@s.com'),
(15, 'Luis', 'deportista', '12345', 'lui@s.com'),
(16, 'Alberto', 'deportista', '12345', 'albert@o.com'),
(17, 'Daniel', 'deportista', '12345', 'danie@l.com'),
(18, 'Adrian', 'deportista', '12345', 'adria@n.com'),
(19, 'Diego', 'deportista', '12345', 'dieg@o.com'),
(20, 'Manuel', 'deportista', '12345', 'manue@l.com'),
(21, 'Axel', 'deportista', '12345', 'axe@l.com'),
(22, 'Nicolas', 'deportista', '12345', 'nicola@s.com'),
(23, 'Ivan', 'deportista', '12345', 'iva@n.com'),
(24, 'Carlos', 'deportista', '12345', 'carlo@s.com'),
(25, 'Miguel', 'deportista', '12345', 'migue@l.com'),
(26, 'Gabriel', 'deportista', '12345', 'gabrie@l.com'),
(27, 'Benjamin', 'deportista', '12345', 'benjami@n.com'),
(28, 'Dante', 'deportista', '12345', 'dant@e.com'),
(29, 'Daniela', 'deportista', '12345', 'daniel@a.com'),
(30, 'Alba', 'deportista', '12345', 'alb@a.com'),
(31, 'Valeria', 'deportista', '12345', 'valeri@a.com'),
(32, 'Emma', 'deportista', '12345', 'emm@a.com'),
(33, 'Laura', 'deportista', '12345', 'laur@a.com'),
(34, 'Claudia', 'deportista', '12345', 'claudi@a.com'),
(35, 'Sofia', 'deportista', '12345', 'sofi@a.com'),
(36, 'Irene', 'deportista', '12345', 'iren@e.com'),
(37, 'Violeta', 'deportista', '12345', 'violet@a.com'),
(38, 'Olivia', 'deportista', '12345', 'olivi@a.com'),
(39, 'Nerea', 'deportista', '12345', 'nere@a.com'),
(40, 'Estela', 'deportista', '12345', 'estel@a.com'),
(41, 'Ariadna', 'deportista', '12345', 'ariadn@a.com'),
(42, 'Candela', 'deportista', '12345', 'candel@a.com'),
(43, 'Samuel', 'entrenador', '12345', 'samue@l.com'),
(44, 'Alexia', 'deportista', '12345', 'alexi@a.com'),
(45, 'Almudena', 'deportista', '12345', 'almuden@a.com'),
(46, 'Aura', 'deportista', '12345', 'aur@a.com'),
(47, 'Adelio', 'deportista', '12345', 'adeli@o.com'),
(48, 'Armando', 'deportista', '12345', 'armand@o.com'),
(49, 'Avelino', 'deportista', '12345', 'avelin@o.com'),
(50, 'Nuria', 'administrador', '12345', 'nuri@a.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campeonatos`
--
ALTER TABLE `campeonatos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_campeonato` (`id_campeonato`);

--
-- Indices de la tabla `clases`
--
ALTER TABLE `clases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_entrenador` (`id_entrenador`),
  ADD KEY `id_pista` (`id_pista`),
  ADD KEY `id_hora` (`id_hora`),
  ADD KEY `id_reserva` (`id_reserva`);

--
-- Indices de la tabla `clases_asistentes`
--
ALTER TABLE `clases_asistentes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_clase` (`id_clase`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `enfrentamientos`
--
ALTER TABLE `enfrentamientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_pareja1` (`id_pareja1`),
  ADD KEY `id_pareja2` (`id_pareja2`),
  ADD KEY `id_hora` (`id_hora`),
  ADD KEY `id_pista` (`id_pista`);

--
-- Indices de la tabla `horas`
--
ALTER TABLE `horas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `parejas`
--
ALTER TABLE `parejas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_capitan` (`id_capitan`),
  ADD KEY `id_jugador` (`id_jugador`);

--
-- Indices de la tabla `partidos`
--
ALTER TABLE `partidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pista` (`id_pista`),
  ADD KEY `id_hora` (`id_hora`),
  ADD KEY `id_usuario1` (`id_usuario1`),
  ADD KEY `id_usuario2` (`id_usuario2`),
  ADD KEY `id_usuario3` (`id_usuario3`),
  ADD KEY `id_usuario4` (`id_usuario4`),
  ADD KEY `id_reserva` (`id_reserva`);

--
-- Indices de la tabla `pistas`
--
ALTER TABLE `pistas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prereservas`
--
ALTER TABLE `prereservas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_hora` (`id_hora`),
  ADD KEY `id_pista` (`id_pista`);

--
-- Indices de la tabla `prereservas_enf`
--
ALTER TABLE `prereservas_enf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_enfrentamiento` (`id_enfrentamiento`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pista` (`id_pista`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_hora` (`id_hora`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `campeonatos`
--
ALTER TABLE `campeonatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `clases`
--
ALTER TABLE `clases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `clases_asistentes`
--
ALTER TABLE `clases_asistentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `enfrentamientos`
--
ALTER TABLE `enfrentamientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `horas`
--
ALTER TABLE `horas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `parejas`
--
ALTER TABLE `parejas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `partidos`
--
ALTER TABLE `partidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pistas`
--
ALTER TABLE `pistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `prereservas`
--
ALTER TABLE `prereservas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prereservas_enf`
--
ALTER TABLE `prereservas_enf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `categorias_ibfk_1` FOREIGN KEY (`id_campeonato`) REFERENCES `campeonatos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `clases`
--
ALTER TABLE `clases`
  ADD CONSTRAINT `clases_ibfk_1` FOREIGN KEY (`id_entrenador`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clases_ibfk_2` FOREIGN KEY (`id_pista`) REFERENCES `pistas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clases_ibfk_3` FOREIGN KEY (`id_hora`) REFERENCES `horas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clases_ibfk_4` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `clases_asistentes`
--
ALTER TABLE `clases_asistentes`
  ADD CONSTRAINT `clases_asistentes_ibfk_1` FOREIGN KEY (`id_clase`) REFERENCES `clases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clases_asistentes_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `enfrentamientos`
--
ALTER TABLE `enfrentamientos`
  ADD CONSTRAINT `enfrentamientos_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enfrentamientos_ibfk_2` FOREIGN KEY (`id_pareja1`) REFERENCES `parejas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enfrentamientos_ibfk_3` FOREIGN KEY (`id_pareja2`) REFERENCES `parejas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enfrentamientos_ibfk_4` FOREIGN KEY (`id_hora`) REFERENCES `horas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enfrentamientos_ibfk_5` FOREIGN KEY (`id_pista`) REFERENCES `pistas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `parejas`
--
ALTER TABLE `parejas`
  ADD CONSTRAINT `parejas_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parejas_ibfk_2` FOREIGN KEY (`id_capitan`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parejas_ibfk_3` FOREIGN KEY (`id_jugador`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `partidos`
--
ALTER TABLE `partidos`
  ADD CONSTRAINT `partidos_ibfk_1` FOREIGN KEY (`id_usuario1`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partidos_ibfk_2` FOREIGN KEY (`id_usuario2`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partidos_ibfk_3` FOREIGN KEY (`id_usuario3`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partidos_ibfk_4` FOREIGN KEY (`id_usuario4`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partidos_ibfk_5` FOREIGN KEY (`id_pista`) REFERENCES `pistas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partidos_ibfk_6` FOREIGN KEY (`id_hora`) REFERENCES `horas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partidos_ibfk_7` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `prereservas`
--
ALTER TABLE `prereservas`
  ADD CONSTRAINT `prereservas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prereservas_ibfk_2` FOREIGN KEY (`id_hora`) REFERENCES `horas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prereservas_ibfk_3` FOREIGN KEY (`id_pista`) REFERENCES `pistas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `prereservas_enf`
--
ALTER TABLE `prereservas_enf`
  ADD CONSTRAINT `prereservas_enf_ibfk_1` FOREIGN KEY (`id_enfrentamiento`) REFERENCES `enfrentamientos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `reservas_ibfk_1` FOREIGN KEY (`id_hora`) REFERENCES `horas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservas_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservas_ibfk_3` FOREIGN KEY (`id_pista`) REFERENCES `pistas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `delete_prereservas_date` ON SCHEDULE EVERY 1 MINUTE STARTS '2019-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM prereservas WHERE fecha < CURDATE()$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
